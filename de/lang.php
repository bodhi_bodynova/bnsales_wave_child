<?php
/**
 * This file is part of OXID eSales Wave theme.
 *
 * OXID eSales Wave theme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eSales Wave theme is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eSales Wave theme.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 */

$sLangName = "Deutsch";

$aLang = array(


    //CUSTLANG
    'charset'                       => 'UTF-8',//'ISO-8859-15',
    'BTN_MY_FAVORITES'              => 'Meine Favoriten',
    'BTN_MYFAVORITES_LISTS'         => 'Meine Favoriten',
    'MYFAVORITES_LISTS_DESC'        => 'Verwalten Sie Ihre Favoriten-Artikel',
    'TO_THE_BASKET'                 => 'In den Warenkorb',
    'SHOW_OPTIONS'                  => 'Varianten anzeigen',
    'ADD_TO_FAVORITE'               => 'Zur Favoritenliste hinzuf&uuml;gen',
    'ADDED_TO_BASKET_CONFIRMATION'  => 'Artikel zum Warenkorb hinzugef&uuml;gt',
    'OPEN_ALL_VARIANTS'             => 'Alle Varianten &ouml;ffnen',
    'SHOW_ARTICLE_DETAILS'          => 'Zeige Artikel Details',
    'AlleArtikel'                   => 'Alle Artikel anzeigen',
    'TOTAL_WEIGHT'                  => 'Gesamtgewicht',
    'ACCOUNT_LOGIN_REQUIRE'         =>  'Sie m&uuml;ssen eingeloggt sein, um den H&auml;ndlershop nutzen zu k&ouml;nnen.',
    'Modal_Title'                   =>  'Artikel Details',
    'THANK_YOU_MESSAGE'             =>  'Vielen Dank f&uuml;r Ihre Nachricht. Wir melden uns schnellstm&ouml;glich bei Ihnen.',
    'AGB_LINK_TITLE'                =>  'AGB',
    'IMPRINT_LINK_TITLE'            =>  'Impressum',
    'BTN_MY_ORDERS'                 =>  'Meine Bestellungen',
    'STREET'                        =>  'Stra&szlig;e',
    'STREETNO'                      =>  'Hausnummer',
    'CITY'                          =>  'Stadt',
    'DATEI'							=>	'Lieferschein anh&auml;ngen',
    'SUBMITFILE'					=>	'hochladen',
    'ALLTOBASKET'					=>	'Alle Artikel in den Warenkorb',
    'RESET_AMOUNT'					=>	'Auswahl zur&uuml;cksetzen',
    'CA'							=>	'ca.',
    'SURCHARGE'                     => 'Aufschlag für Bestellungen unter 75€',

    /* TOOLTIPS */
    'TIP_ALLTOBASKET'				=>	'F&uuml;gt alle zuvor ausgew&auml;hlten Artikel dem Warenkorb hinzu',
    'TIP_SHOW_OPTIONS'				=>	'Zeigt alle verf&uuml;gbaren Varianten des Artikels an',
    'TIP_TO_THE_BASKET'				=>	'Legt die gew&auml;hlte Artikel-Anzahl in den Warenkorb',
    'TIP_MY_ORDERS'					=>	'Schauen Sie sich Ihre bisherigen Bestellungen an und legen die Bestellung einfach erneut in den Warenkorb',
    'TIP_MY_FAVORITES'				=>	'Schauen Sie sich Ihre Artikel-Favoriten an',
    'TIP_QUICKORDER'				=>	'F&uuml;hren Sie White-Label Bestellungen f&uuml;r Ihre Kunden aus',
    'TIP_RESET_AMOUNT'				=>	'Setzt die vorausgew&auml;hlten Artikel-Mengen zur&uuml;ck',
    /* Ende TOOLTIPS */
    'PLEASE_CHANGE_PASSWORD'		=>	'Bitte &auml;ndern Sie zu Ihrer Sicherheit Ihr Passwort!',

    'LOGGEDOUT_HEADLINE'			=>	'Automatischer Logout',
    'LOGGEDOUT_TEXT'				=>	'Sie wurden aufgrund l&auml;ngerer Inaktivit&auml;t automatisch ausgeloggt. ',
    'LOGGEDOUT_BUTTON'				=>	'Neu einloggen',
    'MESSAGE_NOT_ON_STOCK'			=>	'',
    'LOW_STOCK'						=>	'',
    'READY_FOR_SHIPPING'			=>	'',
    'WK_TEXT'						=>	'Die Versandkosten werden nach dem Packen der Ware hinzugef&uuml;gt. Wir geben uns gro&szlig;e M&uuml;he, Ihre Sendung so zu packen dass die Versandkosten so kosteng&uuml;nstig wie m&ouml;glich ausfallen und berechnen nur die tats&auml;chlich anfallenden Kosten.',
    'DELTIMEUNIT'					=>	'Tage',
    'DELIVERYTIME_DELIVERYTIME'     =>  'Produktionszeit',
    /* Quickorder.tpl */
    'BTN_CLOSE'                     =>  'Schließen',
    'BTN_OK'                        =>  'OK',
    'MODAL_ORDER_COMPLETE_TITLE'    =>  'Bestellung Abgeschlossen',
    'MODAL_ORDER_COMPLETE_BODY'     =>  'Ihre Bestellung wurde ausgeführt.',
    'MODAL_ORDER_PREVIEW_TITLE'     =>  'Bestellung Vorschau',
    'MODAL_ORDER_PREVIEW_BODY'      =>  'Die Vorschau zu ihrer Bestellung.',
    'MODAL_ORDER_BTN_CHANGE'        =>  'Bestellung Ändern',
    'QUICKORDER_BTN_ORDERMANDANTORY'=>  'Verbindlich bestellen',
    'QUICKORDER_ARTICLE_DESC'       =>  'Artikel, Beschreibung, o.&Auml;.',
    'BTN_QUICKORDER'                =>  'Schnellbestellung',
    'BTN_ADRESSVERWALTUNG'          =>  'Adressverwaltung',
    'QUICKORDER_DESC'               =>  'neutraler Versand, ohne Werbematerial, direkt an Ihren Kunden (zzgl. Gebühr)',
    'SUBMIT_QUICK_ORDER'            =>  'Bestellung absenden',
    'QUICKORDER_PLACEHOLDER_SEARCHFIELD'    =>  'Artikelnummer oder Beschreibung',
    'SITE_TITLE'                    =>  'Positionen',
    'QUICKORDER_ARTNUM'             =>  'Artikelnummer',
    'QUICKORDER_ARTICLE'            =>  'Artikel',
    'QUICKORDER_SINGLE_PRICE'       =>  'Einzelpreis',
    'QUICKORDER_COUNT'              =>  'Anzahl',
    'QUICKORDER_AVAILABILITY'       =>  'Verfügbarkeit',
    'QUICKORDER_WHITE_LABEL'        =>  'Neutrale Verpackung',
    'QUICKORDER_SEARCH_CONTACTS'    =>  'Adressbuch durchsuchen',
    'QUICKORDER_PDF_TXT'            =>  'Wählen Sie ein PDF, welches wir der Bestellung als Ihren Lieferschein beilegen dürfen:',
    'QUICKORDER_VERFUEGBAR_DELETE'  => 'Verfügbar/Löschen',
    /* Ende Quickorder */
    'ERROR'                             =>  'Fehler',
    'UVP'                               =>  'UVP',
    'TAGE'                              =>  'Tage',

    'NOTHING_ADDED_TO_BASKET_TITLE'     =>  'Hinweis',
    'NOTHING_ADDED_TO_BASKET_TEXT'      =>  'Es wurden keine Artikel in den Warenkorb gelegt!',
    'NOTHING_ADDED_TO_BASKET_CLOSEBTN'  =>  'Schlie&szlig;en',
    'AUTOSUGGEST_DIDYOUMEAN'            =>  'Meinten sie vielleicht: ',
    'PRINT_PDF'                         =>  'PDF erstellen',
    'PRINT_EXCEL'                       =>  'EXCEL erstellen',
    'ALLENEWS'                          =>  'Alle News',
    'ALLENEWSZEIGEN'                    =>  'Alle News öffnen',

    /* zur Übersetzung: */

    'NEUHEITEN'							=>	'Neuheiten',
    'ANGEBOTE'							=>	'Angebote',
    'ARTNR'                             =>  'Artnr',

    /* Checkout Steps */
    'STEPS_BASKET'                      =>  'Warenkorb',
    'STEPS_SEND'                        =>  'Adressen wählen',
    'STEPS_ORDER'                       =>  'Überprüfen & absenden',
    'ADRESSE'                           =>  'Adresse',
    'ADRESS_CHOOSE_HELP'                =>  'Wählen Sie hier eine Lieferadresse an die wir bereits geliefert haben, oder eine Neue Adresse aus.',
    'LIEFERADRESSE'                     =>  'Lieferadresse',
    'RECHNUNGSADRESSE'                  =>  'Rechnungsadresse',
    'NEUTRAL'                           =>  'Neutral',
    'NEUTRALER_VERSAND_HELP'            =>  'neutraler Versand, ohne Werbematerial, direkt an Ihren Kunden (zzgl. Gebühr)',
    'DATEIUPLOAD'                       =>  'Laden Sie Ihren Lieferschein als PDF hoch, wir legen diesen gerne dem Paket bei.',
    'MITTEILUNG'                        =>  'Mitteilung',

    'SIDEBAR_FAVORITES'                 =>  'Meine gespeicherten Links',
    'CHECKOUT'                          =>  'Zur Kasse',
    'ACTION'                            =>  'Aktion',
    'VERPACKUNGSEINHEIT'                =>  'VE',


    'BNTOTAL'                           =>  'Gesamt',

    'WK-OK'                             =>  'Sie haben etwas dem Warenkorb hinzugefügt',
    'WK-ERR'                            =>  'Da ist leider ein Fehler passiert',
    'WK-NO-ART'                         =>  'Sie haben keine Menge gewählt',

    'SEARCHTEXT'                        =>  'Bitte geben Sie hier ihren Suchbegriff ein...',
    'ORDERED-ARTICLES'                  =>  'Bestellte Artikel anzeigen',
    /* Tool-Tip's */
    'TT-WK-OPEN'                           =>  'Warenkorb öffnen',
    'TT-WK-CLOSE'                          =>  'Warenkorb schließen',
    'TT-TO-CHECKOUT'                       =>  'Bestellung Abschließen',
    'TT-VE-TO-BASKET'                      =>  'Aufrunden zur nächst höheren Verpackungseinheit',
    'TT-VE-ADD'                            =>  'Fügt eine Verpackungseinheit hinzu',
    'TT-EDIT'                              =>  'Bearbeiten',
    'TT-SAVE'                              =>  'Speichern',
    'TT-DELETE-ALL'                        =>  'Alle löschen',
    'TT-DELETE'                            =>  'löschen',
    'INCL_TAX_AND_PLUS_SHIPPING'           =>  'Alle Preise Netto, zzgl. Versandkosten',
    'ContaktEmailUserBetr'                 =>  'Vielen Dank für Ihre Anfrage',
    'PREISNEU'                             =>  'ab 01.06.2019',
    'QUICKORDER_ERROR_NO_ARTICLE'          =>  'Sie haben keine Artikel gewählt.',
    'QUICKORDER_ERROR_ADDRESS_FIELD'       =>  'Bitte füllen Sie die Adresse vollständig aus!',
    'QUICKORDER_ERROR'                     =>  'Fehler beim Bestellen',
    'CHANGE_RECHNUNG_ERROR'                =>  'Bitte kontaktieren Sie uns, wenn Sie Änderungen in Ihrer Rechnungsadresse vornehmen möchten.',
    'SUCHBEGRIFF'                          => 'Suchbegriff hinzufügen...',
    //CUSTLANG ENDE

    // Global
    'DD_SORT_DESC'                                          => 'absteigend',
    'DD_SORT_ASC'                                           => 'aufsteigend',
    'DD_DEMO_ADMIN_TOOL'                                    => 'Admin-Tool starten',
    'DD_DELETE'                                             => 'Löschen',

    // Form-Validation
    'DD_FORM_VALIDATION_VALIDEMAIL'                         => 'Bitte geben Sie eine gültige E-Mail-Adresse ein.',
    'DD_FORM_VALIDATION_PASSWORDAGAIN'                      => 'Die Passwörter stimmen nicht überein.',
    'DD_FORM_VALIDATION_NUMBER'                             => 'Bitte geben Sie eine Zahl ein.',
    'DD_FORM_VALIDATION_INTEGER'                            => 'Es sind keine Nachkommastellen erlaubt.',
    'DD_FORM_VALIDATION_POSITIVENUMBER'                     => 'Bitte geben Sie eine positive Zahl ein.',
    'DD_FORM_VALIDATION_NEGATIVENUMBER'                     => 'Bitte geben Sie eine negative Zahl ein.',
    'DD_FORM_VALIDATION_REQUIRED'                           => 'Bitte Wert angeben.',
    'DD_FORM_VALIDATION_CHECKONE'                           => 'Bitte wählen Sie mindestens eine Option.',

    // Header
    'SEARCH_TITLE'                                          => 'Suchbegriff eingeben...',
    'SEARCH_SUBMIT'                                         => 'Suchen',

    // Footer
    'FOOTER_NEWSLETTER_INFO'                                => 'Die neuesten Produkte und die besten Angebote per E-Mail, damit Ihr nichts mehr verpasst.',

    // Startseite
    'MANUFACTURERSLIDER_SUBHEAD'                            => 'Wir präsentieren Ihnen hier unsere sorgsam ausgewählten Marken, deren Produkte Sie in unserem Shop finden.',
    'START_BARGAIN_HEADER'                                  => 'Angebote der Woche',
    'START_NEWEST_HEADER'                                   => 'Frisch eingetroffen',
    'START_TOP_PRODUCTS_HEADER'                             => 'Topseller',
    'START_BARGAIN_SUBHEADER'                               => 'Sparen Sie bares Geld mit unseren aktuellen Schnäppchen!',
    'START_NEWEST_SUBHEADER'                                => 'Frischer geht es nicht. Gerade noch in der Kiste und nun schon im Shop.',
    'START_TOP_PRODUCTS_SUBHEADER'                          => 'Nur %s Produkte, dafür aber die besten, die wir Euch bieten können.',

    // Kontaktformular
    'DD_CONTACT_PAGE_HEADING'                               => 'Kontaktieren Sie uns!',
    'DD_CONTACT_FORM_HEADING'                               => 'Kontaktformular',

    // Link-Seite
    'DD_LINKS_NO_ENTRIES'                                   => 'Es sind leider noch keine Links vorhanden.',

    // 404-Seite
    'DD_ERR_404_START_TEXT'                                 => 'Vielleicht finden Sie die von Ihnen gewünschten Informationen über unsere Startseite:',
    'DD_ERR_404_START_BUTTON'                               => 'Startseite aufrufen',
    'DD_ERR_404_CONTACT_TEXT'                               => 'Dürfen wir Ihnen direkt behilflich sein?<br>Gerne können Sie uns anrufen oder eine E-Mail schreiben:',
    'DD_ERR_404_CONTACT_BUTTON'                             => 'zur Kontaktseite',

    // Login
    'DD_LOGIN_ACCOUNT_PANEL_CREATE_BODY'                    => 'Durch Ihre Anmeldung in unserem Shop, werden Sie in der Lage sein schneller durch den Bestellvorgang geführt zu werden. Des Weiteren können Sie mehrere Versandadressen speichern und Bestellungen in Ihrem Konto verfolgen.',

    // Rechnungs- und Lieferadresse
    'DD_USER_LABEL_STATE'                                   => 'Bundesland',
    'DD_USER_SHIPPING_SELECT_ADDRESS'                       => 'auswählen',
    'DD_USER_SHIPPING_ADD_DELIVERY_ADDRESS'                 => 'neue Adresse hinzufügen',
    'DD_DELETE_SHIPPING_ADDRESS'                            => 'Lieferadresse löschen',

    // Listen
    'DD_LISTLOCATOR_FILTER_ATTRIBUTES'                      => 'Filter',
    'DD_LIST_SHOW_MORE'                                     => 'Produkte ansehen',

    // Lieblingslisten
    'DD_RECOMMENDATION_EDIT_BACK_TO_LIST'                   => 'zurück zur Übersicht',

    // Downloads
    'DD_DOWNLOADS_DOWNLOAD_TOOLTIP'                         => 'herunterladen',
    'DD_FILE_ATTRIBUTES_FILESIZE'                           => 'Dateigröße',
    'DD_FILE_ATTRIBUTES_OCLOCK'                             => 'Uhr',
    'DD_FILE_ATTRIBUTES_FILENAME'                           => 'Dateiname',

    // Detailseite
    'BACK_TO_OVERVIEW'                                      => 'Zur Übersicht',
    'OF'                                                    => 'von',
    'DD_RATING_CUSTOMERRATINGS'                             => 'Kundenmeinungen',
    'PAGE_DETAILS_CUSTOMERS_ALSO_BOUGHT_SUBHEADER'          => 'Kunden die sich diesen Artikel gekauft haben, kauften auch folgende Artikel.',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_ACCESSORIES_SUBHEADER' => 'Folgende Artikel passen gut zu diesem Artikel.',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_SIMILAR_SUBHEADER'     => 'Schauen Sie sich doch auch unsere ähnlichen Artikel an.',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_CROSSSELING_SUBHEADER' => 'Kunden die sich diesen Artikel angesehen haben, haben sich auch folgende Artikel angesehen.',
    'DETAILS_VPE_MESSAGE_1'                                 => 'Dieser Artikel kann nur in Verpackungseinheiten zu je',
    'DETAILS_VPE_MESSAGE_2'                                 => 'erworben werden.',

    // Modal-Warenkorb
    'DD_MINIBASKET_MODAL_TABLE_PRICE'                       => 'Gesamtsumme',
    'DD_MINIBASKET_CONTINUE_SHOPPING'                       => 'weiter einkaufen',

    // Checkout
    'DD_BASKET_BACK_TO_SHOP'                                => 'zurück zum Shop',

    // E-Mails
    'DD_FOOTER_FOLLOW_US'                                    => 'Folgen Sie uns',
    'DD_FORGOT_PASSWORD_HEADING'                            => 'Passwort vergessen',
    'DD_INVITE_HEADING'                                     => 'Artikel-Empfehlung',
    'DD_INVITE_LINK'                                        => 'Link',
    'DD_NEWSLETTER_OPTIN_HEADING'                           => 'Ihre Newsletter-Anmeldung',
    'DD_ORDERSHIPPED_HEADING'                               => 'Versandbestätigung - Bestellung',
    'DD_PRICEALARM_HEADING'                                 => 'Preisalarm',
    'DD_REGISTER_HEADING'                                   => 'Ihre Registrierung',
    'DD_DOWNLOADLINKS_HEADING'                              => 'Ihre Downloadlinks - Bestellung',
    'DD_WISHLIST_HEADING'                                   => 'Wunschzettel',

    'DD_ROLES_BEMAIN_UIROOTHEADER'                          => 'Menü',

    'DD_DELETE_MY_ACCOUNT_WARNING'                          => 'Dieser Vorgang kann nicht rückgängig gemacht werden. Alle persönlichen Daten werden dauerhaft gelöscht.',
    'DD_DELETE_MY_ACCOUNT'                                  => 'Konto löschen',
    'DD_DELETE_MY_ACCOUNT_CONFIRMATION_QUESTION'            => 'Sind Sie sicher, dass Sie Ihr Konto löschen wollen?',
    'DD_DELETE_MY_ACCOUNT_CANCEL'                           => 'Abbrechen',
    'DD_DELETE_MY_ACCOUNT_SUCCESS'                          => 'Ihr Konto wurde gelöscht',
    'DD_DELETE_MY_ACCOUNT_ERROR'                            => 'Das Konto konnte nicht gelöscht werden',

    // Account -> My product reviews
    'DD_DELETE_REVIEW_AND_RATING'                           => 'Bewertung und Sterne-Rating löschen',
    'DD_REVIEWS_NOT_AVAILABLE'                              => 'Es liegen keine Bewertungen vor',
    'DD_DELETE_REVIEW_CONFIRMATION_QUESTION'                => 'Sind Sie sicher, dass Sie die Bewertung löschen wollen?',

    // Contact page
    'DD_SELECT_SALUTATION'                                  => 'Bitte auswählen',

    'DD_CATEGORY_RESET_BUTTON'                              => 'Zurücksetzen',

    'Kommentar'                     => 'Ihr Kommentar',

    //Zusätzliches Zeug
    'AKTIONBIS'                                             => 'Diese Aktion läuft bis zum ',
    'AUSLAUFARTIKEL'                                        => 'Auslaufartikel',
    'LOADPRICELIST'                                         => 'Alle Artikel laden dauert einen Moment!',
    'EXCELPRICELIST'                                        => 'Excel Alle Artikel',
    'PDFPRICELIST'                                          => 'PDF Alle Artikel',
    'EXCELPRICELISTGET'                                     => 'Alle_Artikel',
    'PDFPRICELISTGET'                                       => 'Alle_Artikel',

    //Excel-Output
    'EXCEL_NAME'                                            =>  'Name',
    'EXCEL_VARNAME'                                         =>  'Variante',
    'EXCEL_ARTNUM'                                          =>  'ArtNr',
    'EXCEL_PRICE'                                           =>  'EK/€',
    'EXCEL_UVP'                                             =>  'UVP/€',
    'EXCEL_EAN'                                             =>  'EAN',
    'EXCEL_AMPEL'                                           =>  'Ampel',
    'EXCEL_GEWICHT'                                         =>  'Gewicht',
    'EXCEL_AKTION'                                          =>  'Aktion',
    'EXCEL_RECALL'                                          =>  'Rückruf',
    'EXCEL_RECALLGRUND'                                     =>  'Rückrufgrund',
    'EXCEL_AKTIONENDE'                                      =>  'Aktionsende',
    'EXCEL_AMPEL_0'                                         =>  'Lieferbar',
    'EXCEL_AMPEL_1'                                         =>  'Geringer Bestand',
    'EXCEL_AMPEL_2'                                         =>  'Momentan nicht lieferbar',
    'EXCEL_AUSLAUFARTIKEL'                                  =>  'Auslaufartikel',
    'EXCEL_REASON'                                          =>  'Kaputt',

    'BURGER'                                                => 'Kategorien',
    'MENUKLEIN'                                             => 'Menü',
    'STAND'                                                 => 'Stand',
    'PHONE'                                                 => 'Telefon(1)',
    'PERSONAL_PHONE'                                        => 'Telefon(2)',
    'VETooltip'                                             => 'Verpackungseinheiten',
    'neupreisinfo'                                          => 'Neuer Preis ab',

    'CancelAddressChange'                                   => 'Abbrechen'







);

/*
[{ oxmultilang ident="GENERAL_YOUWANTTODELETE"}]
*/
