[{oxscript include="js/libs/jqBootstrapValidation.min.js" priority=10}]
[{oxscript add="$('input,select,textarea').not('[type=submit]').jqBootstrapValidation();"}]

[{*if $oView->getAccountDeletionStatus() == true}]
    [{assign var="statusMessage" value="DD_DELETE_MY_ACCOUNT_SUCCESS"|oxmultilangassign}]
    [{include file="message/success.tpl" statusMessage=$statusMessage}]
[{/if*}]

<div class="card-deck">
    <div class="card">
        <div class="card-body">
            <form name="login" class="js-oxValidate" action="[{$oViewConf->getSslSelfLink()}]" method="post" novalidate="novalidate">
                <div class="hidden">
                    [{$oViewConf->getHiddenSid()}]
                    [{$oViewConf->getNavFormParams()}]
                    <input type="hidden" name="fnc" value="login_noredirect">
                    <input type="hidden" name="cl" value="[{$oViewConf->getActiveClassName()}]">
                    <input type="hidden" name="tpl" value="[{$oViewConf->getActTplName()}]">
                    <input type="hidden" name="oxloadid" value="[{$oViewConf->getActContentLoadId()}]">
                    [{if $oView->getArticleId()}]
                        <input type="hidden" name="aid" value="[{$oView->getArticleId()}]">
                    [{/if}]
                    [{if $oView->getProduct()}]
                        [{assign var="product" value=$oView->getProduct()}]
                        <input type="hidden" name="anid" value="[{$product->oxarticles__oxnid->value}]">
                    [{/if}]
                </div>

                <div class="row form-group[{if $aErrors}] oxInValid[{/if}]">
                    <label for="loginUser" class="req col-sm-2 col-form-label">[{oxmultilang ident="EMAIL" suffix="COLON"}]</label>
                    <div class="col-sm-10">
                        <input id="loginUser" class="form-control" type="email" name="lgn_usr" role="loginUser" required="required">
                    </div>
                </div>
                <div class="row form-group[{if $aErrors}] has-error[{/if}]"> [{*TODO: Klasse ist aus flow??*}]
                    <label for="loginPwd" class="req col-sm-2 col-form-label">[{oxmultilang ident="PASSWORD" suffix="COLON"}]</label>
                        <div class="col-sm-10">
                            <input id="loginPwd" class="js-oxValidate js-oxValidate_notEmpty  form-control" type="password" name="lgn_pwd" role="loginPwd" required="required">
                        </div>
                </div>
                [{if $oView->showRememberMe()}]
                <div class="form-group row">
                    <label for="loginCookie" class="req col-sm-2 col-7 form-check-label">[{ oxmultilang ident="KEEP_LOGGED_IN" suffix="COLON"}]</label>
                    <div class="col-sm-10 col-5">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="loginCookie" name="lgn_cook" value="1">
                        </div>
                    </div>
                </div>


                [{/if}]

                <div class="form-group row">
                    <div class="offset-md-2 col-sm-10">
                        <button id="loginButton" type="submit" class="btn btn-outline-info submitButton largeButton" role="loginButton" style="margin-left:0 !important;">[{oxmultilang ident="LOGIN"}]</button>
                    </div>
                </div>
            </form>
            <div class="clear"></div>
            <a id="forgotPasswordLink" href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=forgotpwd"}]" class="textLink">[{oxmultilang ident="FORGOT_PASSWORD"}]</a>
        </div>
</div>

