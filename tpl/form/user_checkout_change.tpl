[{oxscript include="js/widgets/oxinputvalidator.js" priority=10 }]
[{oxscript add="$('form.js-oxValidate').oxInputValidator();"}]
[{block name="user_checkout_change"}]
    <form  role="form" class=" js-oxValidate" action="[{$oViewConf->getSslSelfLink()}]" name="order" method="post" enctype="multipart/form-data">
        [{block name="user_checkout_change_form"}]
        [{assign var="aErrors" value=$oView->getFieldValidationErrors()}]
        [{ $oViewConf->getHiddenSid() }]
        [{ $oViewConf->getNavFormParams() }]
    <input type="hidden" name="cl" value="user">
    <input type="hidden" name="option" value="[{$oView->getLoginOption()}]">
    <input type="hidden" name="fnc" value="changeuser">
    <input type="hidden" name="lgn_cook" value="0">
    <input type="hidden" name="blshowshipaddress" value="1">
        <div class="lineBox clear">
            <a href="[{oxgetseourl ident=$oViewConf->getBasketLink() }]" class="prevStep submitButton largeButton btn btn-outline-warning" id="userBackStepTop"><i class="fa fa-long-arrow-left"></i>&nbsp;[{oxmultilang ident="PREVIOUS_STEP"}]</a>
            <button id="userNextStepTop" class="submitButton largeButton nextStep btn btn-outline-success pull-right" name="userform" type="submit">[{oxmultilang ident="CONTINUE_TO_NEXT_STEP"}]&nbsp;<i class="fa fa-long-arrow-right"></i></button>
        </div>
        <div class="checkoutCollumns clear">
            <!--<div class="collumn">-->
            [{block name="user_checkout_billing"}]

            [{* Rechnungsadresse Head *}]
            [{block name="user_checkout_billing_head"}]
            <div class="form-group row">
                <label style="text-align:left;" class="col-xl-2 col-12 control-label norm" for="userChangeAddress"><strong>[{oxmultilang ident="BILLING_ADDRESS" suffix="COLON"}]</strong></label>
                <div class="col-xl-10">
                    <button id="userChangeAddress" style="margin-left: 0 !important;" class="submitButton largeButton btn btn-outline-info" name="changeBillAddress" type="submit">[{oxmultilang ident="CHANGE" }]&nbsp;<i class="fa fa-pencil"></i></i></button>
                </div>
            </div>
            [{*}]<h4 class="blockHead">
                            [{oxmultilang ident="BILLING_ADDRESS" }]
                            <button id="userChangeAddress" class="submitButton largeButton btn btn-info" name="changeBillAddress" type="submit">[{oxmultilang ident="CHANGE" }]&nbsp;<i class="glyphicon glyphicon-pencil"></i></button>
                        </h4>[{*}]
            [{oxscript add="$('#userChangeAddress').click( function() { /*$('#addressForm').show();*/$('#changeinfo').show();/*$('#addressText').hide();$('#userChangeAddress').hide();*/var href=$('#changeinfo')[0].children[0].href;window.location.href=href;return false;});"}]


            [{*if $aErrors}]
            [{oxscript add="$(document).ready(function(){ $('#userChangeAddress').trigger('click');});"}]
            [{/if*}]


            [{/block}]

            [{* Rechnungsadresse Formular *}]
            [{block name="user_checkout_billing_form"}]
            <div id="changeinfo" class="alert alert-danger" style="display: none">[{oxmultilang ident="CHANGE_RECHNUNG_ERROR"}] <a href="mailto:sales@bodynova.com">email</a></div>
            <div class="form-group" style="display: none;" id="addressForm">
                [{include file="form/fieldset/user_billing.tpl" noFormSubmit=true blSubscribeNews=true blOrderRemark=true}]
            </div>
            [{/block}]

            [{* Rechnungsadresse Anzeige *}]
            [{block name="user_checkout_billing_feedback"}]
            <div class="form-group row">
                <label class="col-sm-2 control-label norm" for="showShipAddress" >[{* oxmultilang ident="RECHNUNGSADRESSE" suffix="COLON"*}]</label>
                <div class="col-sm-10" style="margin-top: 8px;">
                    [{include file="widget/address/billing_address.tpl" noFormSubmit=true blSubscribeNews=true blOrderRemark=true}]
                </div>
            </div>
            [{/block}]
            [{/block}]
            <!--</div>-->

            [{block name="user_checkout_shipping"}]

            [{* Lieferadresse Head *}]
            [{block name="user_checkout_shipping_head"}]
            <div class="form-group row">
                <label  style="text-align: left;"  class="col-xl-2 col-12 control-label norm" for="showShipAddress"><strong>[{oxmultilang ident="SHIPPING_ADDRESS" suffix="COLON"}]</strong></label>
                <div class="col-xl-10">
                    <button id="userChangeShippingAddress" onclick="myscrollTo(500)" style="margin-left: 0 !important;" class="submitButton largeButton btn btn-outline-info" name="changeShippingAddress" type="submit" [{if !$oView->showShipAddress() or !$oxcmp_user->getSelectedAddress()}] style="display: none;" [{/if}]>
                        [{oxmultilang ident="CHANGE"}]&nbsp;<i class="fa fa-pencil"></i></i>
                    </button>
                    <button type="button" class="btn btn-default" id="newAdress"  onclick="$('#addressId').val(-1);$('#addressId').click();" [{if !$oView->showShipAddress() or !$oxcmp_user->getSelectedAddress()}] style="display: none;" [{/if}]>[{ oxmultilang ident="NEW_ADDRESS" }]</button>
                </div>
            </div>
            [{if $aErrors}]
            [{oxscript add="$(document).ready(function(){ $('#userChangeShippingAddress').trigger('click');});"}]
            [{/if}]



            [{*}]<h4 class="blockHead">
                            [{oxmultilang ident="SHIPPING_ADDRESS"}]
                            <button id="userChangeShippingAddress" class="submitButton largeButton btn btn-info" name="changeShippingAddress" type="submit" [{if !$oView->showShipAddress() or !$oxcmp_user->getSelectedAddress()}] style="display: none;" [{/if}]>
                                [{oxmultilang ident="CHANGE"}]&nbsp;<i class="glyphicon glyphicon-pencil"></i>
                            </button>
                        </h4>[{*}]
            [{oxscript add="$('#showShipAddress').change(function() { $('#newAdress').toggle($(this).is(':not(:checked)') && $('#addressId').val() != -1 ); $('#userChangeShippingAddress').toggle($(this).is(':not(:checked)') && $('#addressId').val() != -1 ); }); "}]
            [{oxscript add="$('#addressId').change(function() { $('#userChangeShippingAddress').toggle($('#addressId').val() != -1 );}); "}]
            [{oxscript add="$('#newAdress').click( function() { $('#shippingAddressForm').show();$('#shippingAddressText').hide(); $('#newAdress').hide();$('#shippingAddressForm').find('input:text').val('');;return false;});"}]
            [{/block}]

            [{* Rechnungsadresse als Lieferadresse verwenden Checkbox *}]
            [{block name="user_checkout_shipping_change"}]
            <div class="form-group row">
                <label class="col-xl-2 col-sm-3 control-label norm" for="showShipAddress">[{ oxmultilang ident="RECHNUNGSADRESSE" suffix="COLON"}]</label>
                <div class="col-xl-10 col-sm-9">
                    <input style="margin-top: 10px;margin-left:10px;" class="" type="checkbox" name="blshowshipaddress" id="showShipAddress" [{if !$oView->showShipAddress()}]checked[{/if}] value="0">
                    <span class="form-text">[{oxmultilang ident="USE_BILLINGADDRESS_FOR_SHIPPINGADDRESS"}]</span>
                </div>
            </div>

            [{oxscript add="$('#showShipAddress').change( function(e){ changeShipAdress(e)} );"}]
            [{/block}]

            [{* Lieferadresse Formular *}]
            [{block name="user_checkout_shipping_form"}]
            <div id="shippingAddress" class="form-group" [{if !$oView->showShipAddress()}]style="display: none;"[{/if}]>
                [{include file="form/fieldset/user_shipping.tpl" noFormSubmit=true onChangeClass='user'}]
            </div>
            [{/block}]

            [{* Newsletter und Mitteilung *}]
            [{block name="user_checkout_shipping_feedback"}]
            [{include file="form/fieldset/order_newsletter.tpl" blSubscribeNews=true}]
            [{include file="form/fieldset/order_remark.tpl" blOrderRemark=true}]
            [{/block}]

            [{/block}]

        </div>
        <div class="lineBox clear">
            <a href="[{oxgetseourl ident=$oViewConf->getBasketLink()}]" class="prevStep submitButton largeButton btn btn-outline-warning pull-left" id="userBackStepBottom"><i class="fa fa-long-arrow-left"></i>&nbsp;[{oxmultilang ident="PREVIOUS_STEP"}]</a>
            <button id="userNextStepBottom" class="submitButton largeButton nextStep btn btn-outline-success pull-right" name="userform" type="submit">[{oxmultilang ident="CONTINUE_TO_NEXT_STEP"}]&nbsp;<i class="fa fa-long-arrow-right"></i></button>
        </div>
        [{/block}]
    </form>
    [{/block}]


[{*oxscript include="js/libs/jqBootstrapValidation.min.js" priority=10}]

[{capture assign="sValidationJS"}]
    [{strip}]
    $('input,select,textarea').not('[type=submit]').jqBootstrapValidation(
        {
            filter: function()
            {
                if( $( '#shippingAddress' ).css( 'display' ) == 'block' )
                {
                    return $(this).is(':visible, .selectpicker');
                }
                else
                {
                    return $(this).is(':visible, #addressForm .selectpicker');
                }
            }
        }
    );
    [{/strip}]
[{/capture}]

[{oxscript add=$sValidationJS}]

[{block name="user_checkout_change"}]
    <form class="form-horizontal" action="[{$oViewConf->getSslSelfLink()}]" name="order" method="post" novalidate="novalidate">
        [{block name="user_checkout_change_form"}]
            [{assign var="aErrors" value=$oView->getFieldValidationErrors()}]
            [{$oViewConf->getHiddenSid()}]
            [{$oViewConf->getNavFormParams()}]
            <input type="hidden" name="cl" value="user">
            <input type="hidden" name="option" value="[{$oView->getLoginOption()}]">
            <input type="hidden" name="fnc" value="changeuser">
            <input type="hidden" name="lgn_cook" value="0">
            <input type="hidden" name="blshowshipaddress" value="1">

            [{block name="user_checkout_change_next_step_top"}]
                <div class="card bg-light cart-buttons">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <a href="[{oxgetseourl ident=$oViewConf->getBasketLink()}]" class="btn btn-outline-dark float-left prevStep submitButton largeButton" id="userBackStepTop"><i class="fa fa-caret-left"></i> [{oxmultilang ident="PREVIOUS_STEP"}]</a>
                            </div>
                            <div class="col-12 col-md-6 text-right">
                                <button id="userNextStepTop" class="btn btn-primary pull-right submitButton largeButton nextStep" name="userform" type="submit">[{oxmultilang ident="CONTINUE_TO_NEXT_STEP"}] <i class="fa fa-caret-right"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            [{/block}]

            <div class="checkoutCollumns clear">
                <div class="card">
                    [{block name="user_checkout_billing"}]
                        <div class="card-header">
                            [{block name="user_checkout_billing_head"}]
                                <h3 class="card-title">
                                    [{oxmultilang ident="BILLING_ADDRESS"}]
                                    <button id="userChangeAddress" class="btn btn-sm btn-warning float-right submitButton largeButton edit-button" name="changeBillAddress" type="submit" title="[{oxmultilang ident="CHANGE"}]">
                                        <i class="fas fa-pencil-alt"></i>
                                    </button>
                                </h3>
                                [{oxscript add="$('#userChangeAddress').click( function() { $('#addressForm').show();$('#addressText').hide();$('#userChangeAddress').hide();return false;});"}]
                            [{/block}]
                        </div>
                    [{/block}]
                    <div class="card-body">
                        [{block name="user_checkout_billing_form"}]
                            <div [{if !$aErrors|@count }]style="display: none;"[{/if}] id="addressForm">
                                [{if not $oxcmp_user->oxuser__oxpassword->value}]
                                    [{include file="form/fieldset/user_email.tpl"}]
                                [{/if}]
                                [{include file="form/fieldset/user_billing.tpl" noFormSubmit=true blSubscribeNews=true blOrderRemark=true}]
                            </div>
                        [{/block}]
                        [{block name="user_checkout_billing_feedback"}]
                            <div class="col-lg-9 offset-lg-3" id="addressText">
                                [{include file="widget/address/billing_address.tpl" noFormSubmit=true blSubscribeNews=true blOrderRemark=true}]
                            </div>
                        [{/block}]
                    </div>
                </div>

                <div class="card">
                    [{block name="user_checkout_shipping"}]
                        <div class="card-header">
                            [{block name="user_checkout_shipping_head"}]
                                <h3 class="card-title">
                                    [{oxmultilang ident="SHIPPING_ADDRESS"}]
                                    [{if $oView->showShipAddress() and $oxcmp_user->getSelectedAddress()}]
                                        <button id="userChangeShippingAddress" class="btn btn-sm btn-warning float-right submitButton largeButton edit-button" name="changeShippingAddress" type="submit" [{if !$oView->showShipAddress() and $oxcmp_user->getSelectedAddress()}] style="display: none;" [{/if}] title="[{oxmultilang ident="CHANGE"}]">
                                            <i class="fas fa-pencil-alt"></i>
                                        </button>
                                    [{/if}]
                                </h3>
                                [{oxscript add="$('#userChangeShippingAddress').toggle($(this).is(':not(:checked)'));"}]
                            [{/block}]
                        </div>
                    [{/block}]
                    <div class="card-body">
                        [{block name="user_checkout_shipping_change"}]
                            <div class="form-group">
                                <div class="col-lg-9 offset-lg-3">
                                    <div class="checkbox">
                                        <label for="showShipAddress">
                                            <input type="checkbox" name="blshowshipaddress" id="showShipAddress" [{if !$oView->showShipAddress()}]checked[{/if}] value="0"> [{oxmultilang ident="USE_BILLINGADDRESS_FOR_SHIPPINGADDRESS"}]
                                        </label>
                                    </div>
                                </div>
                            </div>
                            [{oxscript add="$('#showShipAddress').change( function() { $('#shippingAddress').toggle($(this).is(':not(:checked)'));});"}]
                        [{/block}]
                        [{block name="user_checkout_shipping_form"}]
                            <div id="shippingAddress" [{if !$oView->showShipAddress()}]style="display: none;"[{/if}]>
                                [{include file="form/fieldset/user_shipping.tpl" noFormSubmit=true onChangeClass='user'}]
                            </div>
                        [{/block}]
                        [{block name="user_checkout_shipping_feedback"}]
                            [{include file="form/fieldset/order_newsletter.tpl" blSubscribeNews=true}]
                            [{include file="form/fieldset/order_remark.tpl" blOrderRemark=true}]
                        [{/block}]
                    </div>
                </div>
            </div>

            [{block name="user_checkout_change_next_step_bottom"}]
                <div class="card bg-light cart-buttons">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <a href="[{oxgetseourl ident=$oViewConf->getBasketLink()}]" class="btn btn-outline-dark float-left prevStep submitButton largeButton" id="userBackStepBottom"><i class="fa fa-caret-left"></i> [{oxmultilang ident="PREVIOUS_STEP"}]</a>
                            </div>
                            <div class="col-12 col-md-6 text-right">
                                <button id="userNextStepBottom" class="btn btn-primary pull-right submitButton largeButton nextStep" name="userform" type="submit">[{oxmultilang ident="CONTINUE_TO_NEXT_STEP"}] <i class="fa fa-caret-right"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            [{/block}]
        [{/block}]
    </form>
    [{include file="form/fieldset/delete_shipping_address_modal.tpl"}]
[{/block*}]
