[{oxscript include="js/libs/jqBootstrapValidation.min.js" priority=10}]
[{oxscript add="$('input,select,textarea').not('[type=submit]').jqBootstrapValidation();"}]
<div class="card-deck">
    <div class="card">
        <div class="card-body">
<p>
    [{oxmultilang ident="HAVE_YOU_FORGOTTEN_PASSWORD"}]<br>
    [{oxmultilang ident="HERE_YOU_SET_UP_NEW_PASSWORD"}]
</p>

            <form class="js-oxValidate form-horizontal" action="[{$oViewConf->getSelfActionLink()}]" name="forgotpwd" method="post" novalidate="novalidate">
                <div class="hidden">
                    [{$oViewConf->getHiddenSid()}]
                    [{$oViewConf->getNavFormParams()}]
                </div>
                <input type="hidden" name="fnc" value="forgotpassword">
                <input type="hidden" name="cl" value="forgotpwd">
                <input type="hidden" name="actcontrol" value="forgotpwd">
                <div class="form clear form-horizontal">
                    <div class="form-group row">
                        <label class="req col-form-label col-sm-2">[{oxmultilang ident="YOUR_EMAIL_ADDRESS" suffix="COLON"}]</label>
                        <div class="col-sm-10">
                            <input id="forgotPasswordUserLoginName[{$idPrefix}]" type="email" name="lgn_usr" value="[{$oView->getActiveUsername()}]" class="form-control js-oxValidate js-oxValidate_notEmpty js-oxValidate_email">
                            <p class="oxValidateError">
                                <span class="js-oxError_notEmpty">[{oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS"}]</span>
                                <span class="js-oxError_email">[{oxmultilang ident="ERROR_MESSAGE_INPUT_NOVALIDEMAIL"}]</span>
                                [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxusername}]
                            </p>
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-sm-2"></label>
                        <div class="col-sm-10">
                            <button class="submitButton btn btn-outline-success btn-large" type="submit">[{oxmultilang ident="REQUEST_PASSWORD"}]</button>
                        </div>
                    </div>
                </div>
            </form>

[{oxmultilang ident="REQUEST_PASSWORD_AFTERCLICK"}]<br><br>
[{oxifcontent ident="oxforgotpwd" object="oCont"}]
    [{$oCont->oxcontents__oxcontent->value}]
[{/oxifcontent}]
        </div>
    </div>
</div>