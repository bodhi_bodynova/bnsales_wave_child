[{if $blSubscribeNews}]
    [{block name="user_billing_newsletter"}]
    <div class="form-group row">
        <label class="col-xl-2 col-sm-3 control-label norm" for="subscribeNewsletter">[{oxmultilang ident="NEWSLETTER" suffix="COLON" }]</label>
        <div class="col-xl-10 col-sm-9" style="padding-top:6px;">
            <input type="hidden" name="blnewssubscribed" value="0">
            <input style="margin-left:10px;" id="subscribeNewsletter" type="checkbox" name="blnewssubscribed" value="1" [{if $oView->isNewsSubscribed()}]checked[{/if}]>
            <span id="hilfeText" class="form-text">[{oxmultilang ident="MESSAGE_NEWSLETTER_SUBSCRIPTION" }]</span>
        </div>
    </div>
    [{/block}]
    [{/if}]
