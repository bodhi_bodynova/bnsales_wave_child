[{if $blOrderRemark}]
    [{*oxscript include="js/widgets/oxinnerlabel.js" priority=10 }]
    [{oxscript add="$( '#orderRemark' ).oxInnerLabel();"*}]
    <div class="form-group row">
        <label class="col-xl-2 col-sm-3 control-label norm">[{oxmultilang ident="MITTEILUNG" suffix="COLON"}]</label>
        <div class="col-xl-10 col-sm-9" style="padding-top:10px;">
            <textarea id="orderRemark" rows="7" name="order_remark" class="areabox form-control" aria-describedby="mitteilungsText" >[{$oView->getOrderRemark()}]</textarea>
            <span class="form-text" id="mitteilungsText">[{oxmultilang ident="HERE_YOU_CAN_ENETER_MESSAGE"}]</span>
        </div>
        [{*}]
        <label for="orderRemark" class="innerLabel textArea">[{oxmultilang ident="HERE_YOU_CAN_ENETER_MESSAGE"}]</label>
        [{*}]
    </div>
    [{/if}]
