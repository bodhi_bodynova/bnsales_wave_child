[{oxscript add="$('a.js-external').attr('target', '_blank');"}]
[{capture append="oxidBlock_content"}]
    [{assign var='rsslinks' value=$oView->getRssLinks() }]
    <div class="card " style="margin-bottom:180px;height:auto;">
        <div class="card-header">
            <h1 class="card-title"><strong>[{$oView->getTitle()}]</strong></h1>
            <div class="pull-right" style="margin-top: -27px;">
                [{if $oView->getClassName() eq "search" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
        </div>
        <div class="card-body">
            [{block name="search_results"}]
            [{if $oView->getArticleCount() }]
            <div class="listRefine clear bottomRound">
                [{block name="search_top_listlocator"}]
                [{include file="widget/locator/listlocator.tpl"  locator=$oView->getPageNavigationLimitedTop() listDisplayType=true itemsPerPage=true sort=true }]
                [{/block}]
            </div>
            [{else}]
            <div>[{oxmultilang ident="NO_ITEMS_FOUND"}]</div>
            [{/if}]
            [{if $oView->getArticleList() }]

            [{*$oView|var_dump*}]
            [{foreach from=$oView->getArticleList() name=search item=product}]
            [{include file="widget/product/list.tpl" type='line' listId="searchList" products=$oView->getArticleList() showMainLink=true }]
            [{/foreach}]
            [{/if}]
            [{if $oView->getArticleCount() }]
            [{include file="widget/locator/listlocator.tpl" locator=$oView->getPageNavigationLimitedBottom() place="bottom"}]
            [{/if}]
            [{/block}]
        </div>
    </div>
    [{/capture}]
[{include file="layout/page.tpl" sidebar="Left" sidebarLeft=1 sidebarRight=1}]
