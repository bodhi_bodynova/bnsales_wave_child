[{capture append="oxidBlock_content"}]
    [{assign var="oContent" value=$oView->getContent()}]
    [{assign var="tpl" value=$oViewConf->getActTplName()}]
    [{assign var="oxloadid" value=$oViewConf->getActContentLoadId()}]
    [{assign var="template_title" value=$oView->getTitle()}]
    <div class="card" style="margin-bottom:180px;height:auto;">
        <div class="card-header">
            <h3 class="card-title">[{$oView->getTitle()}]</h3>
            <div class="pull-right" style="margin-top: -27px;">
                [{if $oView->getClassName() eq "content" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
        </div>
        <div class="card-body">
            [{$oView->getParsedContent()}]
        </div>
    </div>

    [{insert name="oxid_tracker" title=$template_title}]
    [{/capture}]
[{include file="layout/page.tpl" sidebar="Left" sidebarLeft=1 sidebarRight=1}]
