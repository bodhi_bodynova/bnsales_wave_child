[{capture append="oxidBlock_content"}]
    <div class="card" style="margin-top: 150px;height: auto !important;bottom: 150px;">
        <div style="height:46.6px !important;" class="card-header">
            <h1 class="card-title">[{*$oView->getTitle()*}]<strong>[{$oxcmp_shop->oxshops__oxcompany->value}]</strong></h1>
            <div class="pull-right" style="margin-top: -27px;">
                [{if $oView->getClassName() eq "contact" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
        </div>
        <div class="card-body">
            [{if $oView->getContactSendStatus() }]
            [{assign var="_statusMessage" value="THANK_YOU_MESSAGE"|oxmultilangassign:$oxcmp_shop->oxshops__oxname->value}]
            [{include file="message/notice.tpl" statusMessage=$_statusMessage}]
            [{/if }]
            [{*}]<h1 class="pageHead">[{ $oxcmp_shop->oxshops__oxcompany->value }]</h1>[{*}]

            <div class="row">
                <div class="container-fluid">
                    [{include file="form/contact.tpl"}]
                </div>
            </div>

            [{ insert name="oxid_tracker" title=$template_title }]
        </div>
    </div>
    [{/capture}]

[{include file="layout/page.tpl" sidebarLeft=1 sidebarRight=1}]
