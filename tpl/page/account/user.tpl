[{capture append="oxidBlock_content"}]
    [{assign var="template_title" value="BILLING_SHIPPING_SETTINGS"|oxmultilangassign }]
    <div id="cardUser" class="card" style="margin-bottom:140px;height:auto;">
        <div class="card-header">
            <h1 class="card-title"><strong>[{ $template_title }]</strong></h1>
            <div class="d-xl-none ">
                [{if $oView->getClassName() eq "account_user" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
            <div class="pull-right d-none d-sm-none d-md-none d-lg-none d-xl-block" style="margin-top: -27px;">
                [{if $oView->getClassName() eq "account_user" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
        </div>
        <div class="card-body">
            [{block name="account_user_form"}]
            [{include file="form/user.tpl"}]
            [{/block}]
            [{ insert name="oxid_tracker" title=$template_title }]
        </div>
    </div>
    [{/capture}]

[{capture append="oxidBlock_sidebar"}]
    [{include file="page/account/inc/account_menu.tpl" active_link="billship"}]
    [{/capture}]

[{include file="layout/page.tpl" sidebarLeft=1 sidebarRight=1 sidebar="Left"}]
