[{capture append="oxidBlock_content"}]
    [{if $oView->getSubscriptionStatus() != 0 }]
    [{if $oView->getSubscriptionStatus() == 1 }]
    <div class="status success corners">[{oxmultilang ident="MESSAGE_NEWSLETTER_SUBSCRIPTION_SUCCESS"}]</div>
    [{else }]
    <div class="status success corners">[{oxmultilang ident="MESSAGE_NEWSLETTER_SUBSCRIPTION_CANCELED"}]</div>
    [{/if}]
    [{/if}]
    <div id="newsletterAbo" class="card" style="margin-bottom:140px; height:auto;">
        <div class="card-header">
            <h1 class="card-title"><strong>[{$oView->getTitle()}]</strong></h1>
            <div class="d-xl-none">
                [{if $oView->getClassName() eq "account_newsletter" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
            <div class="pull-right d-none d-sm-none d-md-none d-lg-none d-xl-block  " style="margin-top: -27px;">
                [{if $oView->getClassName() eq "account_newsletter" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
        </div>
        <div class="card-body">
            [{include file="form/account_newsletter.tpl"}]
        </div>
    </div>
    [{/capture}]


[{*capture append="oxidBlock_sidebar"}]
    [{include file="page/account/inc/account_menu.tpl" active_link="newsletter"}]
[{/capture*}]

[{include file="layout/page.tpl" sidebarLeft=1 sidebarRight=1}]
