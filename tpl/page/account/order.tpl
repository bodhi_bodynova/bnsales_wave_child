[{capture append="oxidBlock_content"}]
    [{assign var="template_title" value="ORDER_HISTORY"|oxmultilangassign}]
    <div id="cardOrder" style="margin-bottom:140px;height:auto;" class="card">
        <div  class="card-header">
            <h1 class="card-title"><strong>[{ $template_title }]</strong></h1>

            <div class="d-sm-none d-block ">
                [{if $oView->getClassName() eq "account_order" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
            <div class="pull-right d-none d-sm-block" style="margin-top: -27px;">
                [{if $oView->getClassName() eq "account_order" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>

        </div>
        <div class="card-body">

            <button style="margin-left:-15px;" onclick="loadPDF('[{$oViewConf->getSelfLink()}]cl=pricelist&fnc=getPDF&name=order&user=[{$oxcmp_user->oxuser__oxusername->value}]');" type="button" class="btn btn-default btn-outline-primary bntooltip" data-style="expand-right" data-placement="top" title="[{oxmultilang ident="PRINT_PDF"}]">PDF</button>

            <button onclick="window.location='[{$oViewConf->getSelfLink()}]cl=pricelist&fnc=getExcel&name=order&user=[{$oxcmp_user->oxuser__oxusername->value}]'"
               type="button" class="btn btn-default btn-outline-primary bntooltip" data-style="expand-right" data-placement="top"
            >Excel</button>
            [{assign var=oOrders value=$oView->getOrderList()}]

            [{block name="account_order_history"}]
            [{if count($oOrders) > 0}]
            [{assign var=oArticleList value=$oView->getOrderArticleList()}]
            [{*
            <div class="row" style="margin-top:10px">
                [{foreach from=$oOrders item=order}]

                <div class="adressverwaltunggroß" style="border:1px solid rgba(0,0,0,.1);padding:5px;cursor:default">
                    <span class="bntooltip" data-placement="right" data-toggle="tooltip" data-original-title="[{oxmultilang ident="ORDER_DATE"}]">[{ $order->oxorder__oxorderdate->value|date_format:"%d.%m.%Y" }]</span><br>
                    [{if $order->oxorder__oxstorno->value}]
                    <span class="note">[{ oxmultilang ident="ORDER_IS_CANCELED" }]</span>
                    [{elseif $order->oxorder__oxsenddate->value !="-" }]
                    <span>[{ oxmultilang ident="SHIPPED" }]</span>
                    [{else}]
                    <span class="note bntooltip" data-placement="right" data-toggle="tooltip" data-original-title="[{oxmultilang ident="STATUS"}]">[{ oxmultilang ident="NOT_SHIPPED_YET" }]</span>
                    [{/if}]<br>
                    <span class="bntooltip" data-placement="right" data-toggle="tooltip" data-original-title="[{oxmultilang ident="ORDER_NUMBER"}]">[{ $order->oxorder__oxordernr->value }]</span><br>
                    <span class="bntooltip" data-placement="right" data-toggle="tooltip" data-original-title="[{oxmultilang ident="SHIPMENT_TO"}]">[{if $order->oxorder__oxdellname->value }]
                    [{ $order->oxorder__oxdelfname->value }]
                    [{ $order->oxorder__oxdellname->value }]
                    [{else }]
                    [{ $order->oxorder__oxbillfname->value }]
                    [{ $order->oxorder__oxbilllname->value }]
                    [{/if}]</span><br>
                    <a href="[{$oViewConf->getCurrentHomeDir()}]index.php?cl=myfavorites&fnc=showOrderedArticles&orderid=[{$order->oxorder__oxid->value}]" style="color:#337ab7">[{oxmultilang ident="ORDERED-ARTICLES"}]</a>
                    <br>
                </div>
                [{/foreach}]
            </div>*}]
            <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>[{ oxmultilang ident="ORDER_DATE" suffix="COLON" }]</th>
                    <th>[{ oxmultilang ident="STATUS" suffix="COLON" }]</th>
                    <th>[{ oxmultilang ident="ORDER_NUMBER" suffix="COLON" }]</th>
                    <th>[{ oxmultilang ident="SHIPMENT_TO" suffix="COLON" }]</th>
                    <th>[{ oxmultilang ident="ACTION" suffix="COLON" }]</th>
                </tr>
                </thead>
                <tbody>
                [{foreach from=$oOrders item=order}]
                    <tr>
                        <td>
                            [{ $order->oxorder__oxorderdate->value|date_format:"%d.%m.%Y" }]
                        </td>
                        <td>
                            [{if $order->oxorder__oxstorno->value}]
                            <span class="note">[{ oxmultilang ident="ORDER_IS_CANCELED" }]</span>
                            [{elseif $order->oxorder__oxsenddate->value !="-" }]
                            <span>[{ oxmultilang ident="SHIPPED" }]</span>
                            [{else}]
                            <span class="note">[{ oxmultilang ident="NOT_SHIPPED_YET" }]</span>
                            [{/if}]
                        </td>
                        <td>[{ $order->oxorder__oxordernr->value }]</td>
                        <td>
                            [{if $order->oxorder__oxdellname->value }]
                            [{ $order->oxorder__oxdelfname->value }]
                            [{ $order->oxorder__oxdellname->value }]
                            [{else }]
                            [{ $order->oxorder__oxbillfname->value }]
                            [{ $order->oxorder__oxbilllname->value }]
                            [{/if}]
                        </td>
                        <td>
                            <a href="[{$oViewConf->getSelfLink()}]cl=myfavorites&fnc=showOrderedArticles&orderid=[{$order->oxorder__oxid->value}]">[{oxmultilang ident="ORDERED-ARTICLES"}]</a>
                        </td>
                    </tr>
                    [{/foreach}]
                </tbody>
            </table>
            </div>

            [{else}]
            [{ oxmultilang ident="ORDER_EMPTY_HISTORY" }]
            [{/if}]
            [{/block}]
            [{insert name="oxid_tracker" title=$template_title }]
        </div>
    </div>
    [{/capture}]

[{capture append="oxidBlock_sidebar"}]
    [{include file="page/account/inc/account_menu.tpl" active_link="orderhistory"}]
    [{/capture}]

[{include file="layout/page.tpl" sidebarLeft=1 sidebarRight=1 sidebar="Left"}]
