[{capture append="oxidBlock_content"}]
    [{assign var="template_title" value="CHANGE_PASSWORD"|oxmultilangassign }]
    <div id="passwordChange" class="card" style="margin-bottom: 140px; height:auto;">
        <div class="card-header">
            <h1 class="card-title [{*if $oView->isPasswordChanged() }] hide[{/if*}]">
                <strong>[{oxmultilang ident="CHANGE_PASSWORD"}]</strong>
            </h1>
            <div class="d-xl-none d-lg-none d-md-none">
                [{if $oView->getClassName() eq "account_password" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
            <div class="pull-right d-none d-sm-none d-md-block  " style="margin-top: -27px;">
                [{if $oView->getClassName() eq "account_password" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
        </div>
        <div class="card-body">
            [{* Response Message Success: *}]
            [{if $oView->isPasswordChanged() }]
            <div class="status success corners alert alert-success" role="alert">
                [{ oxmultilang ident="MESSAGE_PASSWORD_CHANGED" }]
                <script>
                    window.setTimeout('location.href="index.php"', 2000);
                </script>
            </div>
            [{/if}]
            [{* Response Message Error: *}]
            [{if count($Errors) > 0 && count($Errors.user) > 0}]
            <div class="status error corners">
                [{foreach from=$Errors.user item=oEr key=key }]
                <p>[{ $oEr->getOxMessage()}]</p>
                [{/foreach}]
            </div>
            [{/if}]

            [{* Passwort Formular: *}]
            [{include file="form/user_password.tpl"}]
            [{insert name="oxid_tracker" title=$template_title }]
        </div>
    </div>
    [{/capture}]

[{* Sidebar Menue: *}]
[{capture append="oxidBlock_sidebar"}]
    [{include file="page/account/inc/account_menu.tpl" active_link="password"}]
    [{/capture}]

[{include file="layout/page.tpl" sidebarLeft=1 sidebarRight=1 }]
