[{capture append="oxidBlock_content"}]
        [{oxscript add="$('#showNews').click();"}]

    [{* Device Auswertung *}]

    [{if $oxcmp_user}]
    [{assign var="oxid" value=$oxcmp_user->oxuser__oxid->rawValue}]
        [{oxscript add="(function () {
  'use strict';

  var module = {
    options: [],
    header: [navigator.platform, navigator.userAgent, navigator.appVersion, navigator.vendor, window.opera],
    dataos: [
      { name: 'Windows Phone', value: 'Windows Phone', version: 'OS' },
      { name: 'Windows', value: 'Win', version: 'NT' },
      { name: 'iPhone', value: 'iPhone', version: 'OS' },
      { name: 'iPad', value: 'iPad', version: 'OS' },
      { name: 'Kindle', value: 'Silk', version: 'Silk' },
      { name: 'Android', value: 'Android', version: 'Android' },
      { name: 'PlayBook', value: 'PlayBook', version: 'OS' },
      { name: 'BlackBerry', value: 'BlackBerry', version: '/' },
      { name: 'Macintosh', value: 'Mac', version: 'OS X' },
      { name: 'Linux', value: 'Linux', version: 'rv' },
      { name: 'Palm', value: 'Palm', version: 'PalmOS' }
    ],
    databrowser: [
      { name: 'Chrome', value: 'Chrome', version: 'Chrome' },
      { name: 'Firefox', value: 'Firefox', version: 'Firefox' },
      { name: 'Safari', value: 'Safari', version: 'Version' },
      { name: 'Internet Explorer', value: 'MSIE', version: 'MSIE' },
      { name: 'Opera', value: 'Opera', version: 'Opera' },
      { name: 'BlackBerry', value: 'CLDC', version: 'CLDC' },
      { name: 'Mozilla', value: 'Mozilla', version: 'Mozilla' }
    ],
    init: function () {
      var agent = this.header.join(' '),
          os = this.matchItem(agent, this.dataos),
          browser = this.matchItem(agent, this.databrowser);

      return { os: os, browser: browser };
    },
    matchItem: function (string, data) {
      var i = 0,
          j = 0,
          html = '',
          regex,
          regexv,
          match,
          matches,
          version;

      for (i = 0; i < data.length; i += 1) {
        regex = new RegExp(data[i].value, 'i');
        match = regex.test(string);
        if (match) {
          regexv = new RegExp(data[i].version + '[- /:;]([\\d._]+)', 'i');
          matches = string.match(regexv);
          version = '';
          if (matches) { if (matches[1]) { matches = matches[1]; } }
          if (matches) {
            matches = matches.split(/[._]+/);
            for (j = 0; j < matches.length; j += 1) {
              if (j === 0) {
                version += matches[j] + '.';
              } else {
                version += matches[j];
              }
            }
          } else {
            version = '0';
          }
          return {
            name: data[i].name,
            version: parseFloat(version)
          };
        }
      }
      return { name: 'unknown', version: 0 };
    }
  };

  var e = module.init(),
      debug = '';

  debug += 'os.name = ' + e.os.name + '<br/>';
  debug += 'os.version = ' + e.os.version + '<br/>';
  debug += 'browser.name = ' + e.browser.name + '<br/>';
  debug += 'browser.version = ' + e.browser.version + '<br/>';

  debug += '<br/>';
  debug += 'navigator.userAgent = ' + navigator.userAgent + '<br/>';
  debug += 'navigator.appVersion = ' + navigator.appVersion + '<br/>';
  debug += 'navigator.platform = ' + navigator.platform + '<br/>';
  debug += 'navigator.vendor = ' + navigator.vendor + '<br/>';


  //console.log(e);
  //console.log(navigator);
  $.ajax({
    type: 'get',
    url: 'modules/bodynova/bnsales_news/Core/deviceType.php?name='+e.os.name+'&browser='+e.browser.name+'&oxid=$oxid',
    success: function(result){
      //console.log(result);
    },
    error: function(e){
      //console.log('ERROR: ' + e);
    }
  });
}());"}]
    [{/if}]
    [{* Device Auswertung *}]

    [{* News }]
    [{if $oxcmp_news|count }]
    [{include file="widget/news.tpl" oNews=$oxcmp_news}]
    [{/if}]
    [{* News *}]
    [{oxifcontent ident="startseiteVCMS" object="oContent"}]
        [{$oContent->oxcontents__oxcontent->value}]
    [{/oxifcontent}]

    [{* Startseite Buttons *}]
    <div class="startbtns">
        [{oxifcontent ident="startneuheiten" object="oContent"}]
        [{$oContent->oxcontents__oxcontent->value}]
        [{/oxifcontent}]
    </div>
    [{* Startseite Buttons *}]

    [{* Promo Article not in Use *}]
    [{assign var="oFirstArticle" value=$oView->getFirstArticle()}]
    [{if $oView->getCatOfferArticleList()|@count > 0}]
    [{foreach from=$oView->getCatOfferArticleList() item=actionproduct name=CatArt}]
    [{if $smarty.foreach.CatArt.first}]
    [{assign var="oCategory" value=$actionproduct->getCategory()}]
    [{if $oCategory }]
    [{assign var="promoCatTitle" value=$oCategory->oxcategories__oxtitle->value}]
    [{assign var="promoCatImg" value=$oCategory->getPromotionIconUrl()}]
    [{assign var="promoCatLink" value=$oCategory->getLink()}]
    [{/if}]
    [{/if}]
    [{/foreach}]
    [{/if}]

    [{*if $oView->getBargainArticleList()|@count > 0 || ($promoCatTitle && $promoCatImg)}]
    <div class="promoBoxes clear">
        [{if count($oView->getBargainArticleList()) > 0 }]
        <div id="specBox" class="specBox">
            [{include file="widget/product/bargainitems.tpl"}]
        </div>
        [{/if}]
        [{if $promoCatTitle && $promoCatImg}]
        <div id="specCatBox" class="specCatBox">
            <h2 class="sectionHead">[{$promoCatTitle}]</h2>
            <a href="[{$promoCatLink}]" class="viewAllHover glowShadow corners"><span>[{ oxmultilang ident="VIEW_ALL_PRODUCTS" }]</span></a>
            <img src="[{$promoCatImg}]" alt="[{$promoCatTitle}]">
        </div>
        [{/if}]
    </div>
    [{/if*}]
    [{* Promo Article not in Use *}]

    [{*include file="widget/manufacturersslider.tpl" *}]
    [{* Neueste Artikel not in Use *}]
    [{if $oView->getNewestArticles() }]
    [{assign var='rsslinks' value=$oView->getRssLinks() }]
    [{include file="widget/product/list.tpl" type=$oViewConf->getViewThemeParam('sStartPageListDisplayType') head="JUST_ARRIVED"|oxmultilangassign listId="newItems" products=$oView->getNewestArticles() rsslink=$rsslinks.newestArticles rssId="rssNewestProducts" showMainLink=true}]
    [{/if}]
    [{* Neueste Artikel not in Use *}]



    [{*assign var="oConfig" value=$oViewConf->getConfig()}]
    [{assign var='rsslinks' value=$oView->getRssLinks()}]
    [{oxscript include="js/pages/start.min.js"}]

    [{block name="start_welcome_text"}]
        [{oxifcontent ident="oxstartwelcome" object="oCont"}]
            <div class="welcome-teaser">[{$oCont->oxcontents__oxcontent->value}]</div>
        [{/oxifcontent}]
    [{/block}]

    [{assign var="oTopArticles" value=$oView->getTop5ArticleList()}]

    [{block name="start_bargain_articles"}]
        [{assign var="oBargainArticles" value=$oView->getBargainArticleList()}]
        [{if $oBargainArticles && $oBargainArticles->count()}]
            [{include file="widget/product/list.tpl" type=$oViewConf->getViewThemeParam('sStartPageListDisplayType') head="START_BARGAIN_HEADER"|oxmultilangassign subhead="START_BARGAIN_SUBHEADER"|oxmultilangassign listId="bargainItems" products=$oBargainArticles rsslink=$rsslinks.bargainArticles rssId="rssBargainProducts" showMainLink=true iProductsPerLine=4}]
        [{/if}]
    [{/block}]

    [{block name="start_manufacturer_slider"}]
        [{if $oViewConf->getViewThemeParam('bl_showManufacturerSlider')}]
            [{include file="widget/manufacturersslider.tpl"}]
        [{/if}]
    [{/block}]

    [{block name="start_newest_articles"}]
        [{assign var="oNewestArticles" value=$oView->getNewestArticles()}]
        [{if $oNewestArticles && $oNewestArticles->count()}]
            [{include file="widget/product/list.tpl" type=$oViewConf->getViewThemeParam('sStartPageListDisplayType') head="START_NEWEST_HEADER"|oxmultilangassign subhead="START_NEWEST_SUBHEADER"|oxmultilangassign listId="newItems" products=$oNewestArticles rsslink=$rsslinks.newestArticles rssId="rssNewestProducts" showMainLink=true iProductsPerLine=4}]
        [{/if}]
    [{/block}]

    [{if $oNewestArticles && $oNewestArticles->count() && $oTopArticles && $oTopArticles->count()}]
        <div class="row">
            <hr>
        </div>
    [{/if}]

    [{block name="start_top_articles"}]
        [{if $oTopArticles && $oTopArticles->count()}]
            [{include file="widget/product/list.tpl" type="infogrid" head="START_TOP_PRODUCTS_HEADER"|oxmultilangassign subhead="START_TOP_PRODUCTS_SUBHEADER"|oxmultilangassign:$oTopArticles->count() listId="topBox" products=$oTopArticles rsslink=$rsslinks.topArticles rssId="rssTopProducts" showMainLink=true iProductsPerLine=2}]
        [{/if}]
    [{/block*}]

    [{insert name="oxid_tracker"}]
[{/capture}]
[{include file="layout/page.tpl"}]
