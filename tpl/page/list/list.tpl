[{assign var="actCategory" value=$oView->getActiveCategory()}]

[{capture append="oxidBlock_content"}]
    <div class="card" style="margin-bottom:180px;height:auto;">
        <div class="card-header">
            <h1 class="card-title"><strong>[{$oView->getTitle()}]</strong></h1>
            <div class="d-xl-none">
                [{if $oView->getClassName() eq "alist" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
            <div class="pull-right d-none d-xl-block" style="margin-top: -27px;">
                [{if $oView->getClassName() eq "alist" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
        </div>
        <div class="card-body">

            [{if $actCategory->oxcategories__oxthumb->value && $actCategory->getThumbUrl()}]
            <img src="[{$actCategory->getThumbUrl()}]" alt="[{$actCategory->oxcategories__oxtitle->value}]" class="categoryPicture">
            [{/if}]

            [{if $listType!='tag' && $actCategory && $actCategory->getShortDescription() }]
            <div class="categoryTopDescription" id="catDesc">[{$actCategory->getShortDescription()}]</div>
            [{/if}]
            [{if $actCategory->oxcategories__oxlongdesc->value }]
            <div class="categoryTopLongDescription" id="catLongDesc">[{oxeval var=$actCategory->oxcategories__oxlongdesc}]</div>
            [{/if}]

            [{if $oView->hasVisibleSubCats()}]

                <ul class="nav nav-pills list-inline">
                    [{foreach from=$oView->getSubCatList() item=category name=MoreSubCat}]
                        [{if $category->getIsVisible()}]
                        [{assign var="iconUrl" value=$category->getIconUrl()}]
                        <li class="label [{*active*}]">
                            <a id="moreSubCat_[{$smarty.foreach.MoreSubCat.iteration}]" class="kategorienSuchergebnis btn btn-lg" href="[{ $category->getLink() }]">
                                [{$category->oxcategories__oxtitle->value }][{if $oView->showCategoryArticlesCount() && ($category->getNrOfArticles() > 0) }] <span class="badge badge-pill badge-secondary">[{ $category->getNrOfArticles() }]</span>[{/if}]
                            </a>
                        </li>
                        [{/if}]
                    [{/foreach}]
                </ul>
            [{/if}]

            [{if $oView->getArticleList()|@count > 0}]

            <div class="listRefine clear bottomRound">
                [{include file="widget/locator/listlocator.tpl" locator=$oView->getPageNavigationLimitedTop() attributes=$oView->getAttributes() listDisplayType=true itemsPerPage=true sort=true }]
            </div>
            [{* List types: grid|line|infogrid *}]
            [{include file="widget/product/listNeu.tpl" type=$oView->getListDisplayType() listId="productList" products=$oView->getArticleList()}]
            [{*include file="widget/product/list.tpl" type=$oView->getListDisplayType() listId="productList" products=$oView->getArticleList()*}]
            [{include file="widget/locator/listlocator.tpl" locator=$oView->getPageNavigationLimitedBottom() place="bottom"}]
            [{/if}]

            [{insert name="oxid_tracker"}]
        </div>
    </div>
    [{/capture}]
[{include file="layout/page.tpl" sidebarLeft=1 sidebarRight=1 tree_path=$oView->getTreePath()}]
