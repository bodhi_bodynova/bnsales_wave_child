[{block name="checkout_steps_main"}]
    <div class="wizard" style="height:110px">
        <div class="wizard-inner" style="height:110px">
            <div class="connecting-line"></div>
            <ul class="nav nav-tabs" role="tablist" style="height:110px;border:none;">
                [{if $oxcmp_basket->getProductsCount()}]
                [{assign var=showStepLinks value=true}]
                [{/if}]
                [{block name="checkout_steps_basket"}]
                <li style="display: flex;align-items: center;justify-content: center;" class="nav-item step1[{if $active == 1}] active [{elseif $active > 1}] passed [{else}] disabled [{/if}]" >
                    <a  class="[{if $active == 1}] active [{elseif $active > 1}] passed [{else}] disabled [{/if}]" style="position:absolute; z-index:1;" href="[{if $showStepLinks}][{oxgetseourl ident=$oViewConf->getBasketLink()}][{else}]#[{/if}]"  aria-controls="step1" role="tab" title="[{oxmultilang ident="STEPS_BASKET"}]">
                        <!--<a rel="nofollow" href="[{if $showStepLinks}][{oxgetseourl ident=$oViewConf->getBasketLink()}][{else}]#[{/if}]">-->
                        [{*oxmultilang ident="STEPS_BASKET"*}]
                        <span class="round-tab">
								<i class="fa fa-shopping-cart"></i>
                        </span>
                    </a>
                </li>
                [{/block}]

                [{assign var=showStepLinks value=false}]
                [{if !$oView->isLowOrderPrice() && $oxcmp_basket->getProductsCount()}]
                [{assign var=showStepLinks value=true}]
                [{/if}]

                [{block name="checkout_steps_send"}]
                <li style="display: flex;align-items: center;justify-content: center;" class="nav-item step2[{if $active == 2}] active [{elseif $active > 2}] passed [{else}] disabled [{/if}]" role="presentation">
                    <a class=" [{if $active == 2}] active [{elseif $active > 2}] passed [{else}] disabled [{/if}]" style="position:absolute;z-index:1;" rel="nofollow" href="[{if $showStepLinks}][{oxgetseourl ident=$oViewConf->getOrderLink()}][{else}]#[{/if}]" aria-controls="step2" role="tab" title="[{oxmultilang ident="STEPS_SEND"}]">
                        <span class="round-tab">
                            <i class="fa fa-pencil"></i>
                        </span>
                    </a>
                </li>
                [{/block}]

                [{assign var=showStepLinks value=false}]
                [{if $active != 1 && $oxcmp_user && !$oView->isLowOrderPrice() && $oxcmp_basket->getProductsCount()}]
                [{assign var=showStepLinks value=true}]
                [{/if}]

                [{*
				[{block name="checkout_steps_pay"}]
					<li class="step3[{if $active == 3}] active [{elseif $active > 3}] passed [{/if}]">
						<a rel="nofollow" [{if $oViewConf->getActiveClassName() == "user"}]id="paymentStep"[{/if}] href="[{if $showStepLinks}][{oxgetseourl ident=$oViewConf->getPaymentLink()}][{else}]#[{/if}]">
							[{oxmultilang ident="STEPS_PAY"}]
						</a>
					</li>
					[{oxscript add="$('#paymentStep').click( function() { $('#userNextStepBottom').click();return false;});"}]
				[{/block}]

				$oView->getPaymentList() &&
				*}]

                [{assign var=showStepLinks value=false}]
                [{if $active != 1 && $oxcmp_user && $oxcmp_basket->getProductsCount() && !$oView->isLowOrderPrice()}]
                [{assign var=showStepLinks value=true}]
                [{/if}]

                [{block name="checkout_steps_order"}]
                <li style="display: flex;align-items: center;justify-content: center;" class="nav-item step4[{if $active == 4}] active [{elseif $active > 4}] passed [{else}] disabled [{/if}]" role="presentation">
                    <a class="[{if $active == 4}] active [{elseif $active > 4}] passed [{else}] disabled [{/if}]" style="position:absolute;z-index:1;" rel="nofollow" [{if $oViewConf->getActiveClassName() == "payment"}]id="orderStep"[{/if}] href="[{if $showStepLinks}][{if $oViewConf->getActiveClassName() == "payment"}]javascript:document.forms.order.submit();[{else}][{oxgetseourl ident=$oViewConf->getOrderConfirmLink()}][{/if}][{else}]#[{/if}]"  aria-controls="step4" role="tab" title="[{oxmultilang ident="STEPS_ORDER"}]">
                        <span class="round-tab">
                            <i class="fa fa-search"></i>
                        </span>
                    </a>
                </li>
                [{oxscript add="$('#orderStep').click( function() { $('#paymentNextStepBottom').click();return false;});"}]
                [{/block}]

                [{block name="checkout_steps_laststep"}]
                <li style="display: flex;align-items: center;justify-content: center;" class="nav-item step5[{if $active == 5}] activeLast [{else}] defaultLast disabled[{/if}] " role="presentation">
                    <a class="[{if $active == 5}] activeLast [{else}] defaultLast disabled[{/if}]" style="position:absolute;z-index:1;" href="#"  aria-controls="step5" role="tab" title="[{oxmultilang ident="READY"}]">
                        <span class="round-tab">
                            <i class="fa fa-check"></i>
                        </span>
                    </a>
                </li>
                [{/block}]
            </ul>
        </div>
    </div>
    [{/block}]


[{*block name="checkout_steps_main"}]
    <ol class="checkoutSteps checkout-steps row">
        [{if $oxcmp_basket->getProductsCount()}]
            [{assign var=showStepLinks value=true}]
        [{/if}]

        [{block name="checkout_steps_basket"}]
            <li class="step step1[{if $active == 1}] active [{elseif $active > 1}] passed [{/if}]">
                [{if $showStepLinks}]<a href="[{oxgetseourl ident=$oViewConf->getBasketLink()}]">[{/if}]
                <div class="num">1</div>
                <div class="text">
                    [{oxmultilang ident="STEPS_BASKET"}]
                </div>
                [{if $showStepLinks}]</a>[{/if}]
            </li>
        [{/block}]

        [{assign var=showStepLinks value=false}]
        [{if !$oView->isLowOrderPrice() && $oxcmp_basket->getProductsCount()}]
            [{assign var=showStepLinks value=true}]
        [{/if}]

        [{block name="checkout_steps_send"}]
            <li class="col step step2[{if $active == 2}] active [{elseif $active > 2}] passed [{/if}]">
                [{if $showStepLinks}]<a href="[{oxgetseourl ident=$oViewConf->getOrderLink()}]">[{/if}]
                <div class="num">2</div>
                <div class="text">
                    [{oxmultilang ident="STEPS_SEND"}]
                </div>
                [{if $showStepLinks}]</a>[{/if}]
            </li>
        [{/block}]

        [{assign var=showStepLinks value=false}]
        [{if $active != 1 && $oxcmp_user && !$oView->isLowOrderPrice() && $oxcmp_basket->getProductsCount()}]
            [{assign var=showStepLinks value=true}]
        [{/if}]

        [{block name="checkout_steps_pay"}]
            <li class="col step step3[{if $active == 3}] active [{elseif $active > 3}] passed [{/if}]">
                [{if $showStepLinks}]<a [{if $oViewConf->getActiveClassName() == "user"}]id="paymentStep"[{/if}] href="[{oxgetseourl ident=$oViewConf->getPaymentLink()}]">[{/if}]
                <div class="num">3</div>
                <div class="text">
                    [{oxmultilang ident="STEPS_PAY"}]
                </div>
                [{if $showStepLinks}]</a>[{/if}]
            </li>
            [{*[{oxscript add="$('#paymentStep').click( function() { $('#userNextStepBottom').click();return false;});"}]}]TODO:*
        [{/block}]

        [{assign var=showStepLinks value=false}]
        [{if $active != 1 && $oxcmp_user && $oxcmp_basket->getProductsCount() && $oView->getPaymentList() && !$oView->isLowOrderPrice()}]
            [{assign var=showStepLinks value=true}]
        [{/if}]

        [{block name="checkout_steps_order"}]
            <li class="col step step4[{if $active == 4}] active [{elseif $active > 4}] passed [{/if}]">
                [{if $showStepLinks}]<a [{if $oViewConf->getActiveClassName() == "payment"}]id="orderStep"[{/if}] href="[{if $oViewConf->getActiveClassName() == "payment"}]javascript:document.forms.order.submit();[{else}][{oxgetseourl ident=$oViewConf->getOrderConfirmLink()}][{/if}]">[{/if}]
                <div class="num">4</div>
                <div class="text">
                    [{oxmultilang ident="STEPS_ORDER"}]
                </div>
                [{if $showStepLinks}]</a>[{/if}]
            </li>
            [{*[{oxscript add="$('#orderStep').click( function() { $('#paymentNextStepBottom').click();return false;});"}]}]TODO:*
        [{/block}]

        [{block name="checkout_steps_laststep"}]
            <li class="col step step5[{if $active == 5}] activeLast [{else}] defaultLast [{/if}] last">
                <div class="num">5</div>
                <div class="text">
                    [{oxmultilang ident="READY"}]
                </div>
            </li>
        [{/block}]
    </ol>
    <div class="clearfix"></div>
    <div class="spacer"></div>
[{/block*}]