[{capture append="oxidBlock_content"}]
    <div id="userTPL" style="margin-bottom:180px; height:auto; padding-bottom:10px;" class="card">
        <div class="card-header">
            <h1 class="card-title"><strong>[{*$oView->getTitle()*}][{oxmultilang ident="ADRESSE"}]</strong></h1>
            <div class="pull-right" style="margin-top: -27px;">
                [{if $oView->getClassName() eq "user" && !$blHideBreadcrumb}]
                [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
        </div>
        <div class="card-body">
            [{* ordering steps *}]
            [{include file="page/checkout/inc/steps.tpl" active=2 }]

            [{block name="checkout_user_main"}]
            [{if !$oxcmp_user && !$oView->getLoginOption() }]
            [{include file="page/checkout/inc/options.tpl"}]
            [{/if}]

            [{block name="checkout_user_noregistration"}]
            [{if !$oxcmp_user && $oView->getLoginOption() == 1}]
            [{include file="form/user_checkout_noregistration.tpl"}]
            [{/if}]
            [{/block}]

            [{block name="checkout_user_registration"}]
            [{if !$oxcmp_user && $oView->getLoginOption() == 3}]
            [{include file="form/user_checkout_registration.tpl"}]
            [{/if}]
            [{/block}]

            [{block name="checkout_user_change"}]
            [{if $oxcmp_user}]
            [{include file="form/user_checkout_change.tpl"}]
            [{/if}]
            [{/block}]
            [{/block}]
        </div>
    </div>
    [{/capture}]
[{include file="layout/page.tpl" sidebarLeft="1" sidebarRight="1"}]