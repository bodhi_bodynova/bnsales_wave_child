[{assign var=oBanners value=$oView->getBanners() }]
[{assign var="currency" value=$oView->getActCurrency()}]
[{if $oBanners|@count}]
    [{*oxscript include="js/sliderengine/jquery.js" priority=10 *}]
    [{oxscript include="js/sliderengine/amazingslider.js" priority=10 }]
    [{oxstyle include="js/sliderengine/amazingslider-1.css"}]
    [{oxscript include="js/sliderengine/initslider-1.js" priority=10 }]
    [{*oxscript include="js/widgets/oxslider.js" priority=10 }]
	[{oxscript add="$( '#promotionSlider' ).oxSlider();"*}]
    [{* width:942px; max-height:180px;*}]
    <div id="amazingslider-wrapper-1" style="display:block;position:relative;width:100%; margin:0 auto 0;">
        <div id="amazingslider-1" style="display:block;position:relative;margin:auto;">
            <ul class="amazingslider-slides [{*$oBanners|@count*}]" style="display:none;">
                [{foreach from=$oBanners item=oBanner }]
                [{assign var=oArticle value=$oBanner->getBannerArticle() }]
                <li>
                    [{assign var=sBannerLink value=$oBanner->getBannerLink() }]
                    [{if $sBannerLink }]
                    <a href="[{$sBannerLink}]">
                        [{/if}]
                        [{*if $oArticle}]
								[{assign var="sFrom" value=""}]
								[{assign var="oPrice" value=$oArticle->getPrice()}]
								[{if $oArticle->isParentNotBuyable() }]
									[{assign var="oPrice" value=$oArticle->getVarMinPrice()}]
									[{if $oArticle->isRangePrice() }]
										[{assign var="sFrom" value="PRICE_FROM"|oxmultilangassign}]
									[{/if}]
								[{/if}]
							[{/if*}]
                        [{assign var=sBannerPictureUrl value=$oBanner->getBannerPictureUrl() }]
                        [{if $sBannerPictureUrl }]
                    <img src="[{$sBannerPictureUrl}]" alt="[{*$oBanner->oxactions__oxtitle->value*}]"  title="[{*if $oArticle}][{$oArticle->oxarticles__oxtitle->value}][{else}][{$oBanner->oxactions__oxtitle->value}][{/if*}]" data-description="[{*if $oArticle}][{$oArticle->oxarticles__oxtitle->value}][{$sFrom}][{oxprice price=$oPrice currency=$currency }][{else}][{$oBanner->oxactions__oxtitle->value}][{/if*}]" [{*}]data-duration="5000"[{*}]/>
                        [{/if}]
                        [{if $sBannerLink}]
                    </a>
                    [{/if}]
                </li>
                [{/foreach}]
                [{*}]
				<li><img src="images/volatile_praxisbedarf_massageoel_gruppe.jpg" alt="Massageöl »Energy« 100 ml"  title="Massageöl »Energy« 100 ml" data-description="Rosmarin und Eukalyptus" data-duration="5000" />
				</li>
				<li><a href="images/lightboxbilder/volatile_praxisbedarf_massageoel_gruppe%20%281%29-lightbox.jpg" class="html5lightbox" data-width="960" data-height="720"><img src="images/volatile_praxisbedarf_massageoel_gruppe%20%281%29.jpg" alt="Massageöl »Nacken-Schulter« 100 ml"  title="Massageöl »Nacken-Schulter« 100 ml" data-description="Geheimtipp unter Therapeuten" /></a>
				</li>
				<li><a href="images/lightboxbilder/psdg_34934-lightbox.jpg" class="html5lightbox" data-width="960" data-height="720"><img src="images/psdg_34934.jpg" alt="Meditationstuch beige/grau gemustert"  title="Meditationstuch beige/grau gemustert" data-description="sehr weich, 100% Baumwolle" data-texteffect="Bottom bar" data-duration="5000" /></a>
				</li>
				<li><a href="images/lightboxbilder/luna_trkis_img_7621-lightbox.jpg" class="html5lightbox" data-width="960" data-height="720"><img src="images/luna_trkis_img_7621.jpg" alt="luna_trkis_img_7621"  title="luna_trkis_img_7621" /></a>
				</li>
				[{*}]
            </ul>
        </div>
    </div>
    [{/if}]
