[{if $products|@count gt 0}]

<div class="table-responsive">
    [{* TODO: mobile Ansicht *}]
    [{* TODO: mobile Ansicht *}]
    [{*if $oxcmp_user->oxuser__oxrights->value eq 'malladmin'*}]

    [{*/if*}]
    <table id="[{$listId}]" class="table table-condensed" [{if $products|@count eq 1}] style="height:105px;"[{/if}]>
        <thead>
        <tr>
            <th class="d-none d-sm-none d-md-block"></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        [{assign var="currency" value=$oView->getActCurrency()}]
        [{foreach from=$products item=_product name=productlist}]
            [{assign var="_sTestId" value=$listId|cat:"_"|cat:$smarty.foreach.productlist.iteration}]
            [{oxid_include_widget
                cl="oxwArticleBox"
                _parent=$oView->getClassName()
                nocookie=1
                _navurlparams=$oViewConf->getNavUrlParams()
                iLinkType=$_product->getLinkType()
                _object=$_product
                anid=$_product->getId()
                sWidgetType=product
                sListType=listitem_lineNeu
                iIndex=$_sTestId
                blDisableToCart=$blDisableToCart
                isVatIncluded=$oView->isVatIncluded()
                showMainLink=$showMainLink
                recommid=$recommid
                owishid=$owishid
                toBasketFunction=$toBasketFunction
                removeFunction=$removeFunction
                altproduct=$altproduct
                inlist=$_product->isInList()
                skipESIforUser=1
                }]
            [{/foreach}]
        </tbody>
    </table>
</div>
    <button type="button" class="btn btn-warenkorb ladda-button btn-outline-success pull-right bntooltip" data-style="expand-right" onclick='allItemsIntoTheBasket("[{oxmultilang ident='WK-OK'}]","[{oxmultilang ident='WK-NO-ART'}]")' data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="TIP_TO_THE_BASKET"}]"><i style="vertical-align:super;" class="fa fa-shopping-cart"></i> <i style="vertical-align:super;" class="fa fa-shopping-cart"></i>[{*}]<span class="ladda-label">[{oxmultilang ident="TO_THE_BASKET"}]</span>[{*}]</button>
    [{if $isorder}]
    <button type="button" class="btn btn-warenkorb ladda-button btn-outline-success pull-right bntooltip resetAll" data-style="expand-right" onclick="resetBasketSelection()" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="TIP_RESET_AMOUNT"}]"><span style="vertical-align:super;" class="fa fa-refresh"> [{oxmultilang ident="RESET_AMOUNT"}]</span></button>
    [{/if}]
[{/if}]
[{*debug*}]
