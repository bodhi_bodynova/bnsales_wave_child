[{if $products|@count gt 0}]

    [{*<button type="button" class="btn btn-outline-primary bntooltip" data-style="expand-right" onclick="openAllVariants()" data-placement="top" title="[{oxmultilang ident="OPEN_ALL_VARIANTS"}]"><i class="fa fa-expand"></i></button>*}]
    [{* PDF erstell Link *}]
    [{*if $oView->getClassName() !== "search"}]
    <a href="[{$oViewConf->getSelfLink()}]cl=pricelist&fnc=getPDF&cat=[{$actCategory->oxcategories__oxid->value}]&name=[{$actCategory->oxcategories__oxtitle->value}]" type="button" class="btn btn-outline-primary bntooltip" data-style="expand-right" title="[{oxmultilang ident="PRINT_PDF"}]" ><i class="fa fa-file-text-o"></i></a>
    [{/if*}]


    [{*$actCategory->oxcategories__oxtitle->value*}]
    [{*$oView|var_dump*}]
    <div class="row">
        <div class="col-1" id="Bilder"></div>
        <div class="col-4" id="titelbuttons"></div>
        <div class="col-1" id="wkbuttons"></div>
        <div class="col-1" id="preisStockFahne"></div>
    </div>


    <table id="[{$listId}]" class="table">
        <!--<thead>
            <tr>
                <th class="hidden-xs"></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>-->
        <tbody>
        [{assign var="currency" value=$oView->getActCurrency() }]
        [{foreach from=$products item=_product name=productlist}]
            [{assign var="_sTestId" value=$listId|cat:"_"|cat:$smarty.foreach.productlist.iteration}]
            [{oxid_include_widget
        cl="oxwArticleBox"
        _parent=$oView->getClassName()
        nocookie=1
        _navurlparams=$oViewConf->getNavUrlParams()
        iLinkType=$_product->getLinkType()
        _object=$_product
        anid=$_product->getId()
        sWidgetType=product
        sListType=listitem_$type
        iIndex=$_sTestId
        blDisableToCart=$blDisableToCart
        isVatIncluded=$oView->isVatIncluded()
        showMainLink=$showMainLink
        recommid=$recommid
        owishid=$owishid
        toBasketFunction=$toBasketFunction
        removeFunction=$removeFunction
        altproduct=$altproduct
        inlist=$_product->isInList()
        skipESIforUser=1
        }]
            [{/foreach}]
        </tbody>
    </table>

    [{*}]
    <button type="button" class="btn btn-default btn-warenkorb btn-prima btn-basket ladda-button btn-success pull-right" data-style="expand-right" onclick="allItemsIntoTheBasket()" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="TIP_TO_THE_BASKET"}]"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;<span class="glyphicon glyphicon-shopping-cart"></span><span class="ladda-label">[{oxmultilang ident="TO_THE_BASKET"}]</span></button>

    [{if $isorder}]
	    <button type="button" class="btn btn-default btn-warenkorb btn-prima btn-basket ladda-button btn-warning pull-right" data-style="expand-right" onclick="resetBasketSelection()" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="TIP_RESET_AMOUNT"}]"><span class="glyphicon glyphicon-refresh"></span>&nbsp;[{oxmultilang ident="RESET_AMOUNT"}]</button>
    [{/if}]
	[{*}]
    [{/if}]
[{*debug*}]
