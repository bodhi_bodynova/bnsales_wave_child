[{* Väter und Standart *}]
[{if $product}]
    [{if $_product}]
    [{assign var="_priceUpdate" value=$_product->getUpdatePreis()}]
    [{* Lieferzeit der Varianten beim Vater anzeigen *}]
    [{if $_product->getmindeltime() != 0 || $_product->getmaxdeltime() != 0 }]

    [{*$_product|var_dump*}]

    [{* Standartartikel *}]
    [{if $_product->isBuyable()}]
    [{block name="bnsales_stockinfo"}]
    <div class="stockFlagBox [{if $_product->showCssFlag()}][{$_product->showCssFlag()}][{/if}]" style="top:-3px"></div>
    [{/block}]
    [{if $_priceUpdate > 0 && $_priceUpdate != $_product->getPreisAktuell()}]<i style="color:gray" class="fas fa-chart-line fa-2x bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="neupreisinfo"}] [{$_product->oxarticles__oxupdatepricetime->value|date_format:"%d.%m.%Y"}]"><span style="font-size:15px">[{oxprice price=$_priceUpdate currency=$oView->getActCurrency()}]</span></i>[{/if}]
    <div class="ProduktionszeitText">
        [{if $_product->getmindeltimeS() > 0}]
        [{if $_product->getBnFlag() !='0'}]
        [{oxmultilang ident="DELIVERYTIME_DELIVERYTIME"}] [{$_product->getmindeltimeS()}]-[{$_product->getmaxdeltimeS()}] [{oxmultilang ident="TAGE"}]
        [{/if}]
        [{/if}]
    </div>

    [{*$_product->getDeliveryDate()|var_dump*}]

    [{* Lieferbar ab:  Beim Vater keine Ampel aber bei den Varianten Ampel mit Lieferdatum.*}]
    <div style="white-space:nowrap; margin-left: -30px">
        [{if $_product->getDeliveryDate()}]
        [{if $_product->getBnFlag() == '2'}]
        [{oxmultilang ident="AVAILABLE_ON"}] [{$_product->getDeliveryDate()|date_format:"%d.%m.%Y"}]
        [{/if}]
        [{/if}]
    </div>


    [{/if}]

    [{* Stockflag *}]
    [{*}]<div class="stockFlagBox [{if $_product->showCssFlag()}][{$_product->showCssFlag()}][{/if}]"></div>[{*}]

    [{* Lieferdauer Anfertigungszeit Nur beim Vater nicht bei Varianten bei Varianten keine Ampel anzeigen auch keinen Grünen
                *}]

    [{* Variantenväter *}]
    [{if !$_product->isBuyable()}]
    [{if $_product->getmindeltimeS() > 0}]

    [{* Stockflag *}]
    [{block name="bnsales_stockinfo"}]
    <div class="stockFlagBox [{if $_product->showCssFlag()}][{$_product->showCssFlag()}][{/if}]" style="top:-3px"></div>
    [{/block}]
    [{if $_priceUpdate > 0  && $_priceUpdate != $_product->getPreisAktuell()}]<span style="color:gray" class="fas fa-chart-line fa-2x bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="neupreisinfo"}] [{$_product->oxarticles__oxupdatepricetime->value|date_format:"%d.%m.%Y"}]"><span style="font-size:15px">[{oxprice price=$_priceUpdate currency=$oView->getActCurrency()}]</span></span>[{/if}]
    <div class="ProduktionszeitText">[{oxmultilang ident="DELIVERYTIME_DELIVERYTIME"}] [{$_product->getmindeltimeS()}]-[{$_product->getmaxdeltimeS()}] [{oxmultilang ident="TAGE"}]</div>

    [{/if}]

    [{* Lieferbar ab:  Beim Vater keine Ampel aber bei den Varianten Ampel mit Lieferdatum.*}]
    [{if $_product->getDeliveryDate()}]
    [{* Stockflag *}]
    <div class="stockFlagBox [{if $_product->showCssFlag()}][{$_product->showCssFlag()}][{/if}]" style="top:-3px"></div>
    [{if $_priceUpdate > 0  && $_priceUpdate != $_product->getPreisAktuell()}]<span style="color:gray" class="fas fa-chart-line fa-2x bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="neupreisinfo"}] [{$_product->oxarticles__oxupdatepricetime->value|date_format:"%d.%m.%Y"}]"><span style="font-size:15px">[{oxprice price=$_priceUpdate currency=$oView->getActCurrency()}]</span></span>[{/if}]
    <div style="white-space:nowrap; margin-left: -30px">
        [{if $_product->getBnFlag() == '2'}]
        [{oxmultilang ident="AVAILABLE_ON"}] [{$_product->getDeliveryDate()|date_format:"%d.%m.%Y"}]
        [{/if}]
    </div>
    [{/if}]
    [{/if}]
    [{/if}]

    [{/if}]



    [{* Anzeige bei den Varianten *}]
    [{else}]

    [{* Wenn das Array gefüllt ist: *}]
    [{if $_aProduct}]
    [{assign var="_aPriceUpdate" value=$_aProduct->getUpdatePreis()}]
    [{* Wenn die Mindest oder Maximallieferzeit gleich null ist *}]
    [{if $_aProduct->oxarticles__oxmindeltime->value == 0 || $_aProduct->oxarticles__oxmaxdeltime->value == 0}]

    [{*$_aProduct|@debug_print_var*}]

    [{* Stockflag *}]
    [{block name="bnsales_stockinfo"}]
    [{if $_aPriceUpdate > 0  && $_aPriceUpdate != $_aProduct->getPreisAktuell()}]
    <div class="stockFlagBox [{if $_aProduct->showCssFlag()}][{$_aProduct->showCssFlag()}][{/if}]" style="top:-3px"></div>
    <span style="color:gray" class="fas fa-chart-line fa-2x bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="neupreisinfo"}] [{$_aProduct->oxarticles__oxupdatepricetime->value|date_format:"%d.%m.%Y"}]"><span style="font-size:15px">[{oxprice price=$_aPriceUpdate currency=$oView->getActCurrency()}]</span></span>
    [{else}]
    <div class="stockFlagBox [{if $_aProduct->showCssFlag()}][{$_aProduct->showCssFlag()}][{/if}]"></div>
    [{/if}]
    [{/block}]

    [{* Lieferbar ab: nur anzeigen wenn Ampel rot*}]
    <div style="white-space:nowrap; margin-left: -30px">
        [{if $_aProduct->getDeliveryDate() && $_aProduct->showCssFlag() == "notOnStock"}]
        [{oxmultilang ident="AVAILABLE_ON"}] [{$_aProduct->getDeliveryDate()|date_format:"%d.%m.%Y"}]
        [{/if}]
    </div>
    [{else}]
    [{if $_aPriceUpdate > 0 && $_aPriceUpdate != $_aProduct->getPreisAktuell()}]<span style="color:gray" class="fas fa-chart-line fa-2x bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="neupreisinfo"}] [{$_aProduct->oxarticles__oxupdatepricetime->value|date_format:"%d.%m.%Y"}]"><span style="font-size:15px">[{oxprice price=$_aPriceUpdate currency=$oView->getActCurrency()}]</span></span>[{/if}]
    [{/if}]
    [{/if}]
    [{/if}]

