[{block name="widget_product_listitem_line"}]
    [{assign var="product"              value=$oView->getProduct()                          }]
    [{assign var="owishid"              value=$oView->getWishId()                           }]
    [{assign var="removeFunction"       value=$oView->getRemoveFunction()                   }]
    [{assign var="recommid"             value=$oView->getRecommId()                         }]
    [{assign var="iIndex"               value=$oView->getIndex()                            }]
    [{assign var="showMainLink"         value=$oView->getShowMainLink()                     }]
    [{assign var="blDisableToCart"      value=$oView->getDisableToCart()                    }]
    [{assign var="toBasketFunction"     value=$oView->getToBasketFunction()                 }]
    [{assign var="altproduct"           value=$oView->getAltProduct()                       }]
    [{assign var="aVariantSelections"   value=$product->getVariantSelections(null,null,1)   }]

    [{assign var="preis" value=$product->getPreisAktuell()}]
    [{assign var="streichpreis" value=$product->getUserStreichpreis()}]
    [{assign var="preisaktionbeginn" value=$product->getaktionsbeginn()}]
    [{assign var="preisaktionende" value=$product->getaktionsende()}]


    [{if $showMainLink}]
    [{assign var='_productLink' value=$product->getMainLink()}]
    [{else}]
    [{assign var='_productLink' value=$product->getLink()}]
    [{/if}]
    [{*if $oxcmp_user}]
        [{assign var="preisgruppe" value=$oxcmp_user->getUserPriceData()}]
        [{if $preisgruppe == 'a'}]    [{assign var="preisaktion" value=$product->oxarticles__oxpricea_aktionspreis->value}][{assign var="preisaktionbeginn" value=$product->oxarticles__oxpricea_aktionsbeginn->value}][{assign var="preisaktionende" value=$product->oxarticles__oxpricea_aktionsende->value}]
            [{elseif $preisgruppe == 'b'}]    [{assign var="preisaktion" value=$product->oxarticles__oxpriceb_aktionspreis->value}][{assign var="preisaktionbeginn" value=$product->oxarticles__oxpriceb_aktionsbeginn->value}][{assign var="preisaktionende" value=$product->oxarticles__oxpriceb_aktionsende->value}]
            [{elseif $preisgruppe == 'c'}]    [{assign var="preisaktion" value=$product->oxarticles__oxpricec_aktionspreis->value}][{assign var="preisaktionbeginn" value=$product->oxarticles__oxpricec_aktionsbeginn->value}][{assign var="preisaktionende" value=$product->oxarticles__oxpricec_aktionsende->value}]
            [{elseif $preisgruppe == 'd'}]    [{assign var="preisaktion" value=$product->oxarticles__oxpriced_aktionspreis->value}][{assign var="preisaktionbeginn" value=$product->oxarticles__oxpriced_aktionsbeginn->value}][{assign var="preisaktionende" value=$product->oxarticles__oxpriced_aktionsende->value}]
            [{elseif $preisgruppe == 'e'}]    [{assign var="preisaktion" value=$product->oxarticles__oxpricee_aktionspreis->value}][{assign var="preisaktionbeginn" value=$product->oxarticles__oxpricee_aktionsbeginn->value}][{assign var="preisaktionende" value=$product->oxarticles__oxpricee_aktionsende->value}]
            [{elseif $preisgruppe == 'f'}]    [{assign var="preisaktion" value=$product->oxarticles__oxpricef_aktionspreis->value}][{assign var="preisaktionbeginn" value=$product->oxarticles__oxpricef_aktionsbeginn->value}][{assign var="preisaktionende" value=$product->oxarticles__oxpricef_aktionsende->value}]
        [{/if}]
    [{/if*}]

    [{*TODO: ID unique, alles anpassen*}]
    <div id="article_[{$product->oxarticles__oxid|replace:".":"_"}]" class="tabellenspalte d-xl-none">
        <hr style="border-top:3px solid rgba(0,0,0,.1)">

        <div class="row">
             <div class="col-4">
                 <a name="[{$product->getID()|replace:".":"_"}]"></a>
                 [{assign var="oConfig" value=$oViewConf->getConfig()}]
                 [{if $oConfig->getConfigParam('CDN')}]
                 <div class="pictureBox">
                     <a href="[{*$_productLink*}]#" onclick="loadArticleDetails('[{$product->oxarticles__oxid}]')" class="viewAllHover glowShadow corners" title="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
                         [{if $product->oxarticles__oxpic1 != ''}]

                     <img data-toggle="" src="https://cdn.bodynova.de/out/pictures/generated/product/1/1000_1000_75/[{$product->oxarticles__oxpic1->value}]" alt="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]" class="img-thumbnail">
                         [{else}]

                     <img data-toggle="" src="https://bodynova.de/out/imagehandler.php?artnum=[{$product->oxarticles__oxartnum}]&size=1000_1000_75" class="img-thumbnail">
                         [{/if}]
                     </a>
                 </div>
                 [{* pictureBox VOR CDN*}]
                 [{else}]

                 <div class="pictureBox">
                     <a href="[{*$_productLink*}]#" onclick="loadArticleDetails('[{$product->oxarticles__oxid}]')" class="viewAllHover glowShadow corners" title="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
                         [{*$product->getThumbnailUrl()*}]
                         [{if $product->oxarticles__oxpic1 != ''}]

                     <img class="lazy-img img-thumbnail" src="[{$oViewConf->getImageUrl('spinner.gif')}]" data-src="[{$product->getThumbnailUrl()}]" alt="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
                         [{else}]
                     <img class="lazy-img img-thumbnail" src="[{$oViewConf->getImageUrl('spinner.gif')}]" data-src="https://bodynova.de/out/imagehandler.php?artnum=[{$product->oxarticles__oxartnum}]&size=390_245_75" alt="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
                         [{/if}]
                     </a>
                 </div>
                 [{/if}]
             </div>
             <div class="col-8">
                 <div>
                     <span class="headline">
                        [{*$product|var_dump*}]
                        <b>
                            [{if $product->isBuyable()}]
                                [{$product->oxarticles__oxtitle->value }]
                            [{else}]
                                [{$product->oxarticles__oxtitle->value }] [{$product->oxarticles__oxvarselect->value}]
                            [{/if}]
                        </b>
			        </span>
                 </div>
                 <div class="smallFont">
                     [{oxmultilang ident="ARTNR" suffix="COLON"}] [{$product->oxarticles__oxartnum}]
                     [{if $product->oxarticles__verpackungseinheit->value ne '0' and $product->oxarticles__verpackungseinheit->value > 0}]
                      <br>
                      <span style="margin:0;cursor:default" class="bntooltip" data-toggle="tooltip" data-original-title=[{oxmultilang ident="VETooltip"}]>
                        [{oxmultilang ident="VERPACKUNGSEINHEIT" suffix="COLON"}] [{$product->oxarticles__verpackungseinheit->value}]
                      </span>
                     [{/if}]
                 </div>

             </div>
            <div class="col-sm-4 col-5 offset-2 offset-sm-4" style="margin-top:5px">
                <form>
                    [{* Info Button *}]
                    <button style="height:38px; width:40px;margin-left:0 !important; margin-right:5px;" type="button" class="btn btn-heart-mobile bntooltip" data-style="expand-right" onclick="loadArticleDetails('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="SHOW_ARTICLE_DETAILS"}]"><span class="fa fa-info-circle"></span></button>
                    [{* Herz Button *}]
                    <button style="height:38px; width:40px;margin-right:5px;" type="button" class="btn-favorite btn [{if $product->isFavorite()}]btn-outline-primary[{/if}] bntooltip" data-style="expand-right" onclick="toggleFavoritEntry('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="ADD_TO_FAVORITE" }]"><span class="fa fa-heart-o"></span></button>
                </form>
                <br>
                [{* Rückrufaktion *}]
                [{if $product->oxarticles__bnflagrecall->value == 1}]<button style="margin-top:5px;height:38px; width:40px;margin-right:5px; margin-left:0 !important;" type="button" class="btn btn-outline-danger bntooltip" data-toggle="tooltip" data-placement="top" title="[{$product->oxarticles__recallreason->value}]"><i class="fa fa-exclamation-triangle"></i></button>[{/if}]

                [{* Aktionsartikel *}]
                [{*if $product->isAktionbeendet() }]<button style="margin-top:5px;height:38px; width:40px;margin-right:5px;margin-left:5px" type="button" class="btn btn-outline-success bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AKTIONBIS"}][{$product->oxarticles__aktionsende->value|date_format:"%d.%m.%Y"}]"><i class="fas fa-euro-sign"></i></button>[{/if*}]

                [{if $preisaktionbeginn <= $smarty.now|date_format:"%Y-%m-%d" && $preisaktionende > $smarty.now|date_format:"%Y-%m-%d" && $streichpreis >0}]
                <button style="margin-top:5px;height:38px; width:40px;margin-right:5px;margin-left:5px" type="button" class="btn btn-outline-success bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AKTIONBIS"}][{$preisaktionende|date_format:"%d.%m.%Y"}]"><i class="fas fa-euro-sign"></i></button>
                [{/if}]

                [{* Auslaufartikel *}]
                [{if $product->oxarticles__bnflagauslaufartikel->value == 1}]<button style="margin-top:5px;height:38px; width:40px;margin-right:5px;margin-left:5px" type="button" class="btn-auslauf btn btn-outline-warning bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AUSLAUFARTIKEL"}]"><i class="fa fa-exclamation-triangle"></i></button>[{/if}]


            </div>
             <div class="col-sm-4 col-5" [{if $product->isBuyable()}] style="margin-top:5px"[{/if}]>

                 <div class="text-right">

                     [{*if $product->isBuyable()}]
                     <input  min="0" class="form-control amount" [{* type="number"}]inputmode="numeric" onchange="updateAmount(this,'amount[[{$product->oxarticles__oxid}]]')" value="[{if $product->orderedamount}][{$product->orderedamount}][{else}]0[{/if}]" style="text-align: right;width:50px; padding-top:0; display:unset;">
                     <button style="margin-left:5px;" type="button" class="btn btn-xs btn-prima btn-outline-success bntooltip" onclick='addToBasket("[{$product->oxarticles__oxid}]","[{*oxmultilang ident="WK-OK"}]","[{*oxmultilang ident="WK-ERR"}]", "[{*oxmultilang ident="WK-NO-ART"}]")' data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="TIP_TO_THE_BASKET"}]"><span class="fa fa-shopping-cart fa-2x"></span>[{}]<span class="ladda-label">[{oxmultilang ident="TO_THE_BASKET"}]</span>[{}]</button>
                     [{else}]
                        <button type="button" class="btn btn-outline-info btn-show-variants ladda-button bntooltip" data-style="expand-right" onclick="loadProductOptions('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="TIP_SHOW_OPTIONS"}]"><span class="fa fa-list-ul"></span></button>
                     [{/if*}]
                     [{if $product->isBuyable()}]
                     [{* Warenkorbbutton *}]
                     [{if $product->isBnBuyable()}]
                     <input id="inputSmall[{$product->oxarticles__oxid|replace:".":"_"}]" min="0" class="form-control amount" [{* type="number"*}]inputmode="numeric" onchange="updateAmount(this,'amount[[{$product->oxarticles__oxid}]]')" value="[{if $product->orderedamount}][{$product->orderedamount}][{else}]0[{/if}]" style="text-align: right;width:50px; padding-top:0; display:unset;">
                     <button style="margin-left:5px;" type="button" class="btn btn-xs btn-prima btn-outline-success bntooltip" onclick='addToBasket("[{$product->oxarticles__oxid}]","[{oxmultilang ident='WK-OK'}]","[{*oxmultilang ident="WK-ERR"*}]", "[{*oxmultilang ident="WK-NO-ART"*}]")' data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="TIP_TO_THE_BASKET"}]"><span class="fa fa-shopping-cart fa-2x"></span>[{*}]<span class="ladda-label">[{oxmultilang ident="TO_THE_BASKET"}]</span>[{*}]</button>

                     [{else}]
                     <button type="button"
                             class="btn btn-xs btn-prima btn-default bntooltip disabled"
                             data-style="expand-right"
                             onclick='alert("[{oxmultilang ident="NOT_BUYABLE"}]");'
                             data-toggle="tooltip" data-placement="top"
                             title="[{oxmultilang ident="NOT_BUYABLE"}]"><span
                                 class="fa fa-shopping-cart"></span>[{*}]<span class="ladda-label">[{oxmultilang ident="TO_THE_BASKET"}]</span>[{*}]
                     </button>
                     [{/if}]
                     [{else}]
                     <button type="button" class="btn btn-outline-info btn-show-variants ladda-button bntooltip" data-style="expand-right" onclick="loadProductOptions('[{$product->oxarticles__oxid}]',[{$oViewConf->getActLanguageId()}])" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="TIP_SHOW_OPTIONS"}]"><span class="fa fa-list-ul"></span></button>

                     [{/if}]
                 </div>

                 <div class="text-right" style="margin-top:10px;white-space: nowrap">

                     <div>
                         [{oxid_include_dynamic file="widget/product/stockinfo.tpl"}]
                         <strong>
                             <label id="productPrice_[{$iIndex}]" class="price">
                            <span>
                                [{if $product->isRangePrice()}]
                                    [{oxmultilang ident="PRICE_FROM" }]
                                    [{if !$product->isParentNotBuyable() }]
                                        [{assign var="oPrice" value=$product->getMinPrice()}]
                                    [{else}]
                                        [{assign var="oPrice" value=$product->getVarMinPrice()}]
                                        [{assign var="oPriceAktionVar" value=$product->getMinVar($preisgruppe)}]
                                    [{/if}]
                                [{else}]
                                    [{if !$product->isParentNotBuyable() }]
                                        [{assign var="oPrice" value=$product->getPrice()}]
                                    [{else}]
                                        [{assign var="oPrice" value=$product->getVarMinPrice() }]
                                        [{assign var="oPriceAktionVar" value=$product->getMinVar($preisgruppe) }]
                                    [{/if}]
                                [{/if}]

                            </span>

                                 [{*oxprice price=$oPrice currency=$oView->getActCurrency()*}]
                                 [{if $streichpreis > 0 }]
                                 <s>[{oxprice price=$streichpreis currency=$oView->getActCurrency()}]</s> [{oxprice price=$oPrice currency=$oView->getActCurrency()}]
                                 [{else}]
                                 [{oxprice price=$oPrice currency=$oView->getActCurrency()}]
                                 [{/if}]

                                 [{assign var="preis" value=$product->getPreisAktuell()}]
                                 [{assign var="streichpreis" value=$product->getUserStreichpreis()}]
                                 [{assign var="preisaktionbeginn" value=$product->getaktionsbeginn()}]
                                 [{assign var="preisaktionende" value=$product->getaktionsende()}]


                                    [{*if $product->isParentNotBuyable() && ($oPriceAktionVar gt $oPrice)}] [{* Prüft, ob ein Variantenpreis niedriger ist als der niedrigste Aktionspreis}]
                                         <s>[{oxprice price=$oPriceAktionVar currency=$oView->getActCurrency()}]</s> [{oxprice price=$oPrice currency=$oView->getActCurrency()}]
                                    [{else}]
                                        [{oxprice price=$oPrice currency=$oView->getActCurrency()}]
                                    [{/if}]
                                 [{/if*}]


                                 [{if $oView->isVatIncluded() }]
                                 [{if !($product->hasMdVariants() || ($oViewConf->showSelectListsInList() && $product->getSelections(1)) || $product->getVariants())}]*[{/if}]
                                 [{/if}]
                             </label>
                     </div>
                     </strong>
                 </div>
             </div>
         </div>


    </div>
    <tr id="articleA_[{$product->oxarticles__oxid|replace:".":"_"}]" class="tabellenspalte d-none d-xl-table-row">
        [{if $product->isBuyable()}]
        [{if $product->isBnBuyable()}]
    <input type="hidden" name="amount[[{$product->oxarticles__oxid}]]" value="[{if $product->orderedamount}][{$product->orderedamount}][{else}]0[{/if}]">
        [{else}]
        [{/if}]
        [{/if}]
        [{* Artikelbild *}]
        <td class="d-none d-sm-block">
            <a name="[{$product->getID()|replace:".":"_"}]"></a>
            [{if $oConfig->getConfigParam('CDN')}]

            <div class="pictureBox">
                <a href="[{*$_productLink*}]#" onclick="loadArticleDetails('[{$product->oxarticles__oxid}]')" class="viewAllHover glowShadow corners" title="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
                    [{if $product->oxarticles__oxpic1 != ''}]

                <img data-toggle="" src="https://cdn.bodynova.de/out/pictures/generated/product/1/1000_1000_75/[{$product->oxarticles__oxpic1->value}]" alt="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]" class="img-thumbnail">
                    [{else}]

                <img data-toggle="" src="https://bodynova.de/out/imagehandler.php?artnum=[{$product->oxarticles__oxartnum}]&size=1000_1000_75" class="img-thumbnail">
                    [{/if}]
                </a>
            </div>
            [{* pictureBox VOR CDN*}]
            [{else}]

            <div class="pictureBox">
                <a href="[{*$_productLink*}]#" onclick="loadArticleDetails('[{$product->oxarticles__oxid}]')" class="viewAllHover glowShadow corners" title="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
                    [{*$product->getThumbnailUrl()*}]
                    [{if $product->oxarticles__oxpic1 != ''}]

                <img class="lazy-img img-thumbnail" src="[{$oViewConf->getImageUrl('spinner.gif')}]" data-src="[{$product->getThumbnailUrl()}]" alt="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
                    [{else}]
                <img class="lazy-img img-thumbnail" src="[{$oViewConf->getImageUrl('spinner.gif')}]" data-src="https://bodynova.de/out/imagehandler.php?artnum=[{$product->oxarticles__oxartnum}]&size=390_245_75" alt="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
                    [{/if}]
                </a>
            </div>
            [{/if}]
        </td>

        <td class="ebenetitel">
            [{* Verpackungseinheit }]
            <div class="pull-right" style="margin-top: 2px; margin-left:5px;">
                [{if $product->oxarticles__verpackungseinheit->value ne "0" and $product->oxarticles__verpackungseinheit->value > 1}]
                <span style="margin:0">
                    [{oxmultilang ident="VERPACKUNGSEINHEIT" suffix="COLON"}] [{$product->oxarticles__verpackungseinheit->value}]
                </span>
                [{/if}]
            </div>*}]
            [{* Titel *}]
            <span class="headline">
				[{*$product|var_dump*}]
				[{if $product->isBuyable()}]
					[{$product->oxarticles__oxtitle->value }]
				[{else}]
					[{$product->oxarticles__oxtitle->value }] [{$product->oxarticles__oxvarselect->value}]
				[{/if}]
			</span>
            <br/>
            <form>
                [{* Standardartikel *}]
                [{if $product->isBuyable()}]
                <div class="row">
                    <div class="col-sm-5" style="padding-right: 0 !important;">

                            [{* Mengenwähler, falls hier Änderungen vorgenommen werden, bitte in bodynova.js in der Funktion PlusMinusAmount checken, ob die Funktion davon unberührt bleibt *}]
                            [{if $product->isBnBuyable()}]
                                <div class="amount_input spinner input-group bootstrap-touchspin pull-left">
                                    <span class="input-group-btn"><button onclick="PlusMinusAmount('[{$product->oxarticles__oxid}]','0',this);" type="button" class="btn btn-outline-primary bootstrap-touchspin-down" style="border-top-right-radius:0;border-bottom-right-radius:0">-</button></span>
                                    <!--<span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>-->
                                    <input id="inputBig[{$product->oxarticles__oxid}]" min="0" class="amount" type="numeric" onchange="updateAmount(this,'amount[[{$product->oxarticles__oxid}]]')"  value="[{if $product->orderedamount}][{$product->orderedamount}][{else}]0[{/if}]" style="text-align: center;height:22px !important;width:45px !important">
                                    <span class="input-group-btn"><button onclick="PlusMinusAmount('[{$product->oxarticles__oxid}]','1',this);" type="button" class="btn btn-outline-primary bootstrap-touchspin-up" style="border-top-left-radius:0;border-bottom-left-radius:0;margin-left:0">+</button></span>
                                </div>
                            [{/if}]
                    </div>
                    <div class="col-sm-7">
                            [{* Info Button *}]
                            <button type="button" class="btn btn-outline-info ladda-button bntooltip pull-left" data-style="expand-right" onclick="loadArticleDetails('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="SHOW_ARTICLE_DETAILS"}]"><span class="fa fa-info-circle"></span></button>
                            [{* Herz Button *}]
                            <button type="button" class="btn btn-favorite btn btn-default ladda-button pull-left [{if $product->isFavorite()}]btn-primary[{/if}] bntooltip" data-style="expand-right" onclick="toggleFavoritEntry('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="ADD_TO_FAVORITE" }]"><span class="fa fa-heart-o"></span></button>
                            [{* Artikelnummer & VE-Einheit *}]
                            <div class="artnr pull-left">
                                [{* Artikelnummer *}]
                                [{oxmultilang ident="ARTNR" suffix="COLON"}] [{$product->oxarticles__oxartnum}]
                            </div>
                            <div class="pull-right">
                            [{* Rückrufaktion *}]
                            [{if $product->oxarticles__bnflagrecall->value == 1}]<button style="width:32px;" type="button" class="btn btn-xs btn-outline-danger bntooltip" data-toggle="tooltip" data-placement="top" title="[{$product->oxarticles__recallreason->value}]"><i class="fa fa-exclamation-triangle"></i></button>[{/if}]
                            [{* Aktionsartikel *}]
                            [{*if $product->isAktionbeendet() }]<button style="margin-left:10px; width:32px" type="button" class="btn btn-xs btn-outline-success bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AKTIONBIS"}][{$product->oxarticles__aktionsende->value|date_format:"%d.%m.%Y"}]"><i class="fas fa-euro-sign"></i></button>[{/if*}]
                            [{if $preisaktionbeginn <= $smarty.now|date_format:"%Y-%m-%d" && $preisaktionende > $smarty.now|date_format:"%Y-%m-%d" && $streichpreis >0}]
                            <button style="margin-left:10px; width:32px" type="button" class="btn btn-xs btn-outline-success bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AKTIONBIS"}][{$preisaktionende|date_format:"%d.%m.%Y"}]"><i class="fas fa-euro-sign"></i></button>
                            [{/if}]

                            [{* Auslaufartikel *}]
                            [{if $product->oxarticles__bnflagauslaufartikel->value == 1}]<button style="margin-left:10px; width:32px" type="button" class="btn btn-xs btn-outline-warning bntooltip" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="AUSLAUFARTIKEL"}]"><i class="fa fa-exclamation-triangle"></i></button>[{/if}]
                            </div>
                            <div class="pull-right">
                                [{if $product->oxarticles__verpackungseinheit->value ne "0" and $product->oxarticles__verpackungseinheit->value > 0}]
                                <span style="margin:0;cursor:default" class="bntooltip" data-toggle="tooltip" data-original-title=[{oxmultilang ident="VETooltip"}]>
                                    [{oxmultilang ident="VERPACKUNGSEINHEIT" suffix="COLON"}] [{$product->oxarticles__verpackungseinheit->value}]
                                </span>
                                [{/if}]
                            </div>
                    </div>
                </div>

                [{else}]
                [{* Vaterartikel *}]
                <div class="row">
                    <div class="col-sm-5">
                        <div class="input-line">
                            [{* Varianten Button *}]
                            <button type="button" class="btn btn-outline-info btn-show-variants openVariantsBig ladda-button bntooltip" data-style="expand-right" onclick="loadProductOptionsBig('[{$product->oxarticles__oxid}]',[{$oViewConf->getActLanguageId()}])" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="TIP_SHOW_OPTIONS"}]"><span class="fa fa-list-ul"></span></button>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="pull-left">
                            [{* Info Button *}]
                            <button type="button" class="btn btn-outline-info btn ladda-button bntooltip pull-left" data-style="expand-right" onclick="loadArticleDetails('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="SHOW_ARTICLE_DETAILS"}]"><span class="fa fa-info-circle"></span></button>
                            [{* Herz Button *}]
                            <button type="button" class=" btn btn-favorite btn btn-default ladda-button bntooltip pull-left [{if $product->isFavorite()}]btn-primary[{/if}]" data-style="expand-right" onclick="toggleFavoritEntry('[{$product->oxarticles__oxid}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="ADD_TO_FAVORITE"}]"><span class="fa fa-heart-o"></span></button>
                            [{* Artikelnummer *}]
                            <div class="artnr pull-left">
                                [{oxmultilang ident="ARTNR" suffix="COLON"}] [{$product->oxarticles__oxartnum}]
                            </div>
                        </div>
                    </div>
                </div>
                [{/if}]
            </form>
        </td>
        [{* Warenkorb Button *}]
        <td class="Warenkorb">
            <form>
                <div class="input-line pull-right">
                    [{if $product->isBuyable()}]
                        [{if $product->isBnBuyable()}]
                            [{* Warenkorbbutton *}]
                            <button type="button" class="btn btn-prima btn-basket ladda-button btn-outline-success pull-right bntooltip" data-style="expand-right" onclick='addToBasket("[{$product->oxarticles__oxid}]","[{oxmultilang ident='WK-OK'}]","[{oxmultilang ident='WK-ERR'}]", "[{oxmultilang ident='WK-NO-ART'}]")' data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="TIP_TO_THE_BASKET"}]"><span class="fa fa-shopping-cart fa-2x"></span>[{*}]<span class="ladda-label">[{oxmultilang ident="TO_THE_BASKET"}]</span>[{*}]</button>
                        [{else}]
                            <button type="button"
                                    class="btn btn-prima btn-basket ladda-button btn-outline-default disabled pull-right bntooltip"
                                    data-style="expand-right"
                                    onclick="alert('[{oxmultilang ident="NOT_BUYABLE"}]')"
                                    data-toggle="tooltip" data-placement="top"
                                    title="[{oxmultilang ident="NOT_BUYABLE"}]"><span
                                        class="fa fa-shopping-cart fa-2x"></span>[{*}]<span class="ladda-label">[{oxmultilang ident="TO_THE_BASKET"}]</span>[{*}]
                            </button>
                        [{/if}]
                    [{/if}]
                </div>
            </form>
        </td>
        <td class="selprice" style="white-space: nowrap">

            <label id="productPrice_[{$iIndex}]" class="price">
				<span>
					[{if $product->isRangePrice()}]
						[{oxmultilang ident="PRICE_FROM" }]
						[{if !$product->isParentNotBuyable() }]
							[{assign var="oPrice" value=$product->getMinPrice()}]
						[{else}]
							[{assign var="oPrice" value=$product->getVarMinPrice()}]
                            [{assign var="oPriceAktionVar" value=$product->getMinVar($preisgruppe)}]
						[{/if}]
					[{else}]
						[{if !$product->isParentNotBuyable() }]
							[{assign var="oPrice" value=$product->getPrice()}]
						[{else}]
							[{assign var="oPrice" value=$product->getVarMinPrice() }]
                            [{assign var="oPriceAktionVar" value=$product->getMinVar($preisgruppe)}]
						[{/if}]
					[{/if}]
				</span>
                [{*oxprice price=$oPrice currency=$oView->getActCurrency()*}]

                [{if $streichpreis > 0 }]
                <s>[{oxprice price=$streichpreis currency=$oView->getActCurrency()}]</s> [{oxprice price=$oPrice currency=$oView->getActCurrency()}]
                [{else}]
                [{oxprice price=$oPrice currency=$oView->getActCurrency()}]
                [{/if}]
                [{*if $product->isParentNotBuyable() && ($oPriceAktionVar gt 0) }] [{* Prüft, ob ein Variantenpreis niedriger ist als der niedrigste Aktionspreis}]
                        <s>[{oxprice price=$oPriceAktionVar currency=$oView->getActCurrency()}]</s> [{oxprice price=$oPrice currency=$oView->getActCurrency()}]
                    [{else}]
                        [{oxprice price=$oPrice currency=$oView->getActCurrency()}]
                    [{/if}]
                [{/if*}]

                [{if $oView->isVatIncluded() }]
                [{if !($product->hasMdVariants() || ($oViewConf->showSelectListsInList() && $product->getSelections(1)) || $product->getVariants())}]*[{/if}]
                [{/if}]
            </label>

        </td>
        [{* Ampel *}]
        <td class="ampelcolumn">
            [{oxid_include_dynamic file="widget/product/stockinfo.tpl"}]
        </td>
        [{* Löschen Button *}]
        [{if $template == 'myfavorites' && $product->orderedamount == 0}]
            <td class="ampelcolumn">
                <button class="btn btn-xs btn-outline-danger pull-right" onclick="deleteOxfavorite('[{$product->oxarticles__oxid->value}],[{$oxcmp_user->oxuser__oxid->value}]');"><span class="fa fa-trash"></span></button>
            </td>
        [{elseif $template == 'stockinfoList'}]
        <td style="width:70px;padding-top:19px !important;">
            <button class="btn btn-xs btn-outline-danger pull-right" onclick="deleteStockInfo('[{$product->oxarticles__oxid->value}]','[{$oxcmp_user->oxuser__oxid->value}]');"><span class="fa fa-trash"></span></button>
        </td>
        [{/if}]
    </tr>
    [{if !$product->isBuyable()}]
    <tr id="article_selected_Big[{$product->oxarticles__oxid|replace:".":"_"}]" class="articleselect d-none d-xl-table-row" attribute="big">
        <td colspan="5"></td>
    </tr>
    <div id="article_selected_[{$product->oxarticles__oxid|replace:".":"_"}]" class="d-xl-none" attribute="small">

    </div>

    [{/if}]
[{/block}]
