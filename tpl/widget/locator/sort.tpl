[{block name="widget_locator_sort"}]
    [{if $oView->showSorting()}]
        [{assign var="_listType" value=$oView->getListDisplayType()}]
        [{assign var="_additionalParams" value=$oView->getAdditionalParams()}]
        [{assign var="_artPerPage" value=$oViewConf->getArtPerPageCount()}]
        [{assign var="_sortColumnVarName" value=$oView->getSortOrderByParameterName()}]
        [{assign var="_sortDirectionVarName" value=$oView->getSortOrderParameterName()}]

        <div class="btn-group">
            <button type="button" class="btn btn-outline-dark btn-sm dropdown-toggle" data-toggle="dropdown">
                <strong>[{oxmultilang ident="SORT_BY"}]:</strong>
                [{if $oView->getListOrderBy()}]
                    [{oxmultilang ident=$oView->getListOrderBy()|upper }]
                [{else}]
                    [{oxmultilang ident="CHOOSE"}]
                [{/if}]
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                [{foreach from=$oView->getSortColumns() item=sColumnName}]
                    <li class="filter-item desc[{if $oView->getListOrderDirection() == 'desc' && $sColumnName == $oView->getListOrderBy()}] active[{/if}]">
                        <a class="filter-link" href="[{$oView->getLink()|oxaddparams:"ldtype=$_listType&amp;_artperpage=$_artPerPage&amp;$_sortColumnVarName=$sColumnName&amp;$_sortDirectionVarName=desc&amp;pgNr=0&amp;$_additionalParams"}]" title="[{oxmultilang ident=$sColumnName|upper}] [{oxmultilang ident="DD_SORT_DESC"}]">
                            <i class="fa fa-caret-down"></i> [{oxmultilang ident=$sColumnName|upper}]
                        </a>
                    </li>
                    <li class="filter-item asc[{if $oView->getListOrderDirection() == 'asc' && $sColumnName == $oView->getListOrderBy()}] active[{/if}]">
                        <a class="filter-link" href="[{$oView->getLink()|oxaddparams:"ldtype=$_listType&amp;_artperpage=$_artPerPage&amp;$_sortColumnVarName=$sColumnName&amp;$_sortDirectionVarName=asc&amp;pgNr=0&amp;$_additionalParams"}]" title="[{oxmultilang ident=$sColumnName|upper}] [{oxmultilang ident="DD_SORT_ASC"}]">
                            <i class="fa fa-caret-up"></i> [{oxmultilang ident=$sColumnName|upper}]
                        </a>
                    </li>
                [{/foreach}]
            </ul>
        </div>
    <button id="buttonAllVariants"  type="button" class="btn btn-outline-primary bntooltip btn-sm" data-style="expand-right" onclick="openAllVariants()" data-placement="top" title="[{oxmultilang ident="OPEN_ALL_VARIANTS"}]">
        <span id="spinnerAllVariants" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" style="display:none"></span>
        <i class="fa fa-expand"></i>
    </button>

    [{* PDF erstell Link *}]
    [{if $oView->getClassName() != "search"}]
    <button onclick="loadPDF('[{$oViewConf->getSelfLink()}]cl=pricelist&fnc=getPDF&cat=[{$actCategory->oxcategories__oxid->value}]&name=[{$actCategory->oxcategories__oxtitle->value}]')" type="button" class="btn btn-default btn-outline-primary bntooltip" data-style="expand-right" data-placement="top" title="[{oxmultilang ident="PRINT_PDF"}]">PDF</button>
    <button onclick="window.location='[{$oViewConf->getSelfLink()}]cl=pricelist&fnc=getExcel&cat=[{$actCategory->oxcategories__oxid->value}]&name=[{$actCategory->oxcategories__oxtitle->value}]&user=[{$oxcmp_user->oxuser__oxusername->value}]'"
       type="button" class="btn btn-default btn-outline-primary bntooltip" data-style="expand-right" data-placement="top" title="[{oxmultilang ident="PRINT_EXCEL"}]"
    >Excel[{*}]<span class="glyphicon glyphicon-list-alt"></span>[{*}]</button>
    [{/if}]
    [{/if}]

[{/block}]