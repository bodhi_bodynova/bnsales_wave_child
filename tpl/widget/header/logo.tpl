<!-- Logo -->
[{block name="layout_header_logo"}]

<li role="presentation" class="logo d-md-block">
    [{assign var="slogoImg" value=$oViewConf->getViewThemeParam('sLogoFile')}]
    [{assign var="sLogoWidth" value=$oViewConf->getViewThemeParam('sLogoWidth')}]
    [{assign var="sLogoHeight" value=$oViewConf->getViewThemeParam('sLogoHeight')}]
    <a href="[{$oViewConf->getHomeLink()}]" title="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]" class="logo-link">
        <img src="[{$oViewConf->getImageUrl($slogoImg)}]" alt="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]"  class="logo-img">
    </a>
</li>
[{/block}]
