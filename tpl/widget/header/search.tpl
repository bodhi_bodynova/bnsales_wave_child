[{block name="widget_header_search_form"}]
    [{if $oView->showSearch() }]
    <!-- Suche -->
    <li role="presentation">
        <form class="navbar-form" role="search" action="[{$oViewConf->getSelfActionLink()}]" method="get" name="search">
            <div class="input-group">
                [{$oViewConf->getHiddenSid()}]
                <input type="hidden" name="cl" value="search">
                [{block name="header_search_field"}]
            <input type="text" class="form-control" id="searchParam" name="searchparam"  placeholder="[{oxmultilang ident="SEARCH"}]" value="[{$oView->getSearchParamForHtml()}]">
                [{/block}]
                <div class="input-group-append">
                <button style="border-radius:0" data-toggle="tooltip" title="[{oxmultilang ident="PAGE_TITLE_SEARCH"}]" id="searchParamSubmit" type="submit" class="rounded-right btn btn-default bntooltip"><span class="fas fa-search"></span>
                </button>
                </div>
                <div id="results"></div>
            </div>
        </form>
    </li>
    [{/if}]
[{/block}]


[{*block name="widget_header_search_form"}]
    [{if $oView->showSearch()}]
        <form class="navbar-form" role="search" action="[{$oViewConf->getSelfActionLink()}]" method="get" name="search">
            [{$oViewConf->getHiddenSid()}]
            <input type="hidden" name="cl" value="search">

            [{block name="dd_widget_header_search_form_inner"}]
                <div class="input-group">
                    [{block name="header_search_field"}]
                        <input class="form-control" type="text" id="searchParam" name="searchparam" value="[{$oView->getSearchParamForHtml()}]" placeholder="[{oxmultilang ident="SEARCH"}]">
                    [{/block}]

                    [{block name="dd_header_search_button"}]
                    <div class="input-group-append">
                        <button data-toggle="tooltip" title="[{oxmultilang ident="PAGE_TITLE_SEARCH"}]" id="searchParamSubmit" type="submit" class="btn btn-default bntooltip"><span class="fas fa-search"></span>
                        </button>                    </div>
                    [{/block}]
                </div>
            [{/block}]
        </form>
    [{/if}]
[{/block*}]