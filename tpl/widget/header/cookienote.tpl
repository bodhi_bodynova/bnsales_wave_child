[{if $smarty.cookies.displayedCookiesNotification != '1'}]
    <div class="container">
        [{oxscript include="js/libs/jquery.cookie.min.js"}]
        [{oxscript include="js/widgets/oxcookienote.min.js"}]
        [{oxscript add="$.cookie('testing', 'yes'); if(!$.cookie('testing')) $('#cookieNote').hide(); else{ $('#cookieNote').show(); $.cookie('testing', null, -1);}"}]
        <div id="cookieNote">
            <div class="alert alert-info bn-coockie" style="margin: 0;">
                [{oxmultilang ident='COOKIE_NOTE'}]
                <br/>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="pull-right">
                                <button type="button" class="btn btn-outline-success" data-dismiss="alert">
                                    <i class="fas fa-check"></i>
                                    [{*}]<span aria-hidden="true">&times;</span>[{*}]
                                    <span class="sr-only">
                                        [{oxmultilang ident='COOKIE_NOTE_CLOSE'}]
                                    </span>
                                </button>
                                <a class="btn btn-outline-danger"
                                   href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=clearcookies"}]"
                                   title="[{oxmultilang ident='COOKIE_NOTE_DISAGREE'}]">
                                    [{*oxmultilang ident='COOKIE_NOTE_DISAGREE'*}]
                                    <i class="fas fa-times"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        [{oxscript add="$('#cookieNote').oxCookieNote();"}]
    </div>
[{/if}]
[{oxscript widget=$oView->getClassName()}]

