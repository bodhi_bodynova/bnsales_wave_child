[{if $oView->isLanguageLoaded()}]
    <li class="rest" role="presentation">
        <form class="navbar-form" role="form">
            <!-- Minibasket -->
            <button onclick="klicktest();" id="buttonSidebarLeft" class="pull-left d-block d-sm-block d-md-none btn btn-info bntooltip" data-toggle="tooltip" title="[{oxmultilang ident="MENUKLEIN" }]" data-placement="bottom" type="button" style="margin-top:15px;margin-right:-20px;"><i class="fas fa-angle-down"></i></button>

            <div class="form-group form-group-service" id="[{$_prefix}]miniBasket">

                <a class="minib" href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=basket" }]" style="top:11px">
                    <span class="counter" style="[{if $oxcmp_basket->getItemsCount() < 1}]display: none;[{/if}] margin-right:-8px">
                        <span class="badge badge-pill badge-secondary" data-placement="bottom" data-toggle="tooltip" title="[{oxmultilang ident="ADDED_TO_BASKET_CONFIRMATION" }]">[{$oxcmp_basket->getItemsCount()}]</span>
                    </span>
                    [{*<img id="[{$_prefix}]minibasketIcon" class="basket" alt="Basket" src="[{$oViewConf->getImageUrl('basket.png')}]" data-original-title="" title="">*}]
                    <i id="[{$_prefix}]minibasketIcon" class="basket fal fa-shopping-cart fa-2x" data-original-title="" title="" ></i>
                </a>



                <!-- Flaggen-Box -->
                <div id="languageSwitcher" class="dropdown symbol-flags">
                    <button id="btnLangaugeMenue" class="btn btn-outline-default dropdown-toggle" aria-expanded="true" data-toggle="dropdown" type="button">
                        [{foreach from=$oxcmp_lang item=_lng}]
                        [{if $_lng->selected}]
                        [{assign var="sLangImg" value="lang/"|cat:$_lng->abbr|cat:".png"}]
                    <img src="[{$oViewConf->getImageUrl($sLangImg)}]">
                        [{*$_lng->name*}]
                        [{/if}]
                        [{/foreach}]
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="btnLangaugeMenue" role="menu" style="width:57px;margin-left:2px;top:83% !important; left: auto !important; min-width: 57px;border-top-left-radius: 4px;border-top-right-radius: 4px;">
                        [{foreach from=$oxcmp_lang item=_lng}]
                        [{assign var="sLangImg" value="lang/"|cat:$_lng->abbr|cat:".png"}]
                        <li>
                            <a class="flag [{$_lng->abbr }] [{if $_lng->selected}]selected[{/if}]" title="[{$_lng->name}]" href="[{$_lng->link|oxaddparams:$oView->getDynUrlParams()}]" hreflang="[{$_lng->abbr }]">
                                <img src="[{$oViewConf->getImageUrl($sLangImg)}]">
                                [{*$_lng->name*}]
                            </a>
                        </li>
                        [{/foreach}]
                    </ul>
                </div><!-- Ende: Flaggen-Box -->

                <!-- Logout Symbol -->
                <a style="margin-bottom:3px;" class="btn  btn-sm btn-outline-danger minilogout bntooltip" data-placement="bottom" title="[{oxmultilang ident="LOGOUT"}]"  data-toggle="tooltip" href="[{$oViewConf->getLogoutLink()}]">
                    <span class="fas fa-power-off" aria-hidden="true"></span>
                </a><!-- Ende: Logout Symbol -->
                <!-- Burger -->
                <button class="navbar-toggler d-md-none pull-right btn-sm" type="button" data-toggle="collapse" onmouseover="" data-placement="bottom" data-target="#naviSachen" aria-controls="#naviSachen" aria-expanded="false" aria-label="Toggle Navigation" style="margin-top:12px; margin-right:-27px;">
                    <span class="navbar-toggler-icon bntooltip"  data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="BURGER"}]"></span>
                </button>
                <!-- Burger Ende -->
                [{* Schalter: *}]
                [{*TODO:include file="widget/header/schalter.tpl"*}]

            </div><!-- Ende: Minibasket -->
        </form>
    </li>
    [{/if}]
[{oxscript widget=$oView->getClassName()}]

[{*<div class="btn-group minibasket-menu">
    <button type="button" aria-label="Minibasket" class="btn dropdown-toggle" data-toggle="dropdown" data-href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=basket"}]">
        [{block name="dd_layout_page_header_icon_menu_minibasket_button"}]
            <i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>
            [{if $oxcmp_basket->getItemsCount() > 0}][{ $oxcmp_basket->getItemsCount() }][{/if}]
        [{/block}]
    </button>
    <ul class="dropdown-menu dropdown-menu-right" role="menu">
        [{block name="dd_layout_page_header_icon_menu_minibasket_list"}]
            <li>
                <div class="row">
                    <div class="col-12 ml-auto">
                        <div class="minibasket-menu-box">
                            [{oxid_include_dynamic file="widget/minibasket/minibasket.tpl"}]
                        </div>
                    </div>
                </div>
            </li>
        [{/block}]
    </ul>
</div>*}]