<div class="navbar-nav mr-auto" id="naviLeiste" style="display:flex;">

    <!--<ul id="menu" class="nav navbar-nav navbar-menue">-->
    [{foreach from=$oxcmp_categories item=ocat key=catkey name=root}]
        [{if $ocat->getIsVisible() }]
        [{foreach from=$ocat->getContentCats() item=oTopCont name=MoreTopCms}]
            [{assign var="iCatCnt" value=$iCatCnt+1 }]
            [{assign var="iAllCatCount" value=$iAllCatCount+1 }]
            [{if !$bHasMore && ($iCatCnt >= $oView->getTopNavigationCatCnt())}]
                [{assign var="bHasMore" value="true"}]
                [{assign var="iCatCnt" value=$iCatCnt+1}]
            [{/if}]
            [{if $iCatCnt <= $oView->getTopNavigationCatCnt()}]
                <li class="nav-item dropdown" oxid="[{$oTopCont->oxcategories__oxid->value}]" type="oxcategories"><a class="nav-link dropdown-item" href="[{$oTopCont->getLink()}]"><span class="glyphicon glyphicon-pushpin sort-visible"></span>[{$oTopCont->oxcontents__oxtitle->value}]</a></li>
            [{else}]
                [{capture append="moreLinks"}]
                    <li class="nav-item dropdown"><a class="nav-link dropdown-item" href="[{$oTopCont->getLink()}]">[{$oTopCont->oxcontents__oxtitle->value}]</a></li>
                [{/capture}]
            [{/if}]
        [{/foreach}]

        [{*assign var="iCatCnt" value=$iCatCnt+1*}]
        [{if !$bHasMore && ($iCatCnt >= $oView->getTopNavigationCatCnt())}]
            [{assign var="bHasMore" value="true"}]
            [{assign var="iCatCnt" value=$iCatCnt+1}]
        [{/if}]

        [{if $iCatCnt <= $oView->getTopNavigationCatCnt()}]
        <!--<li class="dropdown [{if $homeSelected == 'false' && $ocat->expanded}]current[{/if}]">-->
        <li style="min-height:38px !important;" role="presentation" class=" nav-item dropdown  [{if $homeSelected == 'false' && $ocat->expanded}]current[{/if}]" oxid="[{$ocat->oxcategories__oxid->value}]" type="oxcategories" >
            [{if $ocat->getSubCats()}]
                <a style="height:100% !important;" id="navsort[{$iCatCnt}]" class="nav-link dropdown-toggle [{if $homeSelected == 'false' && $ocat->expanded}]current[{/if}] " data-toggle="dropdown" href="[{$ocat->getLink()}]" role="button" aria-haspopup="true" aria-expanded="false">[{*}]<span class="glyphicon glyphicon-pushpin sort-visible"></span>[{*}][{$ocat->oxcategories__oxtitle->value}]<span class="caret"></span></a>
                <ul class="dropdown-menu navsortold" aria-labelledby="navsort[{$iCatCnt}]" id="navsort_old[{$iCatCnt}]" style="position:absolute; min-width:280px">
                    <a style="padding-left:16px;" class="dropdown-item [{if $homeSelected == 'false' && $ocat->expanded}]current[{/if}] " href="[{$ocat->getLink()}]">[{oxmultilang ident="AlleArtikel"}] <span class="caret"></span></a>
                    [{foreach from=$ocat->getSubCats() item=osubcat key=subcatkey name=SubCat}]
                        [{*foreach from=$osubcat->getContentCats() item=ocont name=MoreCms}]
                                                    <li><a href="[{$ocont->getLink()}]">[{$ocont->oxcontents__oxtitle->value}]</a></li>
                                                [{/foreach*}]
                        [{if $osubcat->getIsVisible() }]
                            [{if $osubcat->getSubCats()}]

                                <li><a style="white-space: nowrap; padding-left:16px" class="dropwdown-item" [{*onmouseover="$('#subsubcat[{$osubcat->oxcategories__oxid->value}]').css('display','block');" onmouseout="$('#subsubcat[{$osubcat->oxcategories__oxid->value}]').css('display','none');"*}] href="[{$osubcat->getLink()}]">[{$osubcat->oxcategories__oxtitle->value}] <i class="fa fa-angle-down"></i></a></li>
                                    <div class="" id="subsubcat[{$osubcat->oxcategories__oxid->value}]" style="display:block" [{*onmouseover="$(this).css('display','block');" onmouseout="$(this).css('display','none');"*}]>
                                [{foreach from=$osubcat->getSubCats() item=osubsubcat key=subsubcatkey name=SubSubCat}]
                                        <li class="subsubcat"><a id="" style="white-space: nowrap; padding-left:32px;"  class="dropwdown-item" href="[{$osubsubcat->getLink()}]">[{$osubsubcat->oxcategories__oxtitle->value}]</a></li>
                                [{/foreach}]
                                    </div>

                                [{*<li class="dropwdown-submenu" id="[{$osubcat->oxcategories__oxtitle->value}]">
                                    <a style="white-space: nowrap; padding-left:16px" class="dropdown-item dropdown-toggle" href="[{$osubcat->getLink()}]">[{$osubcat->oxcategories__oxtitle->value}]</a>
                                    <ul class="dropdown-menu" role="menu">
                                        [{foreach from=$osubcat->getSubCats() item=osubsubcat key=subsubcatkey name=SubSubCat}]
                                            <li>
                                                <a class="dropdown-toggle"  href="[{$osubsubcat->getLink()}]">[{$osubsubcat->oxcategories__oxtitle->value}]</a>
                                            </li>
                                        [{/foreach}]
                                    </ul>
                                </li>*}]
                                [{*<li class="dropdown-submenu" id="[{$osubcat->oxcategories__oxtitle->value}]">
                                    <a class="dropdown-item dropdown-toggle"></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            [{foreach from=$osubcat->getSubCats() item=osubsubcat key=subsubcatkey name=SubSubCat}]
                                                <a class="dropdown-item"  href="[{$osubsubcat->getLink()}]">[{$osubsubcat->oxcategories__oxtitle->value}]</a>
                                            [{/foreach}]
                                        </li>
                                    </ul>
                                </li>*}]
                            [{else}]
                            <li><a style="white-space: nowrap; padding-left:16px" class="dropdown-item" [{if $homeSelected == 'false' && $osubcat->expanded}]class="current"[{/if}] href="[{$osubcat->getLink()}]">[{$osubcat->oxcategories__oxtitle->value}]<span class="glyphicon glyphicon-pushpin sort-visible"></span></a></li>
                            [{/if}]

                        [{/if}]
                    [{/foreach}]
                </ul>
            [{else}]
                <a class="nav-link" [{if $homeSelected == 'false' && $ocat->expanded}]class="current"[{/if}] href="[{$ocat->getLink()}]"><span class="glyphicon glyphicon-pushpin sort-visible"></span>[{$ocat->oxcategories__oxtitle->value}]</a>
            [{/if}]
        </li>
        [{else}]
            [{capture append="moreLinks"}]
                <li [{if $homeSelected == 'false' && $ocat->expanded}]class="current"[{/if}]>
                    <a href="[{$ocat->getLink()}]">[{$ocat->oxcategories__oxtitle->value}]</a>
                </li>
            [{/capture}]
        [{/if}]
        [{/if}]
    [{/foreach}]
</div><!-- Ende: Navi-Leiste -->
