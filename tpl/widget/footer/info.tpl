<div class="card">
    <div class="card-header">
        <h3 class="card-title"><strong>[{oxmultilang ident="INFORMATION"}]</strong></h3>
    </div>
    <div class="panel-body">
        <ul class="tree list-unstyled" role="tablist">
            [{oxifcontent ident="oximpressum" object="_cont"}]
            <li><a href="[{$_cont->getLink()}]">[{$_cont->oxcontents__oxtitle->value}]</a></li>
            [{/oxifcontent}]
            [{oxifcontent ident="oxagb" object="_cont"}]
            <li><a href="[{$_cont->getLink()}]">[{$_cont->oxcontents__oxtitle->value}]</a></li>
            [{/oxifcontent}]
            [{oxifcontent ident="oxsecurityinfo" object="oCont"}]
            <li><a href="[{$oCont->getLink()}]">[{$oCont->oxcontents__oxtitle->value}]</a></li>
            [{/oxifcontent}]
            [{oxifcontent ident="oxdeliveryinfo" object="oCont"}]
            <li><a href="[{$oCont->getLink()}]">[{$oCont->oxcontents__oxtitle->value}]</a></li>
            [{/oxifcontent}]
            [{oxifcontent ident="oxrightofwithdrawal" object="oCont"}]
            <li><a href="[{$oCont->getLink()}]">[{$oCont->oxcontents__oxtitle->value}]</a></li>
            [{/oxifcontent}]
            [{oxifcontent ident="oxorderinfo" object="oCont"}]
            <li><a href="[{$oCont->getLink()}]">[{$oCont->oxcontents__oxtitle->value}]</a></li>
            [{/oxifcontent}]
            [{oxifcontent ident="oxcredits" object="oCont"}]
            <li><a href="[{$oCont->getLink()}]">[{$oCont->oxcontents__oxtitle->value}]</a></li>
            [{/oxifcontent}]
            [{oxifcontent ident="works" object="oCont"}]
            <li><a href="[{$oCont->getLink()}]">[{$oCont->oxcontents__oxtitle->value}]</a></li>
            [{/oxifcontent}]
        </ul>
    </div>
    <div class="card-footer" style="overflow: hidden;">
           <span class="headline"><strong>&copy; [{php}]echo date("Y",time());[{/php}] Bodynova GmbH</strong></span>
    </div>
</div>

[{*block name="footer_information"}]


    <ul class="information list-unstyled">
        [{oxifcontent ident="oximpressum" object="_cont"}]
            <li><a href="[{$_cont->getLink()}]">[{$_cont->oxcontents__oxtitle->value}]</a></li>
        [{/oxifcontent}]
        [{oxifcontent ident="oxagb" object="_cont"}]
            <li><a href="[{$_cont->getLink()}]">[{$_cont->oxcontents__oxtitle->value}]</a></li>
        [{/oxifcontent}]
        [{oxifcontent ident="oxsecurityinfo" object="oCont"}]
            <li><a href="[{$oCont->getLink()}]">[{$oCont->oxcontents__oxtitle->value}]</a></li>
        [{/oxifcontent}]
        [{oxifcontent ident="oxdeliveryinfo" object="oCont"}]
            <li><a href="[{$oCont->getLink()}]">[{$oCont->oxcontents__oxtitle->value}]</a></li>
        [{/oxifcontent}]
        [{oxifcontent ident="oxrightofwithdrawal" object="oCont"}]
            <li><a href="[{$oCont->getLink()}]">[{$oCont->oxcontents__oxtitle->value}]</a></li>
        [{/oxifcontent}]
        [{oxifcontent ident="oxorderinfo" object="oCont"}]
            <li><a href="[{$oCont->getLink()}]">[{$oCont->oxcontents__oxtitle->value}]</a></li>
        [{/oxifcontent}]
        [{oxifcontent ident="oxcredits" object="oCont"}]
            <li><a href="[{$oCont->getLink()}]">[{$oCont->oxcontents__oxtitle->value}]</a></li>
        [{/oxifcontent}]
        [{if $oViewConf->getViewThemeParam('blFooterShowNewsletter')}]
            <li><a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=newsletter"}]">[{oxmultilang ident="NEWSLETTER"}]</a></li>
        [{/if}]
        [{if $oViewConf->getViewThemeParam('blFooterShowNews')}]
            <li><a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=news"}]">[{oxmultilang ident="NEWS"}]</a></li>
        [{/if}]
    </ul>
[{/block*}]