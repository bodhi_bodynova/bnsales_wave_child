    <div class="card ">
        <div class="card-header">
            <h3 class="card-title"><strong>[{oxmultilang ident="SERVICES" }]</strong></h3>
        </div>
        <div class="card-body">
            <ul class="services list-unstyled">
                [{block name="footer_services_items"}]
                <li><a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=contact"}]">[{oxmultilang ident="CONTACT"}]</a></li>

                [{oxhasrights ident="TOBASKET"}]
                <li>
                    <a href="[{oxgetseourl ident=$oViewConf->getBasketLink()}]">
                        [{oxmultilang ident="CART"}][{if $oxcmp_basket && $oxcmp_basket->getItemsCount() > 0}] <span class="badge badge-pill badge-secondary">[{$oxcmp_basket->getItemsCount()}]</span>[{/if}]

                    </a>
                </li>
                [{/oxhasrights}]
                <li><a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account"}]">[{oxmultilang ident="ACCOUNT"}]</a></li>

                [{if $oViewConf->getShowWishlist()}]
                <li>
                    <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_wishlist"}]">
                        [{oxmultilang ident="MY_GIFT_REGISTRY"}]
                    </a>
                    [{if $oxcmp_user && $oxcmp_user->getWishListArtCnt()}] <span class="badge badge-pill badge-secondary">[{$oxcmp_user->getWishListArtCnt()}]</span>[{/if}]
                </li>
                <li>
                    <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=wishlist" params="wishid="|cat:$oView->getWishlistUserId()}]">
                        [{oxmultilang ident="PUBLIC_GIFT_REGISTRIES"}]
                    </a>
                </li>
                [{/if}]
                [{* Quickorder *}]
                [{block name="footer_services"}]
                [{/block}]
                [{block name="footer_newslink"}]
                [{/block}]

                [{if $oView->isEnabledDownloadableFiles()}]
                <li><a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_downloads"}]">[{oxmultilang ident="MY_DOWNLOADS"}]</a></li>
                [{/if}]

                <li>
                    <a class="bntooltip" data-toggle="tooltip" data-original-title="[{oxmultilang ident="LOADPRICELIST"}]" href="[{$oViewConf->getSelfLink()}]cl=pricelist&fnc=getExcel&name=[{oxmultilang ident="EXCELPRICELISTGET"}]&user=[{$oxcmp_user->oxuser__oxusername->value}]">[{oxmultilang ident="EXCELPRICELIST"}]</a>
                </li>
                <li>
                    <a style="cursor:pointer;" class="bntooltip" data-placement="bottom" data-toggle="tooltip" data-original-title="[{oxmultilang ident="LOADPRICELIST"}]" onclick="loadPDF('[{$oViewConf->getSelfLink()}]cl=pricelist&fnc=getPDF&name=[{oxmultilang ident='PDFPRICELISTGET'}]')" >[{oxmultilang ident="PDFPRICELIST"}]</a>
                </li>
                [{/block}]
            </ul>
        </div>
        <div class="card-footer" style="overflow: hidden;">
            <button onclick="window.location='[{$oViewConf->getLogoutLink() }]'" type="button" class="btn btn-xs btn-outline-danger pull-right bntooltip"
                    data-placement="top" title="[{oxmultilang ident="LOGOUT"}]"><span class="fa fa-power-off"></span></button>
        </div>
    </div>
