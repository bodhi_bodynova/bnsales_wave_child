[{assign var="news" value=$oNews->getlastNews()}]
[{assign var="newsid" value=$news[0]}]
[{assign var="newstitel" value=$news[1]}]
[{assign var="newstext" value=$news[2]}]
[{assign var="newsdate" value=$news[3]}]
<div class="card" style="margin-top: 150px;  height:auto !important; bottom:140px;" id="newsPanel">
    <div class="card-header">
        <h3 class="card-title"><strong>[{oxmultilang ident="NEWS"}]</strong></h3>
    </div>
    <div class="card-body">

                [{* alte News darstellung }]
                <b>[{$newstitel}]&nbsp;&nbsp;&nbsp;<small></small></b>
                [{$newsdate|date_format:"%d.%m.%Y"}] &nbsp; [{$newstext}]
                <div class="">
                    <a class='' data-target='#newsboxmodal' data-toggle='modal' type='button' style="cursor: pointer;">[{oxmultilang ident="ALLENEWSZEIGEN"}]</a>
                </div>
                [{*}]
                [{* neue News darstellung *}]
                [{foreach from=$oNews item=_oNewsItem name=_sNewsList }]

                    <b>[{$_oNewsItem->oxnews__oxshortdesc->value}]&nbsp;&nbsp;&nbsp;<small>[{$_oNewsItem->oxnews__oxdate->value|date_format:"%d.%m.%Y"}]</small></b>
                    <div id="newsBox" class="newsbox">
                        [{$_oNewsItem->getLongDesc()}]
                    </div>
                    <hr/>

                [{/foreach}]
                [{* /neue News darstellung*}]

    </div>
</div>
<div class="startbtns"></div>