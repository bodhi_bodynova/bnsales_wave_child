[{capture append="oxidBlock_pageBody"}]

    [{assign var="slogoImg" value=$oViewConf->getViewThemeParam('sLogoFile')}]
    [{assign var="sLogoWidth" value=$oViewConf->getViewThemeParam('sLogoWidth')}]
    [{assign var="sLogoHeight" value=$oViewConf->getViewThemeParam('sLogoHeight')}]
    [{if !$oxcmp_user->oxuser__oxpassword->value}]
    [{if $oView->getClassName() == "forgotpwd"}]

    <div class="container">

        <a href="[{$oViewConf->getHomeLink()}]" title="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]" class="logo-link">
            <img src="[{$oViewConf->getImageUrl($slogoImg)}]" alt="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]"  class="logo-img">
        </a>



        [{if $oView->showUpdateScreen() }]
        [{assign var="template_title" value="NEW_PASSWORD"|oxmultilangassign}]
        [{elseif $oView->updateSuccess() }]
        [{assign var="template_title" value="CHANGE_PASSWORD"|oxmultilangassign}]
        [{elseif !$oView->updateSuccess()}]
        [{assign var="template_title" value="FORGOT_PASSWORD"|oxmultilangassign}]
        [{/if}]

        <h1 class="pageHead">[{$template_title}]</h1>

        [{if $oView->isExpiredLink() }]
        <div class="box info">[{oxmultilang ident="ERROR_MESSAGE_PASSWORD_LINK_EXPIRED"}]</div>
        [{elseif $oView->showUpdateScreen() }]
        [{include file="form/forgotpwd_change_pwd.tpl"}]
        [{elseif $oView->updateSuccess() }]

        <div class="box info">[{oxmultilang ident="PASSWORD_CHANGED"}]</div>

        <div class="bar">
            <form action="[{$oViewConf->getSelfActionLink()}]" name="forgotpwd" method="post">
                <div>
                    [{ $oViewConf->getHiddenSid() }]
                    <input type="hidden" name="cl" value="start">
                    <button id="backToShop" class="submitButton largeButton" type="submit">
                        [{oxmultilang ident="BACK_TO_SHOP"}]
                    </button>
                </div>
            </form>
        </div>
        [{else}]
        [{if $oView->getForgotEmail()}]
        <div class="box info">[{oxmultilang ident="PASSWORD_WAS_SEND_TO" suffix="COLON"}] [{$oView->getForgotEmail()}]</div>
        <div class="bar">
            <form action="[{$oViewConf->getSelfActionLink()}]" name="forgotpwd" method="post">
                <div>
                    [{$oViewConf->getHiddenSid()}]
                    <input type="hidden" name="cl" value="start">
                    <button id="backToShop" class="submitButton largeButton" type="submit">
                        [{oxmultilang ident="BACK_TO_SHOP"}]
                    </button>
                </div>
            </form>
        </div>
        [{else}]
        [{include file="form/forgotpwd_email.tpl"}]
        [{/if}]
        [{/if}]

        [{if !$oView->isActive('PsLogin') }]
        [{insert name="oxid_tracker" title=$template_title }]
        [{/if}]
    </div>
    [{else}]
    [{include file="widget/header/cookienote.tpl"}]
    <div class="container">
        [{block name="layout_header_logo"}]

        <a href="[{$oViewConf->getHomeLink()}]" title="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]" class="logo-link">
            <img src="[{$oViewConf->getImageUrl($slogoImg)}]" alt="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]"  class="logo-img">
        </a>
        [{/block}]
        <div class="accountLoginView">
            [{*<h1 id="loginAccount" class="pageHead">[{$oView->getTitle()}]</h1>*}]
            <p>[{ oxmultilang ident="LOGIN_ALREADY_CUSTOMER"}]</p>
            [{include file="form/login_account.tpl"}]
        </div>

    </div>
    [{/if}]

    [{else}]

    [{if $oView->showRDFa()}]
        [{include file="rdfa/rdfa.tpl"}]
    [{/if}]

    [{block name="layout_header"}]
        [{include file="layout/header.tpl"}]
    [{/block}]

    [{assign var="blFullwidth" value=$oViewConf->getViewThemeParam('blFullwidthLayout')}]

    <div id="wrapper" [{if $sidebar}]class="sidebar[{$sidebar}]"[{/if}]>

        <div class="underdog">

            [{*//TODO Add option to switch between fullwidth slider and containered slider*}]


            <div class="[{*if $blFullwidth}]container[{else}]container-fluid[{/if*}] container-fluid">
                <div id="wrapper" class="content-box">

                [{*if $oView->getClassName() != "start" && !$blHideBreadcrumb}]
                    [{block name="layout_breadcrumb"}]
                        [{include file="widget/breadcrumb.tpl"}]
                    [{/block}]
                [{/if*}]

                [{$smarty.capture.loginErrors}]

                <div class="row">
                    <div on=false id="sidebarLeft" class="d-none d-sm-none d-md-block d-lg-block col-md-4 col-lg-2 [{$oView->getClassName()}]">
                            [{include file="layout/sidebarLeft.tpl"}]
                    </div>
                    <div id="middelContent" class="col-12 offset-md-4 offset-lg-2 [{*if $sidebar}] col-md-7[{/if*}][{if $oView->getClassName() neq "basket" && $oView->getClassName() neq "user" && $oView->getClassName() neq "order" && $oView->getClassName() neq "adressverwaltung"}]col-md-8 col-lg-7[{else}]col-md-8 col-lg-10[{/if}]">

                        [{* PromoSlider*}]
                        [{if $oView->getClassName()=='start' && $oView->getBanners()|@count > 0 }]
                            [{include file="widget/promosliderNeu.tpl"}]
                        [{/if}]
                        [{* PromoSlider*}]

                        [{if $oView->getClassName() ne "start" &&
                        !$blHideBreadcrumb &&
                        $oView->getClassName() ne "alist" &&
                        $oView->getClassName() ne "search" &&
                        $oView->getClassName() ne "basket" &&
                        $oView->getClassName() ne "user" &&
                        $oView->getClassName() ne "order" &&
                        $oView->getClassName() ne "thankyou" &&
                        $oView->getClassName() ne "quickorder" &&
                        $oView->getClassName() ne "contact" &&
                        $oView->getClassName() ne "account" &&
                        $oView->getClassName() ne "myfavorites" &&
                        $oView->getClassName() ne "content" &&
                        $oView->getClassName() ne "account_newsletter" &&
                        $oView->getClassName() ne "account_password" &&
                        $oView->getClassName() ne "account_user" &&
                        $oView->getClassName() ne "account_order" &&
                        $oView->getClassName() ne "adressverwaltung" &&
                        $oView->getClassName() ne "bnsales_oldnews" &&
                        $oView->getClassName() ne "bnsales_stockinfo"
                        }]
                        [{include file="widget/breadcrumb.tpl"}]
                        [{/if}]
                        [{include file="message/errors.tpl"}]

                        [{foreach from=$oxidBlock_content item="_block"}]
                            [{$_block}]
                            <br>
                        [{/foreach}]
                    </div>
                    [{if $oView->getClassName() neq "basket" && $oView->getClassName() neq "user" && $oView->getClassName() neq "order" && $oView->getClassName() neq "adressverwaltung"}]
                        <div id="sidebarRight" class="d-none d-sm-none d-md-none d-lg-block col-lg-3 [{$oView->getClassName()}]">
                            <div style="width:100% !important;" id="sidebar">
                                [{include file="layout/sidebarRight.tpl"}]
                            </div>
                        </div>
                    [{/if}]
                </div>

            </div>
            </div>
        </div>

    </div>

    [{*include file="layout/footer.tpl"*}]

    [{block name="layout_init_social"}]
    [{/block}]

    <div id="incVatMessage">
        <a href="#" class="back-to-top"><i class="fa fa-arrow-circle-up"></i></a>
        <span class="deliveryInfo">[{oxmultilang ident="INCL_TAX_AND_PLUS_SHIPPING"}]</span>
    </div>[{/if}]
[{/capture}]
[{include file="layout/base.tpl"}]

