[{block name="sidebarRight"}]
    [{block name="Warenkorb"}]

    <div class="card text-center">
        <div class="card-header ">
            <h3 class="card-title pull-left"><strong>[{oxmultilang ident="PAGE_TITLE_BASKET" }]</strong></h3>
        </div>
        [{if $oView->isLanguageLoaded()}]
        <div class="card-body" id="WarenkorbRechts" show="true">
            <form class="navbar-form" role="form">

                [{assign var="counter" value=1}]
                [{assign var="nummer" value=0}]
                [{foreach from=$oxcmp_basket->getContents() name=miniBasketList item=_product}]
                [{if $counter == 0}][{assign var="counter" value=1}][{else}][{assign var="counter" value=0}] [{/if}]

                [{block name="widget_minibasket_product"}]
                [{math assign="nummer" equation="x + y" x=$nummer y=1 }]
                [{assign var="minibasketItemTitle" value=$_product->getTitle()}]
                [{assign var="basketitemkey" value=$_product->getBasketItemKey()}]
                [{assign var="artnum" value=$_product->getArtNum()}]
                [{* $_product->setSort($nummer) *}]
                [{* assign var="sort" value=$_product->getSort() *}]
                [{* $_product->getParentId()|var_dump *}]
                <div id="[{$_product->getProductId()}]" class="row" onclick="jumpToArticle('[{$_product->getParentId()}]','[{$_product->getParentId()}]');" style="cursor:pointer; [{if $counter == 0}]background-color: rgba(0,0,0,.05); border-top:1px solid rgba(0,0,0,.05)[{/if}]">
                    <div class="col-xl-2 d-none d-xl-block" style="padding-left:0">
                        <span id="[{$_product->getProductId()}]amount" class="badge badge-pill badge-secondary sidebarBadge" style="z-index: 10000">[{$_product->getAmount()}]</span>
                        <div class="pictureBoxSB-NoBig d-lg-none d-xl-block"><img src="https://bodynova.de/out/imagehandler.php?artnum=[{$artnum}]&size=450_450_100" class="img-thumbnail"></div>
                    </div>
                    <div class="col-xl-8 col-lg-7 col-9"><p style="-ms-hyphens: auto;-webkit-hyphens: auto;hyphens: auto;">[{$minibasketItemTitle|strip_tags}]</p></div>
                    <div class="col-xl-2 col-lg-5 col-3" style="padding-right:0"><strong class="pull-right" style="white-space: nowrap;">[{oxprice price=$_product->getPrice() currency=$currency}]</strong></div>
                </div>
                [{/block}]
                [{/foreach}]
                <div class="row" style="height:30px; [{if $counter == 1}]background-color: rgba(0,0,0,.05);[{/if}]"></div>
                [{* Rabatte: *}]
                [{foreach from=$oxcmp_basket->getDiscounts() item=oDiscount name=test_Discounts}]
                <hr>
                [{*$oDiscount->formatDiscount()*}]
                <div class="row" id="aufschlagBestellung">
                    <div class="col-10" style="padding-left:0;"><strong class="pull-left">[{if $oDiscount->dDiscount < 0}][{oxmultilang ident="SURCHARGE"}] [{else}][{oxmultilang ident="DISCOUNT"}] [{/if}][{*$oDiscount|var_dump*}]</strong></div>
                    <div class="col-2" style="padding-right:0;white-space: nowrap"><strong class="pull-right">[{oxprice price=$oDiscount->dDiscount*-1 currency=$currency}]</strong></div>
                </div>
                [{/foreach}]
                [{* GesamtSumme: *}]
                [{block name="widget_minibasket_total"}]
                <div class="row">
                    <div class="col-6" style="padding-left:0"><strong class="pull-left">[{oxmultilang ident="TOTAL"}]</strong></div>
                    <div class="col-6" style="padding-right:0;white-space: nowrap">
                        <strong class="pull-right">
                            [{if $oxcmp_basket->isPriceViewModeNetto()}]
                            [{oxprice price=$oxcmp_basket->_getDiscountedProductsSum() currency=$currency}]
                            [{else}]
                            [{$oxcmp_basket->getFProductsPrice()}]
                            [{/if}]
                        </strong>
                    </div>
                </div>
                [{/block}]
            </form>
        </div>
        [{/if}]
        <div class="card-footer ">
            [{* Wk-Ansicht Buttons: *}]
            <button style="margin-bottom:0; margin-left: 0 !important;" type="button" class="btn btn-outline-info btn ladda-button bntooltip pull-left" data-style="expand-right" onclick="WarenkorbHide(this)" data-toggle="tooltip" data-placement="right" title="[{oxmultilang ident="TT-WK-CLOSE"}]" ><i class="fa fa-chevron-up"></i></button>
            <a style="margin-bottom:0;margin-right:0;float:right;" class="minib btn btn-outline-success btn ladda-button bntooltip" href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=basket" }]" data-toogle="tooltip" data-placement="left" title="[{oxmultilang ident="TT-TO-CHECKOUT"}]"><span class="glyphicon glyphicon-shopping-cart"></span> [{oxmultilang ident="CHECKOUT"}]</a>

            [{oxscript widget=$oView->getClassName()}]
        </div>
    </div>
    [{/block}]
    [{* DEBUG Panel only Visible for Admins: *}]
    [{*if $oxcmp_user->oxuser__oxrights->value eq 'malladmin'}]
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">DEBUG: Only for Admins</h3>
			</div>
			<div class="panel-body">
				Oxcmp_Basket:<br>
				<pre>
					[{$oxcmp_basket|var_dump}]
				</pre>
			</div>
		</div>
	[{/if*}]
    [{/block}]
