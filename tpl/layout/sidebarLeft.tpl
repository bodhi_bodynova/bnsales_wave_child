[{block name="sidebarLeft"}]
    <div id="sidebar">
        [{if $oxcmp_user->oxuser__oxrights->value eq 'malladmin'}]
        <div class="card">

            <div class="card-header">
                <h1 class="card-title">[{oxmultilang ident="SIDEBAR_FAVORITES"}]</h1>
            </div>
            <div class="card-body">
                <p id="AnleitungFavoriten" class="hidden">
                    Sie können alles was ein <span class="glyphicon glyphicon-pushpin"></span> hat hierher ziehen und abspeichern.
                </p>
                [{if $oxcmp_user->hasUserFavorites()}]
                <ul class="tree nav nav-pills nav-stacked connectedSortable" role="tablist" id="userFavoriten" userid="[{$oxcmp_user->oxuser__oxid->value}]">
                    <li class="">
                        <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=alist&cnid=fc70cac08dba4163848d70cbab85d2dc"}]">[{oxmultilang ident="ANGEBOTE"}]</a>
                    </li>
                    <li class="">
                        <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=alist&cnid=b9c44cdf5adce577c5d1e08d7dd304be"}]">[{oxmultilang ident="NEUHEITEN"}]</a>
                    </li>
                    <li class="">
                        <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_order"}]" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="TIP_MY_ORDERS"}]"><span class="ladda-label">[{oxmultilang ident="BTN_MY_ORDERS"}]</span></a>
                    </li>
                    [{foreach from=$oxcmp_user->getUserFavorites() name=favoritesList item=_fav}]
                    [{if $_fav[4] eq 'oxcategories'}]
                    [{assign var=link value="cl=alist&cnid="|cat:$_fav[3] }]
                    <li class="" oxid="[{$_fav[3]}]" sort="[{$_fav[5]}]" type="[{$_fav[4]}]" text="[{$_fav[6]}]" id="[{$_fav[0]}]">
                        <a  style="" href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:$link}]">[{$_fav[6]}]</a>
                        <button class="btn btn-xs hidden-trash bntooltip" style="float:right;margin-top: -24px; margin-right:3px;z-index: 10000;" onclick="deleteFavorite('[{$_fav[0]}]')" data-toggle="tooltip" data-placement="left" title="[{oxmultilang ident="TT-DELETE"}]"><span class="glyphicon glyphicon-trash"></span></button>
                    </li>
                    [{/if}]
                    [{/foreach}]
                </ul>
                [{else}]
                <ul class="tree nav nav-pills nav-stacked connectedSortable" role="tablist" id="userFavoriten" userid="[{$oxcmp_user->oxuser__oxid->value}]"></ul>
                <div id="standardfavoriten">
                    <ul class="services list-unstyled" id="Leftsortable">
                        <li class="">
                            <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=alist&cnid=fc70cac08dba4163848d70cbab85d2dc"}]">[{oxmultilang ident="ANGEBOTE"}]</a>
                        </li>
                        <li class="">
                            <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=alist&cnid=b9c44cdf5adce577c5d1e08d7dd304be"}]">[{oxmultilang ident="NEUHEITEN"}]</a>
                        </li>
                        <li class="">
                            <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_order"}]" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="TIP_MY_ORDERS"}]"><span class="ladda-label">[{oxmultilang ident="BTN_MY_ORDERS"}]</span></a>
                        </li>
                        <li class="">
                            <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=myfavorites&fnc=showLists"}]" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="TIP_MY_FAVORITES"}]"><span class="ladda-label">[{oxmultilang ident="BTN_MY_FAVORITES"}]</span></a>
                        </li>
                        [{block name="stockinfoList"}]
                        [{/block}]
                    </ul>
                </div>
                [{/if}]
            </div>
            <div class="card-footer">
                <div class="btn-group btn-group-justified" role="group">
                    [{* bearbeiten Button *}]
                    <div class="btn-group" role="group">
                        <button style="margin-left:0 !important;" type="button" id="bearbeiten-btn" href="#" class="btn btn-info btn-xs bntooltip" onclick="setbearbeiten('[{$oxcmp_user->oxuser__oxid->value}]')" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="TT-EDIT"}]">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </button>
                    </div>
                    [{* speichern Button *}]
                    <div class="btn-group" role="group">
                        <button type="button" id="speichern-btn" href="#" class="btn btn-sucess btn-xs bntooltip" onclick="speichernUserFavoriten()" disabled="disabled" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="TT-SAVE"}]">
                            <i class="glyphicon glyphicon-save"></i>
                        </button>
                    </div>
                    [{* löschen Button *}]
                    <div class="btn-group" role="group">
                        <button type="button" id="abbruch-btn" href="#" class="btn btn-alert btn-xs bntooltip" onclick="abbruchSpeichernUserFavoriten('[{$oxcmp_user->oxuser__oxid->value}]')" disabled="disabled" data-toggle="tooltip" data-placement="top" title="[{oxmultilang ident="TT-DELETE-ALL"}]">
                            <i class="glyphicon glyphicon-trash"></i>
                        </button>
                    </div>
                </div>
                <div id="bearbeitbar"></div>
            </div>
        </div>
        [{else}]


        <div class="card">
            <div class="card-header">
                <h1 class="card-title"><strong>[{oxmultilang ident="SIDEBAR_FAVORITES"}]</strong></h1>
            </div>
            <div class="card-body">

                <div id="standardfavoriten">
                    <ul class="tree list-unstyled" id="Leftsortable">
                        <li class="">
                            <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=alist&cnid=fc70cac08dba4163848d70cbab85d2dc"}]">[{oxmultilang ident="ANGEBOTE"}]</a>
                        </li>
                        <li class="">
                            <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=alist&cnid=b9c44cdf5adce577c5d1e08d7dd304be"}]">[{oxmultilang ident="NEUHEITEN"}]</a>
                        </li>
                        <li class="">
                            <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_order"}]" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="TIP_MY_ORDERS"}]"><span class="ladda-label">[{oxmultilang ident="BTN_MY_ORDERS"}]</span></a>
                        </li>
                        <li class="">
                            <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=myfavorites&fnc=showLists"}]" data-toggle="tooltip" data-placement="bottom" title="[{oxmultilang ident="TIP_MY_FAVORITES"}]"><span class="ladda-label">[{oxmultilang ident="BTN_MY_FAVORITES"}]</span></a>
                        </li>
                        [{block name="stockinfoList"}]
                        [{/block}]
                    </ul>
                </div>
            </div>
        </div>


        [{/if}]
        [{* Service Panel:*}]


        [{oxid_include_widget cl="oxwServiceList" noscript=1 nocookie=1}]

        [{* Information Panel: *}]
        [{oxid_include_widget cl="oxwInformation" noscript=1 nocookie=1}]

        [{* DEBUG Panel only Visible for Admins: *}]
        [{if $oxcmp_user->oxuser__oxrights->value eq 'malladmin'}]
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Only for Admin eyes:</h3>
            </div>
            <div class="card-body">
                (Standardfavoriten)
                <a href="#" onclick="showTour();">Tour</a> only for Info<br>
                + Meine Bestellungen im Service <br>
                Klasse:
                [{$oView->getClassName()}]<br>
                Template:
                [{$oView->getTemplateName()}]<br>
                BreadCrumb:
                [{$oView->getBreadCrumb()|var_dump}]
                Tests:
                <button class="btn btn-default pull-left" onclick="testalert()">TestAlert</button>
            </div>
        </div>
        [{/if}]
    </div>

    [{/block}]
