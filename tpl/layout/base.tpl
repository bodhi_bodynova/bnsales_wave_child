[{* Important ! render page head and body to collect scripts and styles *}]
[{capture append="oxidBlock_pageHead"}]
    [{strip}]
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" id="Viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=[{$oView->getCharSet()}]">
    [{assign var="_sMetaTitlePrefix" value=$oView->getTitlePrefix()}]
    [{assign var="_sMetaTitleSuffix" value=$oView->getTitleSuffix()}]
    [{assign var="_sMetaTitlePageSuffix" value=$oView->getTitlePageSuffix()}]
    [{assign var="_sMetaTitle" value=$oView->getTitle()}]

    [{if !$_sMetaTitle }]
    [{assign var="_sMetaTitle" value=$template_title }]
    [{/if}]


    [{assign var=sPageTitle value=$oView->getPageTitle()}]

    <title>[{ $_sMetaTitlePrefix }][{if $_sMetaTitlePrefix && $_sMetaTitle }] | [{/if}][{$_sMetaTitle|strip_tags}][{if $_sMetaTitleSuffix && ($_sMetaTitlePrefix || $_sMetaTitle) }] | [{/if}][{$_sMetaTitleSuffix}] [{if $_sMetaTitlePageSuffix }] | [{ $_sMetaTitlePageSuffix }] [{/if}]</title>
<meta name="google-site-verification" content="eks5JttGMgnHwp6hO8jWB1CQlTMRjT3P6PfGtfSRiQA" />

    [{block name="head_meta_robots"}]
    [{if $oView->noIndex() == 1}]
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
    [{elseif $oView->noIndex() == 2}]
<meta name="ROBOTS" content="NOINDEX, FOLLOW">
    [{/if}]
    [{/block}]

    [{block name="head_meta_description"}]
    [{if $oView->getMetaDescription()}]
<meta name="description" content="[{$oView->getMetaDescription()}]">
    [{/if}]
    [{/block}]

    [{block name="head_meta_keywords"}]
    [{if $oView->getMetaKeywords()}]
<meta name="keywords" content="[{$oView->getMetaKeywords()}]">
    [{/if}]
    [{/block}]


    [{*block name="head_meta_open_graph"}]
        <meta property="og:site_name" content="[{$oViewConf->getBaseDir()}]">
        <meta property="og:title" content="[{$sPageTitle}]">
        <meta property="og:description" content="[{$oView->getMetaDescription()}]">
        [{if $oViewConf->getActiveClassName() == 'details'}]
            <meta property="og:type" content="product">
            <meta property="og:image" content="[{$oView->getActPicture()}]">
            <meta property="og:url" content="[{$oView->getCanonicalUrl()}]">
        [{else}]
            <meta property="og:type" content="website">
            <meta property="og:image" content="[{$oViewConf->getImageUrl('basket.png')}]">
            <meta property="og:url" content="[{$oViewConf->getCurrentHomeDir()}]">
        [{/if}]
    [{/block*}]

    [{assign var="canonical_url" value=$oView->getCanonicalUrl()}]
    [{block name="head_link_canonical"}]
    [{if $canonical_url}]
<link rel="canonical" href="[{$canonical_url}]">
    [{/if}]
    [{/block}]

    [{block name="head_link_hreflang"}]
    [{if $oView->isLanguageLoaded()}]
    [{assign var="oConfig" value=$oViewConf->getConfig()}]
    [{foreach from=$oxcmp_lang item=_lng}]
    [{if $_lng->id == $oConfig->getConfigParam('sDefaultLang')}]
<link rel="alternate" hreflang="x-default" href="[{$_lng->link}]"/>
    [{/if}]
<link rel="alternate" hreflang="[{$_lng->abbr}]" href="[{$_lng->link|oxaddparams:$oView->getDynUrlParams()}]"/>
    [{/foreach}]
    [{/if}]
    [{/block}]

    [{assign var="oPageNavigation" value=$oView->getPageNavigation()}]
    [{if $oPageNavigation}]
    [{if $oPageNavigation->previousPage}]
<link rel="prev" href="[{$oPageNavigation->previousPage}]">
    [{/if}]
    [{if $oPageNavigation->nextPage}]
<link rel="next" href="[{$oPageNavigation->nextPage}]">
    [{/if}]
    [{/if}]

    [{block name="head_link_favicon"}]
    [{assign var="sFavicon512File" value=$oViewConf->getViewThemeParam('sFavicon512File')}]
    [{if $sFavicon512File}]
<!-- iOS Homescreen Icon (version < 4.2)-->
<link rel="apple-touch-icon-precomposed" media="screen and (resolution: 163dpi)" href="[{$oViewConf->getImageUrl("favicons/`$sFavicon512File`")}]" />
<!-- iOS Homescreen Icon -->
<link rel="apple-touch-icon-precomposed" href="[{$oViewConf->getImageUrl("favicons/`$sFavicon512File`")}]" />

<!-- iPad Homescreen Icon (version < 4.2) -->
<link rel="apple-touch-icon-precomposed" media="screen and (resolution: 132dpi)" href="[{$oViewConf->getImageUrl("favicons/`$sFavicon512File`")}]" />
<!-- iPad Homescreen Icon -->
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="[{$oViewConf->getImageUrl("favicons/`$sFavicon512File`")}]" />

<!-- iPhone 4 Homescreen Icon (version < 4.2) -->
<link rel="apple-touch-icon-precomposed" media="screen and (resolution: 326dpi)" href="[{$oViewConf->getImageUrl("favicons/`$sFavicon512File`")}]" />
<!-- iPhone 4 Homescreen Icon -->
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="[{$oViewConf->getImageUrl("favicons/`$sFavicon512File`")}]" />

<!-- new iPad Homescreen Icon and iOS Version > 4.2 -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="[{$oViewConf->getImageUrl("favicons/`$sFavicon512File`")}]" />

<!-- Windows 8 -->
    [{assign var="sFaviconMSTileColor" value=$oViewConf->getViewThemeParam('sFaviconMSTileColor')}]
    [{if $sFaviconMSTileColor}]
<meta name="msapplication-TileColor" content="[{$sFaviconMSTileColor}]"> <!-- Kachel-Farbe -->
<meta name="theme-color" content="[{$sFaviconMSTileColor}]"/>
    [{/if}]
<meta name="msapplication-TileImage" content="[{$oViewConf->getImageUrl("favicons/`$sFavicon512File`")}]">

<!-- Fluid -->
<link rel="fluid-icon" href="[{$oViewConf->getImageUrl("favicons/`$sFavicon512File`")}]" title="[{$sPageTitle}]" />
    [{/if}]

<!-- Shortcut Icons -->
    [{assign var="sFaviconFile"    value=$oViewConf->getViewThemeParam('sFaviconFile')}]
    [{assign var="sFavicon16File"  value=$oViewConf->getViewThemeParam('sFavicon16File')}]
    [{assign var="sFavicon32File"  value=$oViewConf->getViewThemeParam('sFavicon32File')}]
    [{assign var="sFavicon48File"  value=$oViewConf->getViewThemeParam('sFavicon48File')}]
    [{assign var="sFavicon64File"  value=$oViewConf->getViewThemeParam('sFavicon64File')}]
    [{assign var="sFavicon128File" value=$oViewConf->getViewThemeParam('sFavicon128File')}]

    [{if $sFaviconFile}]
<link rel="shortcut icon" href="[{$oViewConf->getImageUrl("favicons/`$sFaviconFile`")}]?rand=1" type="image/x-icon" />
    [{/if}]
    [{if $sFavicon16File}]
<link rel="icon" href="[{$oViewConf->getImageUrl("favicons/`$sFavicon16File`")}]" sizes="16x16" />
    [{/if}]
    [{if $sFavicon32File}]
<link rel="icon" href="[{$oViewConf->getImageUrl("favicons/`$sFavicon32File`")}]" sizes="32x32" />
    [{/if}]
    [{if $sFavicon48File}]
<link rel="icon" href="[{$oViewConf->getImageUrl("favicons/`$sFavicon48File`")}]" sizes="48x48" />
    [{/if}]
    [{if $sFavicon64File}]
<link rel="icon" href="[{$oViewConf->getImageUrl("favicons/`$sFavicon64File`")}]" sizes="64x64" />
    [{/if}]
    [{if $sFavicon128File}]
<link rel="icon" href="[{$oViewConf->getImageUrl("favicons/`$sFavicon128File`")}]" sizes="128x128" />
    [{/if}]
    [{/block}]

    [{block name="base_style"}]
    [{oxstyle include="css/cssGlobal.css"}]
    [{oxstyle include="css/styles.min.css"}]
    [{*oxstyle include="css/bootstrap4.css"*}]
    [{*oxstyle include="boot-tour/build/css/bootstrap-tour-standalone.css"*}]
    [{*oxstyle include="css/select2-bootstrap.css"*}]
    [{*oxstyle include="css/font-awesome.min.css"*}]
    [{oxstyle include="css/ladda-themeless.min.css"}]
    [{*oxstyle include="css/dre_less.css"*}]
    [{*oxstyle include="css/normalize.css"*}]
    [{*oxstyle include="css/dre_bodynova.css"*}]
    [{*oxstyle include="css/libs/anythingslider.css"*}]
    [{*oxstyle include="css/dre_update.css"*}]
    [{oxstyle include="css/jquery-ui.css"}]

    [{oxstyle include="alertify/themes/alertify.core.css"}]
    [{oxstyle include="alertify/themes/alertify.default.css"}]
    [{oxstyle include="alertify/themes/alertify.bootstrap.css"}]
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.12.0/css/all.css" integrity="sha384-ekOryaXPbeCpWQNxMwSWVvQ0+1VrStoPJq54shlYhR8HzQgig1v5fas6YgOqLoKz" crossorigin="anonymous">


    [{/block}]
    [{block name="base_js"}]
    [{oxscript include="js/libs/jquery.min.js" priority=1}]
    [{*oxscript include="js/libs/jquery.min.js" priority=1*}]
    [{oxscript include=$oViewConf->getResourceUrl('js/jquery-2.1.1.min.js') priority=1}]
    [{oxscript include="js/libs/jquery-ui.min.js" priority=1}]
    [{oxscript include=$oViewConf->getResourceUrl('js/jquery.form.min.js') priority=1}]
    [{oxscript include=$oViewConf->getResourceUrl('js/bootstrap.min.js') priority=1}]
    [{oxscript include=$oViewConf->getResourceUrl('js/select2-3.5.1/select2.min.js') priority=1}]
    [{oxscript include=$oViewConf->getResourceUrl('js/spin.min.js') priority=1}]
    [{oxscript include=$oViewConf->getResourceUrl('js/ladda.min.js') priority=1}]

    [{*oxscript include=$oViewConf->getResourceUrl('js/lupe.js') priority=1*}]
    [{oxscript include=$oViewConf->getResourceUrl('js/jquery.viewport.js') priority=1}]
    [{oxscript include=$oViewConf->getResourceUrl('js/jquery.imageloader.js') priority=1}]
    [{oxscript include=$oViewConf->getResourceUrl('js/bodynova.js') priority=1}]
    [{*}]<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>[{*}]
    [{*oxscript include=$oViewConf->getResourceUrl('js/bootstrap-tour-standalone.js') priority=1}]
	    [{oxscript include=$oViewConf->getResourceUrl('js/bodynova-tour.js') priority=1*}]
    [{* Alertify: *}]
    [{oxscript include=$oViewConf->getResourceUrl('alertify/lib/alertify.min.js') priority=1}]
    [{include file="i18n/js_vars.tpl"}]
    [{oxscript include="js/script.min.js" priority=1}]
    [{/block}]


    [{block name="base_fonts"}]
    [{/block}]

    [{assign var='rsslinks' value=$oView->getRssLinks()}]
    [{block name="head_link_rss"}]
    [{if $rsslinks}]
    [{foreach from=$rsslinks item='rssentry'}]
<link rel="alternate" type="application/rss+xml" title="[{$rssentry.title|strip_tags}]" href="[{$rssentry.link}]">
    [{/foreach}]
    [{/if}]
    [{/block}]

    [{block name="head_css"}]
    [{foreach from=$oxidBlock_head item="_block"}]
    [{$_block}]
    [{/foreach}]
    [{/block}]
    [{/strip}]
    [{/capture}]

[{assign var="blIsCheckout"     value=$oView->getIsOrderStep()}]
[{assign var="blFullwidth"      value=$oViewConf->getViewThemeParam('blFullwidthLayout')}]
[{assign var="sBackgroundColor" value=$oViewConf->getViewThemeParam('sBackgroundColor')}]

[{* Fullpage Background *}]
[{*if $oViewConf->getViewThemeParam('blUseBackground')}]
    [{assign var="sBackgroundPath"          value=$oViewConf->getViewThemeParam('sBackgroundPath')}]
    [{assign var="sBackgroundUrl"           value=$oViewConf->getImageUrl("backgrounds/`$sBackgroundPath`")}]
    [{assign var="sBackgroundRepeat"        value=$oViewConf->getViewThemeParam('sBackgroundRepeat')}]
    [{assign var="sBackgroundPosHorizontal" value=$oViewConf->getViewThemeParam('sBackgroundPosHorizontal')}]
    [{assign var="sBackgroundPosVertical"   value=$oViewConf->getViewThemeParam('sBackgroundPosVertical')}]
    [{assign var="sBackgroundSize"          value=$oViewConf->getViewThemeParam('sBackgroundSize')}]
    [{assign var="blBackgroundAttachment"   value=$oViewConf->getViewThemeParam('blBackgroundAttachment')}]

    [{if $sBackgroundUrl}]
        [{assign var="sStyle" value="background:`$sBackgroundColor` url(`$sBackgroundUrl`) `$sBackgroundRepeat` `$sBackgroundPosHorizontal` `$sBackgroundPosVertical`;"}]

        [{if $sBackgroundSize}]
            [{assign var="sStyle" value=$sStyle|cat:"background-size:`$sBackgroundSize`;"}]
        [{/if}]

        [{if $blBackgroundAttachment}]
            [{assign var="sStyle" value=$sStyle|cat:"background-attachment:fixed;"}]
        [{/if}]
    [{else}]
        [{assign var="sStyle" value="background:`$sBackgroundColor`;"}]
    [{/if}]
[{elseif $sBackgroundColor}]
    [{assign var="sStyle" value="background:`$sBackgroundColor`;"}]
[{/if*}]

<!DOCTYPE html>
<html lang="[{$oView->getActiveLangAbbr()}]" [{block name="head_html_namespace"}][{/block}]>
<head>
    [{foreach from=$oxidBlock_pageHead item="_block"}]
    [{$_block}]
    [{/foreach}]
    [{oxstyle}]

    [{*if $sStyle}]
            <style>
                body {
                    [{$sStyle}]
                }
            </style>
        [{/if*}]
</head>

<body class="cl-[{$oView->getClassName()}][{if $smarty.get.plain == '1'}] popup[{/if}][{if $blIsCheckout}] is-checkout[{/if}][{if $oxcmp_user && $oxcmp_user->oxuser__oxpassword->value}] is-logged-in[{/if}]">

[{* Theme SVG icons block *}]
[{block name="theme_svg_icons"}][{/block}]


<div class="[{if $blFullwidth}]container-fluid[{else}]container[{/if}]">
    <div class="main-row">
        [{foreach from=$oxidBlock_pageBody item="_block"}]
        [{$_block}]
        [{/foreach}]
    </div>
</div>


[{if $oViewConf->getTopActiveClassName() == 'details' && $oView->showZoomPics()}]
    [{include file="page/details/inc/photoswipe.tpl"}]
    [{/if}]

[{*block name="base_js"}]
        [{include file="i18n/js_vars.tpl"}]
        [{oxscript include="js/script.min.js" priority=1}]
    [{/block*}]

[{if $oViewConf->isTplBlocksDebugMode()}]
    [{oxscript include="js/widgets/oxblockdebug.min.js"}]
    [{oxscript add="$( 'body' ).oxBlockDebug();"}]
    [{/if}]

[{oxscript}]

[{if !$oView->isDemoShop()}]
    [{oxid_include_dynamic file="widget/dynscript.tpl"}]
    [{/if}]

[{foreach from=$oxidBlock_pageScript item="_block"}]
    [{$_block}]
    [{/foreach}]
[{*
<div class="d-block [{if ($oView->getClassName() != 'start' && $oView->getClassName() != 'account') || $oxcmp_user}]d-md-none[{else}]container[{/if}] infoAGB" style="[{if $oView->getClassName() == 'start'}]margin-top:50px;[{/if}]">
    [{include file="widget/footer/info.tpl"}]
</div>
*}]
</body>
</html>
