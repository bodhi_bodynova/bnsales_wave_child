<?php
/**
 * This file is part of OXID eSales Wave theme.
 *
 * OXID eSales Wave theme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eSales Wave theme is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eSales Wave theme.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 */

$sLangName = "English";

$aLang = array(
    'charset'                                               => 'UTF-8',


    'BTN_MY_FAVORITES'              =>	'My Favorites',
    'BTN_MYFAVORITES_LISTS'         =>	'My Favorites',
    'MYFAVORITES_LISTS_DESC'        =>	'Manage your favorites',
    'TO_THE_BASKET'                 =>	'Add to basket',
    'SHOW_OPTIONS'                  =>	'Show options',
    'ADD_TO_FAVORITE'               =>	'Add to favorites',
    'ADDED_TO_BASKET_CONFIRMATION'  =>	'Article was added to basket',
    'OPEN_ALL_VARIANTS'             =>	'Open all options',
    'SHOW_ARTICLE_DETAILS'          =>	'Show details',
    'AlleArtikel'                   =>	'Show all articles',
    'TOTAL_WEIGHT'                  =>	'Total weight',
    'ACCOUNT_LOGIN_REQUIRE'         =>	'You must be logged in to use this site.',
    'Modal_Title'                   =>	'Article details',
    'THANK_YOU_MESSAGE'             =>	'Thank you for your message. We will contact you asap.',
    'AGB_LINK_TITLE'                =>	'AGB',
    'IMPRINT_LINK_TITLE'            =>	'Impressum',
    'BTN_MY_ORDERS'                 =>	'My orders',
    'STREET'                        =>	'Street',
    'STREETNO'                      =>	'House number',
    'CITY'                          =>	'City',
    'DATEI'							=>	'Attach delivery slip',
    'SUBMITFILE'					=>	'Upload',
    'ALLTOBASKET'					=>	'All articles in the basket.',
    'RESET_AMOUNT'					=>	'Reset amount',
    'CA'							=>	'ca.',
    'Kommentar'                     => 'Comment',
    'SURCHARGE'                     => 'Surcharge for orders under 75 €',
    'CancelAddressChange'           => 'Cancel',


    // Global
    'DD_SORT_DESC'                                          => 'descending',
    'DD_SORT_ASC'                                           => 'ascending',
    'DD_DEMO_ADMIN_TOOL'                                    => 'Open admin interface',
    'DD_DELETE'                                             => 'Delete',

    // Form validation
    'DD_FORM_VALIDATION_VALIDEMAIL'                         => 'Please enter a valid email address.',
    'DD_FORM_VALIDATION_PASSWORDAGAIN'                      => 'Passwords do not match.',
    'DD_FORM_VALIDATION_NUMBER'                             => 'Please enter a number.',
    'DD_FORM_VALIDATION_INTEGER'                            => 'There are no decimal places allowed.',
    'DD_FORM_VALIDATION_POSITIVENUMBER'                     => 'Please enter a positive number.',
    'DD_FORM_VALIDATION_NEGATIVENUMBER'                     => 'Please enter a negative number.',
    'DD_FORM_VALIDATION_REQUIRED'                           => 'Please specify a value for this required field.',
    'DD_FORM_VALIDATION_CHECKONE'                           => 'Please select at least one option.',

    // Header
    'SEARCH_TITLE'                                          => 'Enter a search term...',
    'SEARCH_SUBMIT'                                         => 'Search',

    // Footer
    'FOOTER_NEWSLETTER_INFO'                                => 'Get informed about the latest products and offers per email.',

    // Home page
    'MANUFACTURERSLIDER_SUBHEAD'                            => 'We present our carefully selected brands, whose products can be found in our shop.',
    'START_BARGAIN_HEADER'                                  => 'Week\'s Special',
    'START_NEWEST_HEADER'                                   => 'Just arrived',
    'START_TOP_PRODUCTS_HEADER'                             => 'Top seller',
    'START_BARGAIN_SUBHEADER'                               => 'Save money with our current bargains!',
    'START_NEWEST_SUBHEADER'                                => 'Fresh as it gets. One minute in the box and now already in store.',
    'START_TOP_PRODUCTS_SUBHEADER'                          => 'Only %s products, but the best we can offer you.',

    // Contact form
    'DD_CONTACT_PAGE_HEADING'                               => 'Contact us!',
    'DD_CONTACT_FORM_HEADING'                               => 'Contact',

    // Link list
    'DD_LINKS_NO_ENTRIES'                                   => 'Unfortunately, there are no links available.',

    // 404 page
    'DD_ERR_404_START_TEXT'                                 => 'You may find the information you want from our home page:',
    'DD_ERR_404_START_BUTTON'                               => 'Go to home page',
    'DD_ERR_404_CONTACT_TEXT'                               => 'May we assist you?<br>Feel free to call us or write an email:',
    'DD_ERR_404_CONTACT_BUTTON'                             => 'to the contact page',

    // Login
    'DD_LOGIN_ACCOUNT_PANEL_CREATE_BODY'                    => 'By creating an account with our store, you will be guided through the checkout process faster. In addition, you can store multiple shipping addresses and track orders in your account.',

    // Billing address
    'DD_USER_LABEL_STATE'                                    => 'State',
    'DD_USER_SHIPPING_SELECT_ADDRESS'                       => 'select',
    'DD_USER_SHIPPING_ADD_DELIVERY_ADDRESS'                 => 'add delivery address',
    'DD_DELETE_SHIPPING_ADDRESS'                            => 'Delete shipping address',

    // List views
    'DD_LISTLOCATOR_FILTER_ATTRIBUTES'                      => 'Filter',
    'DD_LIST_SHOW_MORE'                                     => 'View products',

    // Recommendation list
    'DD_RECOMMENDATION_EDIT_BACK_TO_LIST'                   => 'back to overview',

    // Downloads
    'DD_DOWNLOADS_DOWNLOAD_TOOLTIP'                         => 'downloaded',
    'DD_FILE_ATTRIBUTES_FILESIZE'                           => 'File size:',
    'DD_FILE_ATTRIBUTES_OCLOCK'                             => 'o\'clock',
    'DD_FILE_ATTRIBUTES_FILENAME'                           => 'File name:',

    // Details page
    'BACK_TO_OVERVIEW'                                      => 'to overview',
    'OF'                                                    => 'of',
    'DD_RATING_CUSTOMERRATINGS'                             => 'Customer reviews',
    'PAGE_DETAILS_CUSTOMERS_ALSO_BOUGHT_SUBHEADER'          => 'Customers who bought this item also bought one of the following products.',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_ACCESSORIES_SUBHEADER' => 'The following products fit well to this product.',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_SIMILAR_SUBHEADER'     => 'Take a look at our similar products.',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_CROSSSELING_SUBHEADER' => 'Customers who viewed this product also viewed the following products.',
    'DETAILS_VPE_MESSAGE_1'                                 => "This product can only be ordered in packaging units of ",
    'DETAILS_VPE_MESSAGE_2'                                 => "",

    // Modal basket
    'DD_MINIBASKET_MODAL_TABLE_PRICE'                       => 'Total',
    'DD_MINIBASKET_CONTINUE_SHOPPING'                       => 'continue shopping',

    // Checkout
    'DD_BASKET_BACK_TO_SHOP'                                => 'back to shop',

    // E-Mails
    'DD_FOOTER_FOLLOW_US'                                   => 'Follow us:',
    'DD_FORGOT_PASSWORD_HEADING'                            => 'Forgot password',
    'DD_INVITE_HEADING'                                     => 'Article recommendation',
    'DD_INVITE_LINK'                                        => 'Link',
    'DD_NEWSLETTER_OPTIN_HEADING'                           => 'Your newsletter subscription',
    'DD_ORDERSHIPPED_HEADING'                               => 'Delivery confirmation - Order',
    'DD_PRICEALARM_HEADING'                                 => 'Pricealarm',
    'DD_REGISTER_HEADING'                                   => 'Your registration',
    'DD_DOWNLOADLINKS_HEADING'                              => 'Your download links - Order',
    'DD_WISHLIST_HEADING'                                   => 'Wishlist',

    'DD_ROLES_BEMAIN_UIROOTHEADER'                          => 'Menu',

    'DD_DELETE_MY_ACCOUNT_WARNING'                          => 'This action cannot be undone. This will permanently delete your personal data.',
    'DD_DELETE_MY_ACCOUNT'                                  => 'Delete account',
    'DD_DELETE_MY_ACCOUNT_CONFIRMATION_QUESTION'            => 'Are you sure you want to delete your account?',
    'DD_DELETE_MY_ACCOUNT_CANCEL'                           => 'Cancel',
    'DD_DELETE_MY_ACCOUNT_SUCCESS'                          => 'The account has been deleted',
    'DD_DELETE_MY_ACCOUNT_ERROR'                            => 'The account could not have been deleted',

    // Account -> My product reviews
    'DD_DELETE_REVIEW_AND_RATING'                           => 'Delete review and star rating',
    'DD_REVIEWS_NOT_AVAILABLE'                              => 'No reviews available',
    'DD_DELETE_REVIEW_CONFIRMATION_QUESTION'                => 'Are you sure you want to delete the review?',

    // Contact page
    'DD_SELECT_SALUTATION'                                  => 'Please choose',

    'DD_CATEGORY_RESET_BUTTON'                              => 'Reset',


    'NEUHEITEN'							=>	'New products',
    'ANGEBOTE'							=>	'Sale',
    'ARTNR'                             =>  'Art. Nr.',
    //Zusätzliche Übersetzungen

    //'AlleArtikel'                                           => 'Show all articles',
    'AKTIONBIS'                                             => 'on sale until ',
    'AUSLAUFARTIKEL'                                        => 'Article will be discontinued',
    'LOADPRICELIST'                                         => 'Loading all items may take a moment',
    'EXCELPRICELIST'                                        => 'Excel price list',
    'PDFPRICELIST'                                          => 'PDF price list',
    'EXCELPRICELISTGET'                                     => 'All_Products',
    'PDFPRICELISTGET'                                       => 'All_Products',

    //Excel-Output
    'EXCEL_NAME'                                            =>  'Name',
    'EXCEL_VARNAME'                                         =>  'Variant',
    'EXCEL_ARTNUM'                                          =>  'ArtNr',
    'EXCEL_PRICE'                                           =>  'UP/€',
    'EXCEL_UVP'                                             =>  'UVP/€',
    'EXCEL_EAN'                                             =>  'EAN',
    'EXCEL_AMPEL'                                           => 'Availabilty',
    'EXCEL_GEWICHT'                                         => 'Weight',
    'EXCEL_AKTION'                                          => 'Sale',
    'EXCEL_RECALL'                                          => 'Recall',
    'EXCEL_RECALLGRUND'                                     => 'Reason for recall',
    'EXCEL_AKTIONENDE'                                      => 'End of sale',
    'EXCEL_AMPEL_0'                                         => 'In stock',
    'EXCEL_AMPEL_1'                                         => 'Limited stock',
    'EXCEL_AMPEL_2'                                         => 'Out of stock',
    'EXCEL_AUSLAUFARTIKEL'                                  => 'Will be discontinued',
    'EXCEL_REASON'                                          =>  'Broken',


    /* TOOLTIPS */
    'TIP_ALLTOBASKET'				=>	'Add all selected articles to basket',
    'TIP_SHOW_OPTIONS'				=>	'Show all available options',
    'TIP_TO_THE_BASKET'				=>	'Add to basket',
    'TIP_MY_ORDERS'					=>	'Here you can see your previous orders and add to basket',
    'TIP_MY_FAVORITES'				=>	'See your favorites',
    'TIP_QUICKORDER'				=>	'Direct, neutral delivery to your customer.',
    'TIP_RESET_AMOUNT'				=>	'Reset amount',
    /* Ende TOOLTIPS */
    'PLEASE_CHANGE_PASSWORD'		=>	'Please change your password as a security precaution.',

    'LOGGEDOUT_HEADLINE'			=>	'Automatic logout',
    'LOGGEDOUT_TEXT'				=>	'Logged out due to inactivity',
    'LOGGEDOUT_BUTTON'				=>	'Login again',
    'MESSAGE_NOT_ON_STOCK'			=>	'',
    'LOW_STOCK'						=>	'',
    'READY_FOR_SHIPPING'			=>	'',
    'WK_TEXT'						=>	'Shipping costs will be added after packing. We do our best to keep the shipping costs as low as possible and charge only the actual shipping costs incurred.',
    'DELTIMEUNIT'					=>	'Day',
    'DELIVERYTIME_DELIVERYTIME'     =>  'Production time',
    /* Quickorder.tpl */
    'BTN_CLOSE'                     =>  'Close',
    'BTN_OK'                        =>  'OK',
    'MODAL_ORDER_COMPLETE_TITLE'    =>  'Order complete',
    'MODAL_ORDER_COMPLETE_BODY'     =>  'Your order has been successfully completed.',
    'MODAL_ORDER_PREVIEW_TITLE'     =>  'Order preview',
    'MODAL_ORDER_PREVIEW_BODY'      =>  'The preview of your order.',
    'MODAL_ORDER_BTN_CHANGE'        =>  'Change order',
    'QUICKORDER_BTN_ORDERMANDANTORY'=>  'Order now',
    'QUICKORDER_ARTICLE_DESC'       =>  'Article description',
    'BTN_QUICKORDER'                =>  'Quick order',
    'SUBMIT_QUICK_ORDER'            =>  'Submit order',
    'QUICKORDER_PLACEHOLDER_SEARCHFIELD'    =>  'Article number or description',
    'SITE_TITLE'                    =>  'Article',
    'QUICKORDER_ARTNUM'             =>  'Article number',
    'QUICKORDER_ARTICLE'            =>  'Article',
    'QUICKORDER_SINGLE_PRICE'       =>  'Price',
    'QUICKORDER_COUNT'              =>  'Quantity',
    'QUICKORDER_AVAILABILITY'       =>  'Availability',
    'QUICKORDER_WHITE_LABEL'        =>  'Neutral shipping',
    'QUICKORDER_SEARCH_CONTACTS'    =>  'Search contacts',
    'QUICKORDER_PDF_TXT'            =>  'Send us your delivery slip(PDF) for this order',
    'QUICKORDER_VERFUEGBAR_DELETE'  => 'Verfügbar/Delete',

    'UVP'                           =>  'MSRP',
    'TAGE'                          =>  'days',
    'NOTHING_ADDED_TO_BASKET_TITLE'     =>  'Hinweis',
    'NOTHING_ADDED_TO_BASKET_TEXT'      =>  'Nothing has been added to the shopping cart!',
    'NOTHING_ADDED_TO_BASKET_CLOSEBTN'  =>  'Close',
    'AUTOSUGGEST_DIDYOUMEAN'            =>  'Did you mean perhaps: ',
    'PRINT_PDF'                                             => 'create PDF',
    'PRINT_EXCEL'                                           => 'create Excel',
    'ALLENEWS'                                              => 'All news',
    'ALLENEWSZEIGEN'                                        => 'Show all news',

    /* zur Übersetzung: */

    'NEUHEITEN'							=>	'New products',
    'ANGEBOTE'							=>	'Sale',
    'ARTNR'                             =>  'art.',

    /* Checkout Steps */
    'STEPS_BASKET'                      =>  'Shopping cart',
    'STEPS_SEND'                        =>  'Choose address',
    'STEPS_ORDER'                       =>  'Check and send',
    'ADRESSE'                           =>  'Address',
    'ADRESS_CHOOSE_HELP'                =>  'Choose a previous delivery address or a new delivery address',
    'LIEFERADRESSE'                     =>  'Delivery address',
    'RECHNUNGSADRESSE'                  =>  'Invoice address',
    'NEUTRAL'                           =>  'Neutral',
    'NEUTRALER_VERSAND_HELP'            =>  'neutral dispatch, without advertising material, shipped directly to your customer (with additional fee)',
    'DATEIUPLOAD'                       =>  'Upload your own delivery slip or invoice and it will be sent with the parcel',
    'MITTEILUNG'                        =>  'Message',

    'SIDEBAR_FAVORITES'                 =>  'My saved links',
    'CHECKOUT'                          =>  'Checkout',
    'ACTION'                            =>  'Sale',
    'VERPACKUNGSEINHEIT'                =>  'PU',


    'BNTOTAL'                           =>  'Total',

    'WK-OK'                             =>  'You added something to the shopping cart',
    'WK-ERR'                            =>  'An error has occurred',
    'WK-NO-ART'                         =>  'Please choose a quantity',

    'SEARCHTEXT'                        =>  'Search...',
    'ORDERED-ARTICLES'                  =>  'Show ordered items',
    /* Tool-Tip's */
    'TT-WK-OPEN'                           =>  'Open shopping cart',
    'TT-WK-CLOSE'                          =>  'Close shopping cart',
    'TT-TO-CHECKOUT'                       =>  'Checkout',
    'TT-VE-TO-BASKET'                      =>  'Round up to the next qty/box',
    'TT-VE-ADD'                            =>  'Add one box',
    'TT-EDIT'                              =>  'Edit',
    'TT-SAVE'                              =>  'Save',
    'TT-DELETE-ALL'                        =>  'Delete all',
    'TT-DELETE'                            =>  'Delete',
    'INCL_TAX_AND_PLUS_SHIPPING'           =>  'All prices without VAT, without shipping charges',
    'ContaktEmailUserBetr'                 =>  'Vielen Dank für Ihre Anfrage',
    'PREISNEU'                             =>  'effective 01.06.2019',
    'QUICKORDER_DESC'                      =>  'neutral dispatch, without advertising material, shipped directly to your customer (with additional fee)',
    'QUICKORDER_ERROR_NO_ARTICLE'          => 'You have not selected any articles.',
    'QUICKORDER_ERROR_ADDRESS_FIELD'       => 'Please fill in the complete address!',
    'QUICKORDER_ERROR'                     => 'Error when ordering.',
    'BTN_ADRESSVERWALTUNG' => 'Address Management',
    'CHANGE_RECHNUNG_ERROR' => 'Please contact us if you would like to make changes to your billing address.',
    'SUCHBEGRIFF'                           => 'insert search term ...',

    // Fehlende Übersetzungen
    'VERSANDSELBST'                                         => 'to invoice address',
    'FORMFILL'                                              => 'fill form',
    'ADRESSEAUSGEFUELLT'                                    => 'Form successfully completed',
    'BURGER'                                                => 'Categories',
    'MENUKLEIN'                                             => 'Menu',
    'STAND'                                                 => 'status',
    'PHONE'                                                 => 'Phone(1)',
    'PERSONAL_PHONE'                                        => 'Phone(2)',
    'VETooltip'                                             => 'packaging&nbsp;units',
    'neupreisinfo'                                          => 'New price from'



);

/*
[{ oxmultilang ident="GENERAL_YOUWANTTODELETE"}]
*/
