<?php
/**
 *    This file is part of OXID eShop Community Edition.
 *
 *    OXID eShop Community Edition is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    OXID eShop Community Edition is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with OXID eShop Community Edition.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @package   lang
 * @copyright (C) OXID eSales AG 2003-2016
 * @version OXID eShop CE
 * @translators: alexraimondo (1), Alpha-Sys (5), chatard (318), cowboy9571 (10), ET (10), ftn2018 (236), marco (9), patmat2809 (5), Phenix (203), piccobello (1), Rainbow40 (2), rava3000 (8), vikapera (376)
 */

$sLangName = 'Français';

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(
    'charset' => 'utf-8',

    //Zusätzliche Übersetzungen
    'AlleArtikel'                                           => 'Show all articles',
    'AKTIONBIS'                                             => 'Promo valable jusqu\'au ',
    'AUSLAUFARTIKEL'                                        => 'Cet article ne sera plus disponible',
    'LOADPRICELIST'                                         => 'Téléchargement des articles en cours',
    'EXCELPRICELIST'                                        => 'Liste de prix Excel',
    'PDFPRICELIST'                                          => 'Liste de prix PDF',
    'EXCELPRICELISTGET'                                     => 'All_Products',
    'PDFPRICELISTGET'                                       => 'All_Products',

    'NEUHEITEN'                                             =>  'Nouveautés',
    'ANGEBOTE'                                              =>  'Offres',
    'ARTNR'                                                 =>  'Réf. article',
    'SURCHARGE'                                             => 'Frais additionels pour toute commande inférieure à 75 € HT',
    'CancelAddressChange'                                   => 'Annuler',


    //Excel-Output
    'EXCEL_NAME'                                            =>  'Name',
    'EXCEL_VARNAME'                                         =>  'Variant',
    'EXCEL_ARTNUM'                                          =>  'ArtNr',
    'EXCEL_PRICE'                                           =>  'UP/€',
    'EXCEL_UVP'                                             =>  'UVP/€',
    'EXCEL_EAN'                                             =>  'EAN',
    'EXCEL_REASON'                                          =>  'Broken',
    'EXCEL_AMPEL'                                           =>  'Disponibilité',
    'EXCEL_GEWICHT'                                         =>  'Poids',
    'EXCEL_AKTION'                                          =>  'Promo',
    'EXCEL_RECALL'                                          =>  'Rappel',
    'EXCEL_RECALLGRUND'                                     =>  'Objet du rappel',
    'EXCEL_AKTIONENDE'                                      =>  'Fin de la promo',
    'EXCEL_AMPEL_0'                                         =>  'En stock',
    'EXCEL_AMPEL_1'                                         =>  'Stock limité',
    'EXCEL_AMPEL_2'                                         =>  'Rupture de stock',
    'EXCEL_AUSLAUFARTIKEL'                                  =>  'Fin de série',

    // Fehlende Übersetzungen
    'VERSANDSELBST'                                         => 'À l\'adresse de facturation',
    'FORMFILL'                                              => 'Compléter le formulaire',
    'ADRESSEAUSGEFUELLT'                                    => 'Formulaire dûment complété',
    'BURGER'                                                => 'Catégories',
    'MENUKLEIN'                                             => 'Menu',
    'STAND'                                                 => 'statut',

    'VETooltip'                                             => 'unités&nbsp;d’emballage',
    'neupreisinfo'                                          => 'Nouveau prix à partir de'





);
