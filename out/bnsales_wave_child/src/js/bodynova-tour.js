/**
 * Created by andre on 25.08.16.
 */

/*
Tour Beispiel:
 
 // Instance the tour
 var tour = new Tour({
 steps: [
 {
 element: "#my-element",
 title: "Title of my step",
 content: "Content of my step"
 },
 {
 element: "#my-other-element",
 title: "Title of my step",
 content: "Content of my step"
 }
 ]});
 
 // Initialize the tour
 tour.init();
 
 // Start the tour
 tour.start();
 
 /*
 onNext: function(tour){
 
 var userid = $('#userFavoriten').attr('userid');
 
 setbearbeiten(userid);
 }
 
*/

/*
$(document).ready(function(){
	
});
*/

function checkTour(counter) {
	var num = parseInt(counter);
	
	if(num > 1){
		alertify.success('Tourcounter num > 1: ' + counter);
		
		var tour = initTour();
		tour.init();
		// Start the tour
		tour.start(true);
	}
	
}

function initTour(){
	// Instance the tour
	var tour =  new Tour({
		steps:
			[{
				element: "#bearbeiten-btn",
				title:   "Bearbeiten aktivieren",
				content: "Klicken Sie bitte zuerst auf bearbeiten. Um die Standartfavoriten zu löschen und eigene anzulegen...",
				onNext: function () {
					setbearbeiten();
				}
			},
			{
				element: "#speichern-btn",
				title: "Speichern",
				content: "Speichert die bereits hinzugefügten Favoriten.",
				onShown: function(){
					$("#naviLeiste > li:nth-child(2) > a").trigger('click');
				}
			},
			{
				element: "#abbruch-btn",
				title: "Löschen",
				content: "Löscht alle hinzugefügten Favoriten",
				onNext: function(){
				}
			}]
	});
	
	return tour;
}

function showTour(){
	var tour = initTour();
	
	// Initialize the tour
	tour.init();
	//
	tour.goTo(1);
	
	// Start the tour
	tour.start(true);
	
}
