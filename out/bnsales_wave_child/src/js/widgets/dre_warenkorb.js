$(document).ready(function() {
    // einmal an alle elemente den ursprungswert speichern
    $('.amountinput').each(function() {
        $(this).attr('original-value', $(this).val());
    });
    // show Update Button wenn Mengenänderung im Checkout Basket
    $('.amountinput').keyup(function(e) {
        if($(this).val() != $(this).attr('original-value')) {
            $(this).siblings('.updateBtn').show();
        } else {
            $(this).siblings('.updateBtn').hide();
        }
    });
	// Wenn Mengenänderung mit Enter bestätigt wird, führe Click auf Update Basket aus
	$('.amountinput').keydown(function(e) {
		var key = e.which;
		switch (key) {
			case 13: // enter
				if($(this).val() != $(this).attr('original-value')) {
					$(this).siblings('.updateBtn').trigger("click");
				}
				break;
			default:
				break;
		}
	});
});


/**
 * Die Menge der Verpackungseinheit im Warenkorb hinzufügen und Basket aktualisieren
 * @param id
 * @param ve
 */
function addVe(id, ve){
	var old = parseInt($('#'+id).val());
	var ver = parseInt(ve);
	var neu = old;
	var erg = 0;
	if(ver == 0){
		return;
	}
	if(old < ver){
		while(neu < ver ){
			neu = ver;
		}
	} else {
		while(old > ver ){
			ver = ver + ver;
		}
		neu = ver;
	}
	erg = neu;
	$('#'+id).val(erg);
	if($('#'+id).val() != $('#'+id).attr('original-value')) {
		$('#'+id).siblings('.updateBtn').show().trigger("click");
	}
}
