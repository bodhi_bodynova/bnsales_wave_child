
/**
 *  Läd nur die Bilder, die auch im Browserfenster sichtbar sind.
 */

function imageloading(){
    "use strict";
    $('.lazy-img').each(function(){
        //console.log($(this));
        if($(this).visible() && !$(this).hasClass('loaded')){
            $(this).imageloader().addClass('loaded');
        }
    });
}

/**
 *  execution functions on document ready
 */
$(document).ready(function() {
    $(function () {
        $(document).on('shown.bs.tooltip', function (e) {
            setTimeout(function () {
                $(e.target).tooltip('hide');
            }, 2500);
        });
    });


    // Bind Scroll shrink/unshrink class binding on scrolling
    //imageloading();
    /*
	$(window).scroll(function() {
        setTimeout( function() {
            if ( (window.pageYOffset || document.scrollTop) >= 150 ) {
                $('body').addClass('shrinked' );
            } else {
                $('body').addClass('unshrinked' );
            }
        }, 250 );
		imageloading();
    });
	
    $(window).scroll();
    */

    $('#header .suche button').click(function(){
        if ( $('body').hasClass('shrinked')) {
            $('#header .suche #searchparam').toggle();
            $('#header .suche #results').hide();
        }
        return false;
    });
    // end

    bindSpinner();

    // check if we have to highlight an oxid

    if(getUrlParameter('open')) {
        //
        location.hash = '#' + fixOxId(getUrlParameter('open'));
        //
        //$(document).scrollTop($(document).scrollTop() - 175);
        //
        if(document.body.offsetWidth<1201){
            if(getUrlParameter('open')!=getUrlParameter('highlight')) {
                loadProductOptions(getUrlParameter('open'),getUrlParameter('langHL'), fixOxId(getUrlParameter('highlight')));
            } else {
                // $('#article_' + fixOxId(fixOxId(getUrlParameter('open')))).addClass('highlight');
                $('#articleA_' + fixOxId(fixOxId(getUrlParameter('open')))).addClass('highlight');
                loadProductOptions(getUrlParameter('open'),getUrlParameter('langHL'));
                loadProductOptionsBig(getUrlParameter('open'),getUrlParameter('langHL'));
                $([document.documentElement, document.body]).animate({
                    scrollTop: $('#article_' + fixOxId(fixOxId(getUrlParameter('open')))).offset().top -175
                }, 200);
            }
            // ende
        } else {
            if(getUrlParameter('open')!=getUrlParameter('highlight')) {
                loadProductOptionsBig(getUrlParameter('open'),getUrlParameter('langHL'), fixOxId(getUrlParameter('highlight')));
            } else {
                $('#articleA_' + fixOxId(fixOxId(getUrlParameter('open')))).addClass('highlight');
                loadProductOptions(getUrlParameter('open'),getUrlParameter('langHL'));
                loadProductOptionsBig(getUrlParameter('open'),getUrlParameter('langHL'));
                $([document.documentElement, document.body]).animate({
                    scrollTop: $('#articleA_' + fixOxId(fixOxId(getUrlParameter('open')))).offset().top -175
                }, 200);
            }
            // ende
        }
        // ende
    }

    // Tab Order

    makeTabOrder();
});

/**
 * Fixes Oxid to match css-id criteria
 *
 * @param oxid
 * @returns {XML|string|void}
 */
function fixOxId(oxid) {
    return oxid.replace(/\./g, '_');
}


/**
 * Bind spinner
 */

function bindSpinner() {
    // first unbind to avoid count clash
    $('.spinner .bootstrap-touchspin-up').unbind('click');
    $('.spinner .bootstrap-touchspin-down').unbind('click');
    // ende
}

/**
 * Die Funktion holt sich den Wert des Inputs, das sich hierarchisch neben der <span> der Plus-Minus-Button befindet. Mit Hilfe der übergebenen Oxid
 * und dem neu kalkuliertem Wert wird das hidden-Input-Feld, das für die Verarbeitung des Warenkorbes notwendig ist, befüllt.
 * @param oxid
 * @param plusminus
 * @param x = this -> Plus bzw. Minus-Button
 * @constructor
 *
 */
function PlusMinusAmount(oxid, plusminus,x){
    var Input = $(x).parent().siblings('input:first');

    var valueInput = parseInt(Input.val());
    var newValue;

    newValue = plusminus === '1' ? valueInput + 1 : valueInput -1;

    if(newValue < 0){
        newValue = 0;
    }

    Input.val(newValue);

    var hiddenInput = $('input[name="amount['+oxid+']"');
    hiddenInput.val(newValue);
    if(hiddenInput[1] !== undefined){
        hiddenInput[1].val(newValue);
    }
}

/**
 *
 *
 */
function handleAjaxLogout(response) {
    //
    if(response.success !== undefined && !response.success) {
        //
        //console.log(response);
        window.scrollTo(0,0);
        //
        $('body').append(
            '<div id="darkInizer"></div>' +
            '<div id="alertLoggedOut" role="alert" class="alert alert-danger alert-dismissible fade in">' +
            '   <h4>' + translations.loggedOutHeadline+ '</h4>' +
            '   <p>' + translations.loggedOutText + '</p>' +
            '   <p>' +
            '       <button class="btn btn-default" type="button" onclick="location.href=\'/index.php?cl=account&sourcecl=start\';">' + translations.loginButton + '</button>' +
            '    </p>' +
            '</div>'
        );
        //
        $('#alertLoggedOut').alert();
    }
    // ende
}

/**
 *
 * @param oxid
 * @param highlightoxid
 */
function loadProductOptions(oxid,lang, highlightoxid) {
    // remove and hide options on second click
    if($('#article_selected_' + fixOxId(oxid)).attr('attribute')=='smallOpen') {
        $('#article_selected_' + fixOxId(oxid)).attr('attribute','small');
        $('#article_selected_' + fixOxId(oxid) + ' td').html('');
        $('#article_selected_' + fixOxId(oxid)).hide();
        return;
    }
    // TODO: ladda durch spinner ersetzen
    //var ladda = Ladda.create( document.querySelector( '#article_' + fixOxId(oxid) + ' .btn-show-variants' ) );
    //
    //ladda.start();
    //
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=getproductselections&fnc=getList&oxid=' + oxid + '&lang='+lang,
        async : false,
        dataType: "html",
        error: function (e){
            //console.log(e);
            handleAjaxLogout(res);
        },
        success : function (res) {
            //console.log(res);
            //
            handleAjaxLogout(res);
            // show area for select and inject htmlcode
            $('#article_selected_' + fixOxId(oxid)).attr('attribute','smallOpen');

            $('#article_selected_' + fixOxId(oxid) + ' td').html(res);
            $('#article_selected_' + fixOxId(oxid)).html(res);
            //
            bindSpinner();
            //
            //ladda.stop();
            //
            $('#article_selected_' + fixOxId(oxid)).show();
            //
            if(highlightoxid !== undefined) {
                $('#article_' + fixOxId(fixOxId(highlightoxid))).addClass('highlight');
            }
            // reorganize tab order
            makeTabOrder();
            //imageloading();
            // ende
        }
    });
}


function loadProductOptionsBig(oxid,lang, highlightoxid) {
    // remove and hide options on second click
    if($('#article_selected_Big' + fixOxId(oxid)).attr('attribute') == 'bigOpen') {
        $('#article_selected_Big' + fixOxId(oxid)).attr('attribute','big');

        $('#article_selected_Big' + fixOxId(oxid) + ' td').html('');
        $('#article_selected_Big' + fixOxId(oxid)).hide();
        return;
    }
    //
    //var ladda = Ladda.create( document.querySelector( '#articleA_' + fixOxId(oxid) + ' .btn-show-variants' ) );
    //
    //ladda.start();
    //
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=getproductselections&fnc=getList&oxid=' + oxid +'&lang='+lang,
        async : false,
        dataType: "html",
        error: function (e){
            console.log('error');
            handleAjaxLogout(res);
        },
        success : function (res) {
            //console.log(res);
            //
            $('#article_selected_Big' + fixOxId(oxid)).attr('attribute','bigOpen');
            handleAjaxLogout(res);
            // show area for select and inject htmlcode
            $('#article_selected_Big' + fixOxId(oxid) + ' td').html(res);
            //$('#article_selected_Big' + fixOxId(oxid)).html(res);
            //
            bindSpinner();
            //
            //ladda.stop();
            //
            $('#article_selected_Big' + fixOxId(oxid)).show();
            //
            if(highlightoxid !== undefined) {
                $('#articleA_' + fixOxId(fixOxId(highlightoxid))).addClass('highlight');
            }
            // reorganize tab order
            makeTabOrder();
            //imageloading();
            // ende
        }
    });
}




/**
 *
 */
function makeTabOrder() {
    $('input.amount').each(function(key,val) {
        $(this).attr('tabindex', key+1);
    });
}


/**
 *
 */
/*
function loadArticleDetails(t) {
    console.log('Test');
    var e = $.ajax({
        type: "GET",
        url: "/index.php?cl=ajaxbasket&fnc=getArticleDetails&oxid=" + t,
        async: !1,
        dataType: "json",
        error: handleAjaxLogout
    }).responseText;//.responseJSON;
    console.log(e);
    $("body").append(e), $("#articledetails").modal(), $("#articledetails").on("hide.bs.modal", function (t) {
        $("#articledetails").remove()
    })
}
*/
function loadArticleDetails(oxid) {
    //
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=ajaxbasket&fnc=getArticleDetails&oxid=' + oxid,
        async: false,
        dataType: "html",
        error: function (){
            handleAjaxLogout(res)
        },
        success: function (res) {
            //
            //console.log('success');
            //console.log(res);

            $('body').append(res);
            //
            $('#articledetails').modal();
            //
            //
            $('#articledetails').on('hide.bs.modal', function (e) {
                $('#articledetails').remove();
            });
        }
    });

    // ende
}


function loadAdressbuch(oxid) {
    //
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=ajaxbasket&fnc=getAdressbuch&oxid=' + oxid,
        async: false,
        dataType: "json",
        error: handleAjaxLogout
    }).responseJSON;

    //console.log(res.content);

    //
    $('body').append(res.content);
    //
    $('#adressbuch').modal();

    var timeOutHolderAdressbookSearch = null;

    $('#searchadressbook').keyup(function() {

        //console.log('suche im Adressbuch');
        //
        if(timeOutHolderAdressbookSearch)
            window.clearTimeout(timeOutHolderAdressbookSearch);
        //
        timeOutHolderAdressbookSearch = window.setTimeout('searchInAdressbook()', 500);
        // ende
    });
    //
    $('#addressbookid, #oxcompany, #oxsal, #oxfname, #oxlname, #oxstreet, #oxstreetnr, #oxaddinfo, #oxzip, #oxcity, #oxfon, #oxcountryid').change(function() {
        $('#addressbookid').val('');
    });


    //
    $('#adressbuch').on('hide.bs.modal', function (e) {
        $('#adressbuch').remove();
    });
    // ende
}

function setAdress(oxid, company, sal, fname, lname, street, streetnr, addinfo, zip, city, countryid, fon ) {
    //
    $('#addressbookid').val(oxid);
    $('#oxcompany').val(company).attr('placeholder','');
    $('#oxsal').val(sal);
    $('#oxfname').val(fname).attr('placeholder','');
    $('#oxlname').val(lname).attr('placeholder','');
    $('#oxstreet').val(street).attr('placeholder','');
    $('#oxstreetnr').val(streetnr).attr('placeholder','');
    $('#oxaddinfo').val(addinfo).attr('placeholder','');
    $('#oxzip').val(zip).attr('placeholder','');
    $('#oxcity').val(city).attr('placeholder','');
    $('#oxfon').val(fon).attr('placeholder','');
    $('#oxcountryid').val(countryid).attr('placeholder','');
    //
    $('#adressBookAutocomplete').html('');
    $('#searchadressbook').val('');
    // ende
}



function searchInAdressbook() {
    //
    var res = $.ajax({
        type: "GET",
        dataType: "json",
        url: '/index.php?cl=adressbuch&fnc=getAdressAutocomplete&search=' + $('#searchadressbook').val(),
        async : false
    }).responseJSON;

    //console.log(res);

    //
    $('#adressBookAutocomplete').html('');
    //
    $.each(res, function() {
        //
        $('#adressBookAutocomplete').append(
            '<div onclick="setAdress(\'' + this.oxid + '\',\'' + this.company + '\',\'' + this.sal + '\',\'' + this.fname + '\',\'' + this.lname + '\',\'' + this.street + '\',\'' + this.streetnr + '\',\'' + this.addinfo + '\',\'' + this.zip + '\',\'' + this.city + '\',\'' + this.countryid + '\',\'' + this.fon + '\')$(\'#adressformmodal\').show()" class="addressBookItem">' +
            (this.company!='' ? this.company + '<br>': '') +
            this.fname + ' ' + this.lname + '<br>' +
            this.street + ' ' + this.streetnr + '<br>' +
            this.zip + ' ' + this.city + '<br>' +
            '</div>'
        );
        // ende
    });
    // ende
}

/**
 *
 * @param oxid
 */

function jumpToArticle(oxparentid , oxid,lang) {
    //
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=ajaxbasket&fnc=getCategoryURL&oxid=' + oxid,
        async: false,
        dataType: "json",
        error: handleAjaxLogout,
        success : function (r) {
            //console.log(r);
        }
    }).responseJSON;
    window.location.href = res.url + (res.url.indexOf('?') < 0 ? '?' : '') + '&open=' + oxparentid + '&highlight=' + oxid + '&langHL='+lang;
}

function getParentId(oxid){
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=ajaxbasket&fnc=getParentId&oxid=' + oxid,
        async: false,
        dataType: "json",
        error: handleAjaxLogout,
        success: function (r) {
            //console.log(r);
        }
    }).responseJSON;
    return res;
}

function newjJumpToArticle(oxid) {
    //
    var parentid = getParentId(oxid);
    if(parentid == null){
        console.log('null');
    }

    /*
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=ajaxbasket&fnc=getCategoryURL&oxid=' + oxid,
        async: false,
        dataType: "json",
        error: handleAjaxLogout,
        success: function (r) {
            //console.log(r);
        }
    }).responseJSON;
    window.location.href = res.url + (res.url.indexOf('?') < 0 ? '?' : '') + '&open=' + oxparentid + '&highlight=' + oxid;
    */
}

/**
 *
 * @param sParam
 * @returns {*}
 */
function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}


/**
 * TODO: Prüfe, welche Varianten geöffnet sind. Unterscheide zwischen großem/kleinen viewport.
 */
function openAllVariants() {

    $('#spinnerAllVariants').show();

    setTimeout(function(){
        var time1 = Date.now();
        var bool = true;
        if(document.body.offsetWidth<1201){
            $('.btn-show-variants').each(function () {
                //console.log(Date.now()-time1);
                if(Date.now()-time1>200 && bool){
                    bool = false;
                }
                $(this).click();
            });
        } else {
            $('.openVariantsBig').each(function () {
                //console.log(Date.now()-time1);
                if(Date.now()-time1>200 && bool){
                    bool = false;
                }
                $(this).click();
            });
        }
        setTimeout(function(){$('#spinnerAllVariants').hide();},1);
        if($('#buttonAllVariants')[0].children[1].className === 'fa fa-expand'){
            $('#buttonAllVariants')[0].children[1].className = 'fa fa-compress';
        } else {
            $('#buttonAllVariants')[0].children[1].className = 'fa fa-expand';
        }
        //$('#buttonAllVariants').show();
        //imageloading();
    },1);

}


/**
 *
 * @param response
 */
function handleAddTobasketResponse(response) {
    //
    //console.log(response);
    handleAjaxLogout(response);
    //
    //console.log(response);
    if(response.added.length)
    {
        // update the badge
        $('#miniBasket .badge').text(response.itemscount);
        $('#miniBasket .counter').show();
        //
        $('#miniBasket .badge').tooltip('show');
        window.setTimeout("$('#miniBasket .badge').tooltip('hide');", 2500);
        //
        if(response.oxid) {
            $('#article_selected_' + fixOxId(response.oxid)).hide();
        }
        //
        $('input[name^=\'amount\']').val('0');
    }
    else
    {
        //console.log(response);
        //$('#nothingaddedtobasketmodal').modal({});
        alertify.error('Error');
    }
    // ende

    //window.location.reload();

}

/**
 * Funktion zum runden von Zahlen
 */
function runde(zahl,stelle) {
    n_stelle = Math.pow(10,stelle);
    zahl = (Math.round(zahl * n_stelle) / n_stelle);
    return zahl;
}


/**
 * adds a bulkform to the basket
 * @param oxid
 */
function addBulkCart(oxid) {

    var gesamtSumme = 0;
    var neu = true;
    var data;
    if($('#article_selected_Big' + fixOxId(oxid)).attr('attribute') == 'bigOpen'){
        data = $('#article_selected_Big' + fixOxId(oxid) + ' form').serialize();
    } else {
        data = $('#article_selected_' + fixOxId(oxid) + ' form').serialize();
    }

    var res = $.ajax({
        async : false,
        type: "POST",
        url: '/index.php?cl=ajaxbasket&fnc=addBulk&oxid=' + oxid,
        data: data,
        dataType: "json",
        success: handleAddTobasketResponse,
        error: handleAjaxLogout
    }).responseJSON;
    var x = $('#WarenkorbRechts .navbar-form')[0].children;
    var keys = Object.keys(res.PostAmount);
    var values = Object.values(res.PostAmount);
    //console.log(keys); TODO: KEYS löschen, die schon vorhanden sind! Alle die übrig bleiben, werden in die Tabelle geschrieben.
    //console.log(values);
    var z = 0; // Index für Menge im Key/Value Array

    for(var i = 0;i<x.length;i++){
        oxid = oxid.replace('.','_');
        //console.log(x[i].id);
        if(keys.includes(x[i].id)){
            z= keys.indexOf(x[i].id);
            neu = false;
            oxid = oxid.replace('.','_');
            //var xid= x[i].id;

            var anzahlVorher = x[i].children[0].children[0].innerHTML;
            var preisVorher = x[i].children[2].children[0].innerHTML;
            var einzelPreis = runde(parseFloat((preisVorher.replace('.','')).replace(',','.')) / parseInt(anzahlVorher),2);
            var preisNachher = (runde((parseInt(anzahlVorher) + parseInt(values[z]))*einzelPreis,2)).toFixed(2);
            //console.log('Anzahlvorher: ' + anzahlVorher + ', preisVorher: ' + preisVorher + ', einzelPreis: ' + einzelPreis + ', preisNachher: ' + preisNachher);

            //console.log(x[i].children[0].children[0].innerHTML);
            //var amountOld = parseInt(x[i].children[0].children[0].innerHTML);
            x[i].children[0].children[0].innerHTML = parseInt(anzahlVorher) + parseInt(values[z]);
            x[i].children[2].children[0].innerHTML = (preisNachher.toString()).replace('.',',') + '&euro;';

            values[z] = null;
            keys[z] = null;
            //z++;

        }
        if(x[i].children[2]){
            //console.log(gesamtSumme);
            gesamtSumme = (parseFloat(gesamtSumme) + parseFloat((x[i].children[2].children[0].innerHTML.replace('.','')).replace(',','.'))).toFixed(2);
        }
    }

    for(var k = 0; k<keys.length;k++){
        if(keys[k] !== null && values[k] !== "0"){
            window.location.reload();
            return;
        }
    }

    if(gesamtSumme>=(75) && x[x.length-2].id === 'aufschlagBestellung'){
        x[x.length-2].remove();
    } else {
        gesamtSumme = (parseFloat(gesamtSumme) + 3.5).toFixed(2);
    }
    x[x.length-1].children[1].children[0].innerHTML = gesamtSumme.toString().replace('.',',');

    if(neu){
        //window.location.reload();
    }
    /*
    console.log(values);
    console.log(keys);
    for(var j = 0; j<values.length;j++){
        if(values[j] !== null && values[j] !== "0"){
            console.log(j);
            var string = '<div id="4c953eb8918066a3801eba05305e8e4b" class="row" onclick="jumpToArticle(\'4c970b65dd824a7b5dbf78449a12a919\',\'4c953eb8918066a3801eba05305e8e4b\');" style="cursor:pointer; background-color: rgba(0,0,0,.05); border-top:1px solid rgba(0,0,0,.05)">\n' +
                '                    <div class="col-xl-2 d-none d-xl-block" style="padding-left:0">\n' +
                '                        <span id="4c953eb8918066a3801eba05305e8e4bamount" class="badge badge-pill badge-secondary sidebarBadge" style="z-index: 10000">7</span>\n' +
                '                        <div class="pictureBoxSB-NoBig d-lg-none d-xl-block"><img src="https://bodynova.de/out/imagehandler.php?artnum=ymadb&amp;size=450_450_100" class="img-thumbnail"></div>\n' +
                '                    </div>\n' +
                '                    <div class="col-xl-8 col-lg-7 col-9"><p style="-ms-hyphens: auto;-webkit-hyphens: auto;hyphens: auto;">ASANA mat, dunkelblau</p></div>\n' +
                '                    <div class="col-xl-2 col-lg-5 col-3" style="padding-right:0"><strong class="pull-right" style="white-space: nowrap;">31,25 €</strong></div>\n' +
                '                </div>';
            $('#WarenkorbRechts .navbar-form').append(string);
        }
    }
*/
    //window.location.reload();
    //alertify.success(ok);

    //if(res != undefined ){
    //window.location.reload();
    //}
}

function WarenkorbHide(x){
    if($('#WarenkorbRechts').attr('show') === 'true'){
        $('#WarenkorbRechts').hide();
        $('#WarenkorbRechts').attr('show','false');
        x.innerHTML = '<span class="fa fa-chevron-down"></span>';

    } else {
        $('#WarenkorbRechts').show();
        $('#WarenkorbRechts').attr('show','true');
        x.innerHTML = '<span class="fa fa-chevron-up"></span>';

    }
}

function addVeToInput(oxid , amount){
    var wert = parseInt(amount);
    var input = $('#article_' + fixOxId(oxid) + ' input[name=\'amount[' + oxid + ']\']');
    var gesetzt = parseInt(input.first().val());
    var neu = input.first().val(gesetzt + wert);
    //console.log('input: ' + input + 'amount ' + amount + 'neu:' + neu);
    //console.log(neu);

}
/**
 * Adds a Single Item to the Basket
 * @param oxid
 */
function addToBasket(oxid, ok, err, noart){
    //
    var data = {};
    data['amount'] = {};

    data['amount'][oxid] = $('#articleA_' + fixOxId(oxid) + ' input[name=\'amount[' + oxid + ']\']').val();

    //
    if(data['amount'][oxid]==0){
        alertify.error(noart);
        return;
    }
    var res = $.ajax({
        async : false,
        type: "POST",
        url: '/index.php?cl=ajaxbasket&fnc=addBulk&oxid=' + oxid,
        data: data,
        dataType: "json",
        success: handleAddTobasketResponse,
        error: handleAjaxLogout
    });
    var neu = true;
    //console.log(res.responseJSON.added);
    //console.log($('#WarenkorbRechts .navbar-form')[0].children);
    var x = $('#WarenkorbRechts .navbar-form')[0].children;
    var gesamtSumme = 0;

    for(var i = 0;i<x.length;i++){
        //console.log(x[i].id);
        if(x[i].id === res.responseJSON.added[0]){
            neu = false;
            oxid = oxid.replace('.','_');
            //console.log($('#inputBig'+oxid+'')[0].value);
            $('#inputBig'+oxid+'')[0].value = 0;
            var anzahlVorher = x[i].children[0].children[0].innerHTML;
            var preisVorher = x[i].children[2].children[0].innerHTML;
            var einzelPreis = runde(parseFloat((preisVorher.replace('.','')).replace(',','.')) / parseFloat(anzahlVorher),2);
            var preisNachher = (runde(res.responseJSON.added[1]*einzelPreis,2)).toFixed(2);
            //console.log('Anzahlvorher:' + anzahlVorher + ', preisvorher:' + preisVorher + ', einzelpreis: ' + einzelPreis + ', preisNachher:' + preisNachher);
            x[i].children[0].children[0].innerHTML = res.responseJSON.added[1];
            x[i].children[2].children[0].innerHTML = (preisNachher.toString()).replace('.',',') + '&euro;';

        }
        if(x[i].children[2]){
            //console.log(gesamtSumme);
            gesamtSumme = (parseFloat(gesamtSumme) + parseFloat((x[i].children[2].children[0].innerHTML.replace('.','')).replace(',','.'))).toFixed(2);
        }
    }
    //console.log(gesamtSumme);
    if(gesamtSumme>=(75) && x[x.length-2].id === 'aufschlagBestellung'){
        x[x.length-2].remove();
    } else if(gesamtSumme<75) {
        gesamtSumme = (parseFloat(gesamtSumme) + 3.5).toFixed(2);
    }

    x[x.length-1].children[1].children[0].innerHTML = gesamtSumme.toString().replace('.',',');
    if(neu){
        window.location.reload();
    }
    //window.location.reload();
    alertify.success(ok);
    // ende
}

/**
 *
 */
function allItemsIntoTheBasket(ok,error) {
    //
    var data = {}, count = 0;
    data['amount'] = {};
    //

    $('input[name^=\'amount\']').each(function() {
        //
        var name= $(this).attr('name');
        //updateAmount(this,name);
        //$('input[name="'+name+'"]').value=$('input[name"'+name+'"]')
        //console.log($('input[name="'+name+'"]'));
        if($(this).val()<1)
            return;
        //
        var strOXID = $(this).attr('name').substr(7);


        //TODO: If 1. Stelle M kürze 5
        strOXID = strOXID.substr(0, strOXID.length-1);
        if(strOXID[0] === 'm'){
            strOXID = strOXID.substr(5);
        }
        //
        count+= $(this).val();
        data['amount'][strOXID] = $(this).val();
        // ende
    });
    //

    if(count) {
        alertify.success(ok);
        $.ajax({
            async : false,
            type: "POST",
            url: '/index.php?cl=ajaxbasket&fnc=addBulk',
            data: data,
            dataType: "json",
            success: handleAddTobasketResponse,
            error: handleAjaxLogout
        });
        window.location.reload();
    } else {
        alertify.error(error);
    }
    // ende
}


/**
 *
 */
function resetBasketSelection() {
    $('input[name^=\'amount\']').val('0');
    $('.amount').val('0');
}


/**
 * Handle the response from toggleFavoritEntry
 */
function handleFavoriteToggle(response) {
    //
    handleAjaxLogout(response);
    //
    response.oxid = response.oxid.replace(".","_");
    //console.log(response);
    if(response.added) {
        $('#article_' + response.oxid + ' .btn-favorite').addClass('btn-outline-primary');
    } else {
        $('#article_' + response.oxid + ' .btn-favorite').removeClass('btn-outline-primary');
    }
    // ende
}

/**
 * Add or Remove a Item from the favorite list
 * @param oxid
 */
function toggleFavoritEntry(oxid) {
    var fixedOxid = fixOxId(oxid);

    $.ajax({
        async : false,
        type: "POST",
        url: '/index.php?cl=myfavorites&fnc=toggleFavoritEntry&oxid=' + oxid,
        dataType: "json",
        success: handleFavoriteToggle,
        error: handleAjaxLogout
    });

    if($('#articleA_'+fixedOxid + ' .ampelcolumn')[1] !== undefined){
        $('#articleA_'+fixedOxid + ' .ampelcolumn')[1].firstElementChild.click();
    } else {
        //console.log($('#articleA_'+fixedOxid+ ' .btn-favorite'));
        if($('#articleA_'+fixedOxid+ ' .btn-favorite').hasClass('btn-primary')){
            $('#articleA_'+fixedOxid+ ' .btn-favorite').removeClass('btn-primary');
        } else {
            $('#articleA_'+fixedOxid+ ' .btn-favorite').addClass('btn-primary');
        }
    }
}


/**
 * Add slahes arround " and '
 * @param str
 * @returns {string}
 */
function addslashes(str) {
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0').replace(/"/g, '\'');
}

/**
 * Minibasket Popover
 */
$(document).ready(function() {



    //

    //$('.bs-popover-top').addClass('in');
    if(document.body.offsetWidth>1200) {

        $('#minibasketIcon').popover({
            trigger: 'hover',
            placement: 'bottom',
            content: function () {
                //
                var response = $.ajax({
                    async: false,
                    type: "POST",
                    url: '/index.php?cl=ajaxbasket&fnc=getCurrentBasket',
                    dataType: "json"
                }).responseJSON;
                //
                var strRet = '<table id="tableMiniBasket" class="table table-striped" style="min-width:320px">';
                $.each(response.articles, function () {
                    //console.log(this);
                    strRet += '<tr>';
                    strRet += '<td class="pictureBox"><img style="width:45px !important;" src="' + this.pic1 + '"<td>';
                    strRet += '   <td class="amount">' + this.amount + 'x</td>';
                    strRet += '   <td class="title">' + this.title + '</td>';
                    strRet += '   <td style="text-align: right">' + this.price + '</td>';
                    strRet += '</tr>';
                });
                // Rabatte die angewendet werden:
                //console.log(response.basketcheckouturl);
                if (response.discounts != null) {
                    if (Object.keys(response.discounts).length > 0) {
                        strRet += '<tr><td></td><td></td><td></td></tr>';
                        $.each(response.discounts, function () {
                            strRet += '<tr >';
                            strRet += '   <td colspan="3"><strong>' + (parseFloat(this.dDiscount) < parseFloat(0) ? response.AufschlagText : response.DiscountText) + ' ' + this.sDiscount + '</strong></td><td style="text-align:right">' + (parseFloat(this.dDiscount) < parseFloat(0) ? parseFloat(this.dDiscount * -parseFloat(1)).toFixed(2).toString().replace(".", ",") : parseFloat(this.dDiscount).toFixed(2)).toString().replace(".", ",") + ' €</td>';
                            strRet += '</tr>';
                        });
                    }
                }
                strRet += '<tr>';
                if (response.GesamtPreis == null) {
                    response.GesamtPreis = 0;
                }
                strRet += '<td colspan="3"><strong>' + response.summentext + ': </strong></td><td style="text-align:right">' + parseFloat(response.GesamtPreis).toFixed(2).toString().replace(".", ",") + ' €</td>';

                strRet += '</table>';


                strRet += '<button style="margin-bottom:15px" type="button" class="pull-right btn btn-outline-success" onclick="(location=\'' + response.basketcheckouturl + '\')">' + response.checkouttext + '</a>';

                // DEBUG:
                //console.log(response);
                //strRet+='<div class="minbsum">Gesamtsumme: ' + response.pricesum + '</div>';
                //
                return strRet;
                // ende
            },
            html: true,
            delay: {"hide": 5000}
        }).addClass("in");
        //
    }

    $(document).click(function(e) {
        if (e.target.id == undefined || e.target.id != 'minibasketIcon') {
            $('#minibasketIcon').popover('hide');
        }
    });
    // ende
});

/**
 * Back to top Button:
 */
$(document).ready(function() {

    var offset = 250;
    var duration = 300;

    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(duration);
        } else {
            $('.back-to-top').fadeOut(duration);
        }
    });
    $('.back-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, duration);
        return false;
    });
});

/**
 * Basket Content zweite Tabelle positionierung
 */
$(document).ready(function() {
    $('.sumeditCol').width($('.editCol').width());
    $('.sumthumbCol').width($('.thumbCol').width());
    $('.sumtitle').width($('.title').width());
    $('.sumcountCol').width($('.countCol').width());
    $('.sumweightCol').width($('.weightCol').width());
    $('.sumpriceCol').width($('.priceCol').width());
    $('.sumuvp').width($('.uvp').width());
    $('.sumtotalCol').width($('.totalCol').width());
});

/**
 * Newsbox Modal
 */
$('#newsboxmodal').on('shown.bs.modal', function () {
    $('#meinEingabefeld').focus();
});

/**
 *  WK Ansicht speichern
 * @param userid
 * @param wkansicht
 */
function setwkansicht(userid, wkansicht){
    var test = wkansicht;
    //var test = $("input:radio[name=wkAnsicht]:checked").val();
    //console.log(userid , test);
    //$.get("/index.php?cl=oxcmp_user&fnc=setWkAnsicht&userid="+userid+"&wkAnsicht="+test);
    var res = $.ajax({
        type: "GET",
        url: '/index.php?cl=ajaxbasket&fnc=setWkAnsicht&userid=' + userid + '&wkAnsicht=' + test,
        async: false,
        dataType: "json",
        error: handleAjaxLogout
    }).responseJSON;
    //console.log(res.content);
    location.reload();
}
/**
 * WK sortierbar
 */
$( function() {
    $( "#sortable" ).sortable({
        cursor: "move",
        //forceHelperSize: true,
        //forcePlaceholderSize: true,
        placeholder: "ui-state-highlight",
        deactivate: function( event, ui ) {
            var i = 0;
            //var array = $.makeArray();
            //array['alteNummer'] = ui.item.attr( "nummer" );

            var nummer = [];
            var neuenummer = [];
            var oxid = [];
            $(ui.item).parent().children().each(function(index){
                $(this).attr({"neuenummer" : index + 1});
            });

            var data = [];
            $(ui.item).parent().children().each(function(index){

                /* data = Array[ int array[ int array[ 0 = key , 1 = value  ] ] ];
                 * data[0][0][0] = 'oxid';
                 */

                data.push([ ['oxid' , $(this).attr("oxid")] , ['nummer' , $(this).attr("nummer")] , ['neueNummer' , $(this).attr("neuenummer")] ]);
            });

            //console.log(data);

            /*
            var retNummer = nummer;
            var retNeueNummer = neuenummer;
            var retOxid = oxid;
            var data = [retOxid[ retNummer [ retNeueNummer ]]];
            */

            //console.log("data" , data);

            var res = $.ajax({
                type: "post",
                url: '/index.php?cl=ajaxbasket&fnc=basketSortierung',
                data: {"element" : data},
                async: false,
                dataType: "json",
                success: function() {

                },
                error: function(xhr, desc, err) {
                    /*console.log(xhr);
                    console.log("Details: " + desc + "\nError:" + err);
                    */
                }
            }); // end ajax call

            //console.log(res);
            //console.dir(res);
            //console.log("ui:" , ui , "nummer:" ,  ui.item.attr( "nummer" ) , "neuenummer", ui.item.attr("neuenummer"));
        }
    });
    $( "#sortable" ).disableSelection();
} );



function test (id) {
    //var erg = null;
    var data = id;
    var response = $.ajax({
        type: "post",
        url: '/index.php?cl=ajaxbasket&fnc=getUserFavorites',
        data: {"userid" : data},
        async: false,
        dataType: "json",
        success: function() {
        },
        error: function(xhr, desc, err) {
            /*
        	console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
            */
        }
    }).responseJSON; // end ajax call
    //console.log("erg: " , response);
    $("#test").html(response);
}

/**
 *
 * @param nummer
 */
function sortchange(nummer){
    console.log(nummer);
}

/**
 * Update Input für Warenkorb
 */
function updateAmount(x,oxid){

    var newValue = parseInt(x.value);
    $("input[name='"+oxid+"']")[0].value = newValue;
    //console.log($("input[name='"+oxid+"']")[0]);
    if($("input[name='"+oxid+"']")[1] !== undefined){
        $("input[name='"+oxid+"']")[1].value = newValue;
    }
}

/**
 *
 */
$( function() {
    $("#Leftsortable").sortable({
        cursor: "move",
        forceHelperSize: true,
        forcePlaceholderSize: true,
        placeholder: "ui-state-highlight"
    });
    $("#Leftsortable").disableSelection();
});

/**
 *
 */
function setbearbeiten(){
    $(function () {
        if(showBearbeiten()){

            // Top Navi Leiste
            $('#naviLeiste').find('span').filter( ".sort-visible" ).css('visibility','visible');

            $( "#naviLeiste, #userFavoriten" ).sortable({
                connectWith: ".connectedSortable",
                placeholder: "ui-state-highlight",
                forceHelperSize: true,
                forcePlaceholderSize: true,
                revert: true,
                items: "> li",
                start: function (event, ui) {
                    $('#userFavoriten').css('height','30px');
                }
            }).disableSelection();

            // Yoga
            $('#navsort1').find('span').filter( ".sort-visible" ).css('visibility','visible');
            $( "#navsort1, #userFavoriten" ).sortable({
                connectWith: ".connectedSortable",
                placeholder: "ui-state-highlight",
                forceHelperSize: true,
                forcePlaceholderSize: true,
                helper:"clone",
                revert: true,
                items: "> li",
                start: function (event, ui) {
                    $('#userFavoriten').css('height','30px');
                }
            }).disableSelection();

            // Physio Massage
            $('#navsort2').find('span').filter( ".sort-visible" ).css('visibility','visible');
            $( "#navsort2, #userFavoriten" ).sortable({
                connectWith: ".connectedSortable",
                placeholder: "ui-state-highlight",
                forceHelperSize: true,
                forcePlaceholderSize: true,
                revert: true,
                items: "> li",
                start: function (event, ui) {
                    $('#userFavoriten').css('height','30px');
                }
            }).disableSelection();

            // Meditation
            $('#navsort3').find('span').filter( ".sort-visible" ).css('visibility','visible');
            $( "#navsort3, #userFavoriten" ).sortable({
                connectWith: ".connectedSortable",
                placeholder: "ui-state-highlight",
                forceHelperSize: true,
                forcePlaceholderSize: true,
                revert: true,
                items: "> li",
                start: function (event, ui) {
                    $('#userFavoriten').css('height','30px');
                }
            }).disableSelection();

            // Shiatsu
            $('#navsort4').find('span').filter( ".sort-visible" ).css('visibility','visible');
            $( "#navsort4, #userFavoriten" ).sortable({
                connectWith: ".connectedSortable",
                placeholder: "ui-state-highlight",
                forceHelperSize: true,
                forcePlaceholderSize: true,
                revert: true,
                items: "> li",
                start: function (event, ui) {
                    $('#userFavoriten').css('height','30px');
                }
            }).disableSelection();

            // Pilates & Fitness
            $('#navsort5').find('span').filter( ".sort-visible" ).css('visibility','visible');
            $( "#navsort5, #userFavoriten" ).sortable({
                connectWith: ".connectedSortable",
                placeholder: "ui-state-highlight",
                forceHelperSize: true,
                forcePlaceholderSize: true,
                revert: true,
                items: "> li",
                start: function (event, ui) {
                    $('#userFavoriten').css('height','30px');
                }
            }).disableSelection();

            // Angebote
            $('#navsort6').find('span').filter( ".sort-visible" ).css('visibility','visible');
            $( "#navsort6, #userFavoriten" ).sortable({
                connectWith: "#userFavoriten",
                placeholder: "ui-state-highlight",
                forceHelperSize: true,
                forcePlaceholderSize: true,
                helper: 'clone',
                revert: true,
                items: "> li",
                cursor: "move",
                start: function (event, ui) {
                    $('#userFavoriten').css('height','30px');
                }
            }).disableSelection();
        }
    });
}

/**
 *
 */
function speichernUserFavoriten(){
    var sort = 0;
    $('#userFavoriten').find('li').closest('#userFavoriten > li').each(function(sort){
        $(this).attr('sort', sort);
        var name = $(this).text();
        $(this).attr('text', name.trim() );
        sort++;
        //console.log('li:' , this);
        //$(this).remove('.sort-visible');
        //$(this).delay( 800 );
    });
    $('#userFavoriten').find( "span" ).remove('.sort-visible');
    var anzahl = $('#userFavoriten').find('li').closest('#userFavoriten > li').size();
    var userid = $('#userFavoriten').attr('userid');
    var data = [];
    var i = 0;
    $('#userFavoriten').find('li').closest('#userFavoriten > li').each(function(i){
        //console.log('this: i', i ,  this);
        data.push(
            [ i ,
                [ ['oxid']   , $(this).attr('oxid') ] ,
                [ ['text']   , $(this).attr('text') ] ,
                [ ['type']   , $(this).attr('type') ] ,
                [ ['sort']   , $(this).attr('sort') ] ,
                [ ['anzahl'] , anzahl ] ,
                [ ['userid'] , userid ]
            ]
        );
        i++;
    });
    //console.log('data: ', data);
    var response = $.ajax({
        type: "post",
        url: '/index.php?cl=ajaxbasket&fnc=addUserFavorites',
        data: {"item" : data},
        async: false,
        dataType: "json",
        success: function() {
        },
        error: function(xhr, desc, err) {
            /*
            console.log(xhr);

            console.log("Details: " + desc + "\nError:" + err);
            */
        }
    }).responseJSON; // end ajax call
    //console.log("speichern: " , response);
    $(function () {
        endBearbeiten();
    });
    window.setTimeout(location.reload(), 1000);
    //window.location.reload();
}

/* ToDo */
function abbruchSpeichernUserFavoriten(oxid){
    $(function () {
        endBearbeiten();
        deleteAllFavorites(oxid);
    });
}

/**
 * User Favoriten bearbeiten.
 * @returns {boolean}
 */
function showBearbeiten(){
    var userid = $('#userFavoriten').attr('userid');
    var anzahl = $('#userFavoriten').find('li').size();
    if(!anzahl > 0){
        $('#AnleitungFavoriten').removeClass('hidden');
    }
    if(anzahl > 10){
        $('.hidden-trash').css('visibility','visible');

        $('#bearbeitbar').after().html('<div class="alert alert-danger" id="alarm" role="alert">Sie haben bereits zu viele Favoriten<br>' +
            '<div class="btn-group">'+
            '<button type="button" class="btn btn-danger" href="#" onclick="deleteAllFavorites(\''+userid+'\')">Alle löschen</button>' +
            '<button type="button" class="btn btn-success" href="#" onclick="endBearbeiten()">OK</button> '+
            '</div>' +
            '</div>');
        return false;
    } else {
        $('#bearbeiten-btn').addClass('active').attr( "disabled", "disabled" );
        $('#speichern-btn').addClass('btn-warning').removeAttr("disabled");
        $('#abbruch-btn').addClass('btn-danger').removeAttr("disabled");
        $('#standardfavoriten').empty(); //.addClass("ui-state-highlight").css('height','20px');
        $('.hidden-trash').css('visibility','visible');
        return true;
    }
}

/**
 * User Favoriten Bearbeiten beenden.
 */
function endBearbeiten(){
    $('#bearbeiten-btn').removeClass('active').removeAttr("disabled");
    $('#speichern-btn').removeClass('btn-warning').attr( "disabled", "disabled" );
    $('#abbruch-btn').removeClass('btn-danger').attr( "disabled", "disabled" );
    //$('#standardfavoriten').removeClass("ui-state-highlight").css('height','0');
    $('.hidden-trash').css('visibility','hidden');
    $('#alarm').remove();
}

/**
 * Ausgewählten User Favoriten löschen
 * @param oxid
 */
function deleteFavorite(oxid){
    var data = oxid;
    var response = $.ajax({
        type: "post",
        url: '/index.php?cl=ajaxbasket&fnc=delUserFavorite',
        data: {"item" : data},
        async: false,
        dataType: "json",
        success: function() {
        },
        error: function(xhr, desc, err) {
            /*
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
            */
        }
    }).responseJSON; // end ajax call
    console.log("deletet: " , response);
    window.location.reload();
}

function deleteOxfavorite(oxid,user){


    /*
        var data = oxid;
        var userid = user;*/
    var response = $.ajax({
        type: "post",
        url: '/index.php?cl=ajaxbasket&fnc=deleteOxfavorite',
        data: {
            "item": oxid,
            "user" : user
        },
        async: false,
        dataType: "json",
        success: function (response) {
            //console.log(response);
        },
        error: function (xhr, desc, err) {
            /*
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
            */
        }
    }).responseJSON; // end ajax call
    //console.log("deletet: ", response);
    window.location.reload();
}

/**
 * Alle User Favoriten löschen
 * @param oxid
 */
function deleteAllFavorites(oxid){
    var data = oxid;
    var response = $.ajax({
        type: "post",
        url: '/index.php?cl=ajaxbasket&fnc=delAllFavorites',
        data: {"userid" : data},
        async: false,
        dataType: "json",
        success: function() {
        },
        error: function(xhr, desc, err) {
            /*
            console.log(xhr);
            console.log("Details: " + desc + "\nError:" + err);
            */
        }
    }).responseJSON; // end ajax call
    //console.log("deletet: " , response);
    window.location.reload();
}


/*
 Checkout Steps:
*/
$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });
    $(".next-step").click(function (e) {
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {
        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);
    });

    if(document.body.offsetWidth<1000){
        $(function() {
            $('.bntooltip').tooltip({
                trigger: "hover",
                delay: {show: 0 , hide: 2500}
            });
        });
    } else {
        $(function() {
            $('.bntooltip').tooltip({
                trigger: "hover",
                delay: {show: 0 , hide: 0}
            });
        });
    }

});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}

/* end Checkout Steps  */


function changeShipAdress(e){
    //console.log('e: ', e.target);
    //console.log( $(e.target).prop("checked") );
    if($(e.target).prop("checked")){
        /*
        console.log('wahr');
        console.log($(document).scrollTop());
        */
        $('#shippingAddress').css('display','none');
    } else {
        $('#shippingAddress').css('display','block');

        //console.log($(document).scrollTop());
        $(document).scrollTop(100);
        /*
        console.log($(document).scrollTop());
        console.log('unwahr');
        */
    }
    //$('#shippingAddress').toggle( e.is(':not(:checked)'));
}

function myscrollTo(e){
    var set = parseInt(e);
    $('html, body').animate({scrollTop: set}, 300);
}

$('#scroll').click(function(e){
    //console.log('element: ', e);
    //$.scrollTo('+=100px', 800, { axis:'y' });
    //console.log('ist da:' , $(window).scrollTop());
    $('html, body').animate({scrollTop: ($(window).scrollTop() + 100)}, 300);
    //console.log('ist da:' , $(window).scrollTop());
    /*console.log('ist da:' , $(window).scroll());
    $(window).scroll(50);
    console.log('ist da:' , $(window).scroll());*/
});

/**
 * Testalert
 */
function testalert(){
    // alert dialog
    alertify.alert("Message");
    // confirm dialog
    alertify.confirm("confirm dialog", function (e) {
        if (e) {
            // user clicked "ok"
        } else {
            // user clicked "cancel"
        }
    });
    // prompt dialog
    alertify.prompt("prompt dialog", function (e, str) {
        // str is the input text
        if (e) {
            // user clicked "ok"
        } else {
            // user clicked "cancel"
        }
    }, "Default Value");
    // standard notification
    // setting the wait property to 0 will
    // keep the log message until it's clicked
    // alertify.log("Notification", type, wait);
    alertify.log("Notification", "log", 0 ); //wait);
    alertify.success("Success notification");
    alertify.error("Error notification");
}


/**
 * Eigene Suche Autosuggest
 */
function doSearch() {
    var q = jQuery.trim($('#searchparam').val());
    //
    //$('#header .searchBox').addClass('resultspresent');
    $('.navbar-form .searchBox').addClass('resultspresent');
    //
    /*
    console.log(q);
    console.log(startSuggest);
    */
    if (q.length >= startSuggest) {
        // neu: $("#results").hide();
        $.ajax({
            url: '/modules/autosuggest/controllers/autosuggest.php?q=' + q,
            cache: false,
            async: false,
            dataType: "html",
            type: "get",
            success: function(data) {
                if(data) {
                    $("#results").html(" ");
                    $("#results").html(data);
                    $("#results").show();
                }
            }
        });
    } else {
        $('#results').hide();
    }
}
/**
 * Eigene Suche SolrSuggest
 */
function doSolrSearch(){
    var q = jQuery.trim($('#solrsearchparam').val());
    //
    //$('#header .searchBox').addClass('resultspresent');
    $('.navbar-form .searchBox').addClass('resultspresent');
    //
    if (q.length >= startSuggest) {
        // neu: $("#results").hide();
        $.ajax({
            url: '/modules/solrsuche/controllers/solrsuggest.php?q=' + q,
            cache: false,
            async: false,
            dataType: "html",
            type: "get",
            success: function(data) {
                if(data) {
                    $("#solrresults").html(" ");
                    $("#solrresults").html(data);
                    $("#solrresults").show();
                }
            }
        });
    } else {
        $('#solrresults').hide();
    }
}

/**
 * Eigene Suche Autosuggest mit delay
 */
function autoSuggest() {

    //$("#foo").bindWithDelay("keyup", function(e) { }, 100);

    /*
    //
    if(objQuickSearchTimoutHandler)
        window.clearTimeout(objQuickSearchTimoutHandler);
     //load with a delay to avaoid duolciate querys on keyup
    objQuickSearchTimoutHandler = window.setTimeout(doSearch, 100);
    // end
    */
}



function solrSuggest(){
    //
    if(objSolrQuickSearchTimoutHandler)
        window.clearTimeout(objSolrQuickSearchTimoutHandler);
    // load with a delay to avaoid duolciate querys on keyup
    objSolrQuickSearchTimoutHandler = window.setTimeout(doSolrSearch, 100);
    // end
}


function bildgross() {
    //$('.mag').toggle('width','100%');
    if($('.detailbild').css('width') == '200px'){
        $('.detailbild').css('width','100%');
        $('#buttonBildGross')[0].innerHTML = '-';
    } else {
        $('.detailbild').css('width','200px');
        $('#buttonBildGross')[0].innerHTML = '+';
    }

}

function testSpin(x){
    var a = $("input[name='amount"+x);
    console.log(a[1].value);
    var b = parseInt(a[1].value);
    if(a[1].value === ""){
        a[1].value = 1 ;
    } else {
        a[1].value = b + 1;
    }
}

/**
 * Funktion zur Verwaltung der benötigten Felder bei der Adresseingabe. Es werden entweder
 * Name UND Vorname oder die Firma benötigt.
 * @param x : inputField
 */
/*
function reqShip(x){

    if(x.id === 'firmaShip' ){
        if(x.value !== ''){
            $('#vornameShip').removeAttr('required');
            $('#nachnameShip').removeAttr('required');
        } else {
            $('#vornameShip').attr('required','');
            $('#nachnameShip').attr('required','');
            //$('#firmaShip').attr('required','');
        }
    } else if(x.id === 'vornameShip'){
        if(x.value !== '' || $('#nachnameShip')[0].value !== ''){
            $('#firmaShip').removeAttr('required');
        } else {
            $('#firmaShip').attr('required','');
        }
    } else if(x.id === 'nachnameShip') {

        if (x.value !== '' || $('#vornameShip')[0].value !== '') {
            $('#firmaShip').removeAttr('required');
        } else {
            $('#firmaShip').attr('required', '');
        }
    }
}
*/
function loadPDF(x){
    /*var mod = '<div style="position:absolute" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\n' +
        '  <div class="modal-dialog">\n' +
        '    <div class="modal-content">\n' +
        '      <div class="modal-header">\n' +
        '      </div>\n' +
        '      <div class="modal-body">\n' +
        '        <div class=\'modal-body1\'>\n' +
        '          <h3>Bitte haben Sie einen Moment Geduld...</h3>\n' +
        '        </div>\n' +
        '      </div>\n' +
        '      <div class="modal-footer">\n' +
        '      </div>\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>';
    $('#middelContent').append(mod);
    console.log($('#myModal'));

    $('#myModal').modal({backdrop: 'static', keyboard: false}); */
    window.location = x;
}

/* Funktion zur Einblendung der LeftSideBar*/
function klicktest(){
    if($('#sidebarLeft').attr('on') === "false"){
        $('#sidebarLeft').attr('style','display:block !important');
        $('#sidebarLeft').attr('on',true);
        $('#buttonSidebarLeft').html('<i class="fas fa-angle-up"></i>');

    } else{
        $('#sidebarLeft').attr('style','display:none ');
        $('#sidebarLeft').attr('on',false);
        $('#buttonSidebarLeft').html('<i class="fas fa-angle-down"></i>');

    }


}