var objSearchTimeout = null;

$('#OXARTNUM').keyup(function() {

    if(objSearchTimeout)
        window.clearInterval(objSearchTimeout);
    objSearchTimeout = window.setTimeout(lookupForItems, 500);
})


function lookupForItems() {
    //
	var loged = $('#OXARTNUM').val();
	console.log(loged);
	
    var res = $.ajax({
        type: "GET",
        dataType: "json",
        url: '/index.php?cl=quickorder&fnc=getArticles&artnum=' + $('#OXARTNUM').val(),
        async : false,
        success : function(res) {
            //
            console.log(res);
            $('#newEntryAutoComplete').html('');
            //
            if(res.articles!=undefined)
            {
                $.each(res.articles, function(key,val) {
                    $('#newEntryAutoComplete').append(
                        '<table>' +
                        '   <tr class="tr ' + (key % 2 == 0 ? 'even' : 'odd') + '" onclick="addQuickorderItem(\'' + addslashes(JSON.stringify(val)) + '\');">' +
                        '       <td class="td artnum">' + this.artnum + '</td>' +
                        '       <td class="td title">' + this.title + '</td>' +
                        '       <td class="td price">' + this.price + '</td>' +
                        '       <td class="td amount"></td>' +
                        '       <td class="td stock"><div class="stock' + this.bnflagbestand + '"></div></td>' +
                        '   </tr>' +
                        '</table>'
                    );
                });
            }
            else if(res.suggestions!=undefined)
            {
                var strSuggestion = strSuggestionHtml = '';
                //
                $.each(res.suggestions, function(key, val) {
                    strSuggestion+= val.word + ' ';
                    strSuggestionHtml+=(val.changed ? '<b><i>' : '') + val.word + (val.changed ? '</i></b>' : '') + ' ';
                });
                //
                $('#newEntryAutoComplete').append(
                    res.suggesttext
                    +
                    '<a href="#" onclick="$(\'#OXARTNUM\').val(\'' + strSuggestion +'\');lookupForItems();">' + strSuggestionHtml + '</a>'
                );
            }
        },
        error: function(res){
            console.log(res + 'err');
        }
    });
}

/**
 * Funktion zur Löschung eines Items aus Warenkorb in Quickorder
 */

function deleteBasketItem(x){
    console.log(x.offsetParent.parentElement);
    x.offsetParent.parentElement.remove();
}


/**
 *
 * @param params
 */
function addQuickorderItem(params) {
    //
    eval('params = ' + params);
    // clear result list
    $('#newEntryAutoComplete').html('');
    // clear inputs
    $('#OXARTNUM').val('');
    $('#OXTITLE').val('');
    // add row to table
    var strEntry =  '<tr class="tr item">' +
        '   <td class="td artnum">' + params.artnum + '</td>' +
        '   <td class="td title">' + params.title + '</td>' +
        '   <td class="td price">' + params.price + '</td>' +
        '   <td class="td amount"><input type="text" name="amount[' + params.oxid + ']" value="1"></td>' +
        '   <td class="td stock"><div class="stock' + params.bnflagbestand + '"></div></td>' +
        '   <td class="td trash" ><button type="button" class="btn btn-xs btn-outline-danger" onclick="deleteBasketItem(this)"><span class="fa fa-trash"></span></button></td>' +
        '</tr>';
    //
    $(strEntry).insertBefore('#QuickPositions #newEntry');
    // ende
}

/**
 *
 */
function placeQuickOrder(e1, e2, e3) {
    // disable button with ladda
    //var ladda = Ladda.create(document.querySelector('#btnPlaceOrder'));
    //var spinner= '  <span id="spinner" class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>';
    //$('#btnPlaceOrder').append(spinner);
    $('#spinnerOrder').attr('style','display:block');

    //ladda.start();
    $("#frmQuickOrder").submit(
            $.ajax({
                url : '/index.php?cl=quickorder&fnc=submitBasket',
                type: 'POST',
                data: $("#frmQuickOrder").serialize(),
                dataType : 'json',
                success: function(res){
                    $('#spinnerOrder').attr('style','display:none');
                    console.log(res);
                    //ladda.stop();
                    // close preview modal and open confirmation when successful
                    // console.log(res);
                    if(res.state == 1) {
                        $('#previeworder').modal('hide');


                        // console.log(res);
                        // var strsuccessMsg = res.msg;
                        // var ordernr = res.ordernr;
                        // var cont = formatString(strsuccessMsg , ordernr);

                        $('#ordercomplete .modal-body').append('<div class="alert alert-success" role="alert">#' + res.ordernr  + '<div/>');
                        $('#ordercomplete').modal({});
                    } else {
                        //
                        var strErrorMsg = '';
                        switch(res.state) {
                            case 0:
                                strErrorMsg = e1;
                                break;
                            case -1:
                                strErrorMsg = e2;
                                break;
                        }
                        //
                        $('#previeworder .modal-body').prepend('<div class="alert alert-danger" role="alert">' + strErrorMsg + '</div>');
                        // ende
                    }
                },
                error:function(res){
                    $('#spinnerOrder').attr('style','display:none');

                    alertify.error(e3);

                    console.log(res);
                }
            })

    );


    //
    /*
    $('#frmQuickOrder').ajaxForm({
        type: "POST",
        dataType: 'json',
        url: '/index.php?cl=quickorder&fnc=submitBasket',
        success:function(res) {
            //
            ladda.stop();
            // close preview modal and open confirmation when successful
	        // console.log(res);
            if(res.state == 1) {
                $('#previeworder').modal('hide');
                
	
                // console.log(res);
	            // var strsuccessMsg = res.msg;
	            // var ordernr = res.ordernr;
	            // var cont = formatString(strsuccessMsg , ordernr);
	            
	            $('#ordercomplete .modal-body').append('<div class="alert alert-success" role="alert">#' + res.ordernr  + '<div/>');
	            $('#ordercomplete').modal({});
            } else {
                //
                var strErrorMsg = '';
                switch(res.state) {
                    case 0:
                        strErrorMsg = e1;
                    break;
                    case -1:
                        strErrorMsg = e2;
                    break;
                }
                //
                $('#previeworder .modal-body').prepend('<div class="alert alert-danger" role="alert">' + strErrorMsg + '</div>');
                // ende
            }
            // ende
        },
        error:function(res){
            alertify.error(e3);
            
            console.log(res);
        }
    }).submit(); */
    // ende
}





//
//
//
//
//

/**
 *
 */
function setAdress(oxid, company, sal, fname, lname, street, streetnr, addinfo, zip, city, countryid, fon ) {
    //

    $('#addressbookid').val(oxid);
    $('#oxcompany').val(company).attr('placeholder','');
    $('#oxsal').val(sal);
    $('#oxfname').val(fname).attr('placeholder','');
    $('#oxlname').val(lname).attr('placeholder','');
    $('#oxstreet').val(street).attr('placeholder','');
    $('#oxstreetnr').val(streetnr).attr('placeholder','');
    $('#oxaddinfo').val(addinfo).attr('placeholder','');
    $('#oxzip').val(zip).attr('placeholder','');
    $('#oxcity').val(city).attr('placeholder','');
    $('#oxfon').val(fon).attr('placeholder','');
    $('#oxcountryid').val(countryid).attr('placeholder','');
    //
    $('#adressBookAutocomplete').html('');
    $('#searchadressbook').val('');
    // ende

}


/**
 *
 */
function searchInAdressbook() {
    //
    var res = $.ajax({
        type: "GET",
        dataType: "json",
        url: '/index.php?cl=dre_ajax_quickorder&fnc=getAdressAutocomplete&search=' + $('#searchadressbook').val(),
        async : false,
        success:function (response) {
            console.log('success');
            console.log(response);

        }
    }).responseJSON;

    $('#adressBookAutocomplete').html('');
    //
    $.each(res, function() {
        //
        $('#adressBookAutocomplete').append(
            '<div onclick="setAdress(\'' + this.oxid + '\',\'' + this.company + '\',\'' + this.sal + '\',\'' + this.fname + '\',\'' + this.lname + '\',\'' + this.street + '\',\'' + this.streetnr + '\',\'' + this.addinfo + '\',\'' + this.zip + '\',\'' + this.city + '\',\'' + this.countryid + '\',\'' + this.fon + '\')" class="addressBookItem">' +
                (this.company!='' ? this.company + '<br>': '') +
                this.fname + ' ' + this.lname + '<br>' +
                this.street + ' ' + this.streetnr + '<br>' +
                this.zip + ' ' + this.city + '<br>' +
                this.addinfo  +
            '</div>'
        );
        // ende
    });
     // ende
}

var timeOutHolderAdressbookSearch = null;

$(document).ready(function() {
    $('#searchadressbook').keyup(function() {
        //
        if(timeOutHolderAdressbookSearch)
            window.clearTimeout(timeOutHolderAdressbookSearch);
        //
        timeOutHolderAdressbookSearch = window.setTimeout('searchInAdressbook()', 500);
        // ende
    });
    //
    $('#addressbookid, #oxcompany, #oxsal, #oxfname, #oxlname, #oxstreet, #oxstreetnr, #oxaddinfo, #oxzip, #oxcity, #oxcountryid').change(function() {
        $('#addressbookid').val('');
    });
    //
    /*
    $('#searchadressbook').val('geil').keyup();
    addQuickorderItem('{\'oxid\':\'edae7ac1f920327673822d2c2bc1f404\',\'artnum\':\'hms1\',\'title\':\'Hamam Set I zum Sonderpreis\',\'price\':4.96,\'bnflagbestand\':\'0\'}');
    addQuickorderItem('{\'oxid\':\'eda3ce1925f2b48e3947eadc87bcb196\',\'artnum\':\'hms2b\',\'title\':\'Hamam Set II blau, zum Sonderpreis\',\'price\':10,\'bnflagbestand\':\'0\'}');
    addQuickorderItem('{\'oxid\':\'8894ac5e442372d96.61335702\',\'artnum\':\'ms88b\',\'title\':\'Meditations Set: Zafu & Zabuton(80x80 cm) | blau\',\'price\':62.94,\'bnflagbestand\':\'0\'}');
    */
    // ende
});

function showConfirmQuickOrder(count, article, price, sum) {
    var strOverview = "<table id=\"orderPreview\"><tr class=\"headline\">";
    strOverview += "<td class=\"amount\">" + count + "</td>";
    strOverview += "<td class=\"title\">" + article + "</td>";
    strOverview += "<td class=\"price\">" + price + "</td></tr>";
    var summe = 0.00;
    $(".item").each(function () {
        strOverview += "<tr>";
        strOverview += "  <td class=\"amount\">" + $(this).children(".amount").children("input").val() + "</td>";
        strOverview += "  <td class=\"title\">" + $(this).children(".title").text() + "</td>";
        strOverview += "  <td class=\"price\">" + $(this).children(".price").text() + "</td>";
        strOverview += "</tr>";
        summe = summe + (parseFloat($(this).children(".price").text().replace(',','.'))*(parseInt($(this).children(".amount").children("input").val())));
    });
    strOverview +="<tr><td></td><td></td><td style='text-align: right'><b>" + sum + " " + summe.toFixed(2).replace('.', ',') + "€</b></td></tr>";

    strOverview += "</table>";
    $("#previeworder .modal-body").html(strOverview);
    $("#previeworder").modal();
}